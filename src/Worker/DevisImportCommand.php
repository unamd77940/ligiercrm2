<?php

namespace App\Worker;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\Article;
use App\Entity\Devis;
use App\Entity\DevisImport;
use App\Service\ImportCSV;

Class DevisImportCommand extends ContainerAwareCommand {

    private $fileName = 'DEVIS.CSV';
	private $fileNameLignes = 'LIGNESDEVIS.CSV';

	protected function configure() {
		$this
            ->setName('importcsv:devis')
            ->setDescription('Importe les devis issus de X3')
            ->addArgument('start', InputArgument::OPTIONAL, 'Démarrer à partir de l\'entrée n° (défaut 0)');
            ;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		$now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        $start = intval($input->getArgument('start'));
        if (!$start) {
            $start = 0;
        } 

		$this->import($input, $output, $start);
		$now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
	}


    protected function import(InputInterface $input, OutputInterface $output, $start) {
        $directory = $this->getContainer()->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);


        // Coupe l'execution si la base est déjà en cours de mise à jour
        $count = $em->getRepository(DevisImport::class)->countEntries();
        sleep(280);
        $count2 = $em->getRepository(DevisImport::class)->countEntries();
        if($count>$count2) {
            $output->writeln("Mise à jour déjà en cours");
            exit;
        }

        // stocke les désignations des articles pour les réinjecter dans chaque ligne de devis
        $articles = $em->getRepository(Article::class)->findAll();
        
        foreach ($articles as $key => $value) {
            //$designations[$value->getCodeX3()] = $value->getDesignation();
            $articlesAllInfos[$value->getCodeX3()] = [
                        'designation' => $value->getDesignation(),
                        'uv' => $value->getUv(),
                        'uf' => $value->getUf(),
                        'prix_revient_rennes' => $value->getPrixRevientRennes(),
                        'prix_revient_lille' => $value->getPrixRevientLille()
                    ];
        }

        $i = 0;


        //$logger = new \Doctrine\DBAL\Logging\DebugStack();
        //$connexion->getConfiguration()->setSQLLogger($logger);

        $devisImport = $em->getRepository(DevisImport::class)->findEntries();

        while($devisImport) {
            foreach ($devisImport as $key => $value) {
                // Cherche la devis dans la table contenant ce code
                $devis = $em->getRepository(Devis::class)->findOneBy(array('code' => $value->getCode()));
                

                if(!$devis) {
                    // Devis n'existe pas, on crée un devis
                    $devis = new Devis;
                }

                $devis->setCode($value->getCode());
                $devis->setClientCode($value->getClientCode());
                $devis->setRedacteurCode($value->getRedacteurCode());
                $devis->setDate($value->getDate());
                $devis->setEtat($value->getEtat());
                $devis->setStatut($value->getStatut());
                $devis->setPerte($value->getPerte());
                $devis->setObservations($value->getObservations());
                $devis->setAdresseCode($value->getAdresseCode());
                $devis->setSiteVente($value->getSiteVente());
                $devis->setSiteExpedition($value->getSiteExpedition());
                $devis->setCommentaires($value->getCommentaires());
                $devis->setRef($value->getRef());
                $devis->setFraisTransport($value->getFraisTransport());
                $devis->setTotaldevis($value->getTotaldevis());
                $devis->setTotalHt($value->getTotalHt());
                $devis->setDateCreation($value->getDateCreation());
                $devis->setDateValidite($value->getDateValidite());



                // Recherche des caractéristiques des articles de la devis
                $sql = "SELECT * FROM lignesdevis WHERE code_devis = '".$value->getCode()."' ";
                $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
                $stmt->execute();
                $lignes = $stmt->fetchAll();

                //var_dump($sql);

                $detailDevis = [];
                foreach ($lignes as $keyArticle => $valueArticle) {            
                    $detailDevis[] = [
                                            "code" => $valueArticle['code'],
                                            "designation" => isset($articlesAllInfos[$valueArticle['code']]['designation']) ? $$articlesAllInfos[$valueArticle['code']]['designation'] : $valueArticle['code'],
                                            "site" => $valueArticle['site'],
                                            "quantite" => $valueArticle['quantite'],
                                            //"reliquat" => $valueArticle['reliquat'],
                                            "prix_tarif" => $valueArticle['prix_tarif'],
                                            "remise" => $valueArticle['remise'],
                                            "prix_net" => $valueArticle['prix_net'],
                                            "prix_revient" => $valueArticle['prix_revient'],
                                            "uv" => $articlesAllInfos[$valueArticle['code']]['uv'],
                                            "uf" => $articlesAllInfos[$valueArticle['code']]['uf'],
                                            "prix_revient_rennes" => $articlesAllInfos[$valueArticle['code']]['prix_revient_rennes'],
                                            "prix_revient_lille" => $articlesAllInfos[$valueArticle['code']]['prix_revient_lille']
                                        ];
                }

                $devis->setArticles(json_encode($detailDevis));





                $em->persist($devis);            
                $em->remove($value); 
                usleep(100000); 
                $i++;

                if($i%20 == 0) {
                    //$output->writeln("flush");  
                    $em->flush();
                    //var_dump($logger->queries);
                    $output->writeln($i." devis à jour");
                    sleep(2);
                }

                if($i%1000 == 0) sleep(10);
                if($i%10000 == 0) sleep(240);

                // if($i > 960) {
                //     $now = new \DateTime();
                //     $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
                //     exit;
                // }
            }
            $em->flush(); 
            sleep(3);
            $devisImport = $em->getRepository(DevisImport::class)->findEntries();           
        }



        $output->writeln("Terminé");


    }

	protected function get(InputInterface $input, OutputInterface $output, $file) 
    {
        // Getting the CSV from filesystem
        //$this->fileName = 'public/imports/ARTICLES.CSV';

        $csvService = new ImportCSV;
        $data = $csvService->convert($file, ',');
        


        return $data;
    }



}


