<?php

namespace App\Worker;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class SavWorkerCommand extends AbstractWorkerCommand
{
    protected static $defaultName = 'worker:import:sav';

    protected function getEntityClass(){
        return \App\Entity\Sav::class;
    }
    protected function getEntityBuilder($repository){
        return new \App\Builder\SavBuilder($repository);
    }
    protected function getTable(){
        return 'sav_imports';
    }
}