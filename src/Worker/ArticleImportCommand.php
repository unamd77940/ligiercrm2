<?php

namespace App\Worker;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\Article;
use App\Entity\ArticleImport;
use App\Service\ImportCSV;

Class ArticleImportCommand extends ContainerAwareCommand {

	private $fileName = 'public/imports/ARTICLES.CSV';

	protected function configure() {
		$this->setName('importcsv:article')->setDescription('Importe les articles issus de X3');
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		$now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
		$this->import($input, $output);
		$now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
	}


	protected function import(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);

        // Coupe l'execution si la base est déjà en cours de mise à jour
        $count = $em->getRepository(ArticleImport::class)->countEntries();
        //sleep(280);
        $count2 = $em->getRepository(ArticleImport::class)->countEntries();
        if($count>$count2) {
            $output->writeln("Mise à jour déjà en cours");
            exit;
        }

        $articlesImport = $em->getRepository(ArticleImport::class)->findAll(array(), array('code_x3'=>'ASC'));


        // Récupère et "fusionne" dans un tableau les articles importés 
        $allImport = [];
        foreach ($articlesImport as $keyImport => $articleImport) {
            if(!empty($articleImport->getSite())) {
                $allImport[$articleImport->getCodeX3()]['designation'] = trim($articleImport->getDesignation());
                $allImport[$articleImport->getCodeX3()]['fournisseur'] = trim($articleImport->getFournisseur());
                $allImport[$articleImport->getCodeX3()]['uv'] = $articleImport->getUv();

                // Menu local X3 1003. Oui c'est en dur mais là sincèrement...
                $refUf = [
                            1 => 1,
                            2 => 5,
                            3 => 10,
                            4 => 20,
                            5 => 25,
                            6 => 50,
                            7 => 100,
                            8 => 150,
                            9 => 200,
                            10 => 250,
                            11 => 500,
                            12 => 1000
                          ];
                $allImport[$articleImport->getCodeX3()]['uf'] = $refUf[$articleImport->getUf()];

                $allImport[$articleImport->getCodeX3()]['cdt'] = $articleImport->getCdt();
                $allImport[$articleImport->getCodeX3()]['prixTarif'] = $articleImport->getPrixTarif();
                $allImport[$articleImport->getCodeX3()]['prixRevientRennes'] = $articleImport->getPrixRevientRennes();
                $allImport[$articleImport->getCodeX3()]['prixRevientLille'] = $articleImport->getPrixRevientLille();
                $allImport[$articleImport->getCodeX3()]['prixMedian'] = $articleImport->getPrixMedian();

                $site = ucfirst(strtolower($articleImport->getSite()));            
                $allImport[$articleImport->getCodeX3()]['tenue'.$site] = $articleImport->getTenue();
                $allImport[$articleImport->getCodeX3()]['stockInterneA'.$site] = $articleImport->getStockInterneA();                
            }

        }
        $output->writeln(count($allImport).' articles à intégrer'); 

        // Insère ou met à jour les données
        $batchSize = 20;
        $i = 0;

        foreach ($allImport as $codeX3 => $articleImporte) {
            $article = $em->getRepository(Article::class)->findOneBy(array('code_x3'=>$codeX3));
            if(!$article) $article = new Article;

            $article->setCodeX3($codeX3);
            // Pour chaque entrée $value['nomdelaclé'] ou va faire appel à $article->setNomdelaclé()
            foreach ($articleImporte as $classSuffixe => $value) {
                $article->{'set'.ucfirst($classSuffixe)}($value);   
            }

            // Calcul du prix median 
            $sql = "SELECT prix_net FROM lignescommandes WHERE code='".$codeX3."' AND prix_net>0 ORDER BY prix_net";
            $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $stmt->execute();
            $prixnet = $stmt->fetchAll();
            if(count($prixnet)) {
                $indexMedian = ceil(count($prixnet)/2)-1;
                $article->setPrixMedian($prixnet[$indexMedian]['prix_net']);
            }


            $em->persist($article);
            try {
                $toRemove = $em->getRepository(ArticleImport::class)->findOneBy(array('code_x3'=>$codeX3));
                $em->remove($toRemove);
            } catch(\Exception $e) {
                $output->writeln($e->getMessage());
            }

            if (($i % $batchSize) === 0) { 
                $em->flush();
                $em->clear();       
                $output->writeln($i.' imports article terminés');  
                usleep(500000); 
            } 

            $i++;
        }

        $output->writeln($i.' imports au total');  

        $em->flush();
        $em->clear();

        //var_dump($allImport['A143828']);





		// $data = $this->get($input, $output); // csv-> array

		// //$output->writeln($data);

		// $em = $this->getContainer()->get('doctrine')->getManager();
		// $em->getConnection()->getConfiguration()->setSQLLogger(null);

		// $size = count($data);
  //       $batchSize = 10;
  //       $i = 0;

  //       $output->writeln($size.'  total'); 

  //       foreach($data as $row) { 
  //           $article = $em->getRepository(Article::class)->findOneBy(array('code_x3'=>$row[0]));
						 
		// 	if(!$article){
  //               $article = new Article();
  //               $article->setCodeX3($row[0]);
  //           }
  //           $output->writeln(implode(" / ", $row)); 
		// 	// Remplissage de l'objet article			
  //           $article->setDesignation($row[1]);
  //           $article->setFournisseur($row[2]);
  //           $article->setUv($row[3]);
  //           $article->setUf($row[4]);
  //           $article->setCdt($row[5]);
  //           $article->setPrixTarif($row[6]);            	


			
  //           $em->persist($article);
            
  //           if (($i % $batchSize) === 0) { 
  //               $em->flush();
  //               $em->clear();       
  //           	$output->writeln($i.' imports terminés');  
  //   			usleep(500000); 
  //           } 

  //           $i++;
  //       }

  //       $now = new \DateTime;
  //       $newFile = str_replace(".CSV", "_termine_".$now->format('Y-m-d-his').".CSV", $this->fileName);

  //       $fs = new Filesystem();
  //       //$fs->rename($this->fileName, $newFile);

  //   	$output->writeln($i.' imports au total');  

	 //    $em->flush();
  //   	$em->clear();
	}


	protected function get(InputInterface $input, OutputInterface $output) 
    {
        // Getting the CSV from filesystem
        //$this->fileName = 'public/imports/ARTICLES.CSV';

        $csvService = new ImportCSV;
        $data = $csvService->convert($this->fileName, ',');
        


        return $data;
    }



}


