<?php

namespace App\Worker;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class ContactWorkerCommand extends AbstractWorkerCommand
{
    protected static $defaultName = 'worker:import:contact';

    protected function getEntityClass(){
        return \App\Entity\Contact::class;
    }
    protected function getEntityBuilder($repository){
        return new \App\Builder\ContactBuilder($repository);
    }
    protected function getTable(){
        return 'contact_imports';
    }
}