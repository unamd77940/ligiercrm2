<?php

namespace App\Worker;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\Article;
use App\Entity\Facture;
use App\Entity\FactureImport;
use App\Service\ImportCSV;

Class FactureImportCommand extends ContainerAwareCommand {

    private $fileName = 'FACTURES.CSV';
	private $fileNameLignes = 'LIGNESFACTURES.CSV';

	protected function configure() {
		$this
            ->setName('importcsv:facture')
            ->setDescription('Importe les factures issues de X3')
            ->addArgument('start', InputArgument::OPTIONAL, 'Démarrer à partir de l\'entrée n° (défaut 0)');
            ;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		$now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        $start = intval($input->getArgument('start'));
        if (!$start) {
            $start = 0;
        } 

		$this->import($input, $output, $start);
		$now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
	}


    protected function import(InputInterface $input, OutputInterface $output, $start) {
        $directory = $this->getContainer()->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);


        // Coupe l'execution si la base est déjà en cours de mise à jour
        $count = $em->getRepository(FactureImport::class)->countEntries();
        sleep(20);
        $count2 = $em->getRepository(FactureImport::class)->countEntries();
        if($count>$count2) {
            $output->writeln("Mise à jour déjà en cours");
            exit;
        }

        // stocke les désignations des articles pour les réinjecter dans chaque ligne de facture
        $articles = $em->getRepository(Article::class)->findAll();
        
        foreach ($articles as $key => $value) {
            $designations[$value->getCodeX3()] = $value->getDesignation();
        }

        $i = 0;


        //$logger = new \Doctrine\DBAL\Logging\DebugStack();
        //$connexion->getConfiguration()->setSQLLogger($logger);

        $factureImport = $em->getRepository(FactureImport::class)->findEntries();

        while($factureImport) {
            foreach ($factureImport as $key => $value) {
                // Cherche la facture dans la table contenant ce code
                $facture = $em->getRepository(Facture::class)->findOneBy(array('code' => $value->getCode()));
                

                if(!$facture) {
                    // Facture n'existe pas, on crée un facture
                    $facture = new Facture;
                }

                $facture->setCode($value->getCode());
                $facture->setClientCode($value->getClientCode());
                $facture->setDate($value->getDate());
                $facture->setSiteVente($value->getSiteVente());
                $facture->setTotalHt($value->getTotalHt());
                $facture->setType($value->getType());



                // Recherche des caractéristiques des articles de la facture
                $sql = "SELECT * FROM lignesfacture WHERE code_facture = '".$value->getCode()."' ";
                $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
                $stmt->execute();
                $lignes = $stmt->fetchAll();

                //var_dump($sql);

                $detailFacture = [];
                foreach ($lignes as $keyArticle => $valueArticle) {            
                    $detailFacture[] = [
                                            "code" => $valueArticle['code'],
                                            "designation" => isset($designations[$valueArticle['code']]) ? $designations[$valueArticle['code']] : $valueArticle['code'],
                                            "quantite" => $valueArticle['quantite'],
                                            "prix_tarif" => $valueArticle['prix_tarif'],
                                            "remise" => $valueArticle['remise'],
                                            "marge" => $valueArticle['marge'],
                                            "prix_net" => $valueArticle['prix_net'],
                                            "prix_revient" => $valueArticle['prix_revient']
                                        ];
                }

                $facture->setArticles(json_encode($detailFacture));





                $em->persist($facture);            
                $em->remove($value); 
                //usleep(100000); 
                //usleep(5000); 
                $i++;

                if($i%100 == 0) {
                    //$output->writeln("flush");  
                    $em->flush();
                    //var_dump($logger->queries);
                    $output->writeln($i." factures à jour");
                    sleep(1);
                }

                //if($i%1000 == 0) sleep(10);
                //if($i%10000 == 0) sleep(240);

                // if($i > 960) {
                //     $now = new \DateTime();
                //     $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
                //     exit;
                // }
            }
            $em->flush(); 
            sleep(2);
            $factureImport = $em->getRepository(FactureImport::class)->findEntries();           
        }



        $output->writeln("Terminé");


    }

	protected function get(InputInterface $input, OutputInterface $output, $file) 
    {
        // Getting the CSV from filesystem
        //$this->fileName = 'public/imports/ARTICLES.CSV';

        $csvService = new ImportCSV;
        $data = $csvService->convert($file, ',');
        


        return $data;
    }



}


