<?php

namespace App\Worker;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\Article;
use App\Entity\Commande;
use App\Entity\CommandeImport;
use App\Service\ImportCSV;

Class CommandeImportCommand extends ContainerAwareCommand {

    private $fileName = 'COMMANDES.CSV';
	private $fileNameLignes = 'LIGNESCOMMANDES.CSV';

	protected function configure() {
		$this
            ->setName('importcsv:commande')
            ->setDescription('Importe les commandes issues de X3')
            ->addArgument('start', InputArgument::OPTIONAL, 'Démarrer à partir de l\'entrée n° (défaut 0)');
            ;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		$now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        $start = intval($input->getArgument('start'));
        if (!$start) {
            $start = 0;
        } 

		$this->import($input, $output, $start);
		$now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
	}


    protected function import(InputInterface $input, OutputInterface $output, $start) {
        $directory = $this->getContainer()->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);


        // Coupe l'execution si la base est déjà en cours de mise à jour
        $count = $em->getRepository(CommandeImport::class)->countEntries();
        sleep(280);
        $count2 = $em->getRepository(CommandeImport::class)->countEntries();
        if($count>$count2) {
            $output->writeln("Mise à jour déjà en cours");
            exit;
        }

        // stocke les désignations des articles pour les réinjecter dans chaque ligne de commande
        $articles = $em->getRepository(Article::class)->findAll();
        
        foreach ($articles as $key => $value) {
            $designations[$value->getCodeX3()] = $value->getDesignation();
        }

        

        // Requete les lignes de commande
        /*
        $sql = "SELECT * FROM lignescommandes";
        $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
        $stmt->execute();
        $lignes = $stmt->fetchAll();
        $output->writeln("Lignes de commandes chargées");
        
        foreach ($lignes as $key => $value) {            
            $detailCommande[$value['code_commande']][] = [
                                        "code" => $value['code'],
                                        "designation" => isset($designations[$value['code']]) ? $designations[$value['code']] : $value['code'],
                                        "site" => $value['site'],
                                        "quantite" => $value['quantite'],
                                        "reliquat" => $value['reliquat'],
                                        "prix_tarif" => $value['prix_tarif'],
                                        "remise" => $value['remise'],
                                        "prix_net" => $value['prix_net']
                                    ];
            unset($lignes[$key]);
        }

        unset($lignes);
        $output->writeln("Table de détail des lignes crée");
        */

        

        // Modifie les commandes pour y intégrer les articles
        //$output->writeln("commandesImport : ".count($commandesImport));
        $i = 0;


        //$logger = new \Doctrine\DBAL\Logging\DebugStack();
        //$connexion->getConfiguration()->setSQLLogger($logger);

        $commandesImport = $em->getRepository(CommandeImport::class)->findEntries();

        while($commandesImport) {
            foreach ($commandesImport as $key => $value) {
                // Cherche la commande dans la table contenant ce code
                $commande = $em->getRepository(Commande::class)->findOneBy(array('code' => $value->getCode()));
                

                if(!$commande) {
                    // Commande n'existe pas, on crée une nouvelle commande
                    $commande = new Commande;
                }

                $commande->setCode($value->getCode());
                $commande->setClientCode($value->getClientCode());
                $commande->setStatut($value->getStatut());
                $commande->setAdresseCode($value->getAdresseCode());
                $commande->setSiteVente($value->getSiteVente());
                $commande->setSiteExpedition($value->getSiteExpedition());
                $commande->setModeLivraison($value->getModeLivraison());
                $commande->setCommentaires($value->getCommentaires());
                $commande->setRef($value->getRef());
                $commande->setFraisTransport($value->getFraisTransport());
                $commande->setTotalHT($value->getTotalHT());
                $commande->setDateCreation($value->getDateCreation());

                // Recherche des caractéristiques des articles de la commande
                $sql = "SELECT * FROM lignescommandes WHERE code_commande = '".$value->getCode()."' ";
                $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
                $stmt->execute();
                $lignes = $stmt->fetchAll();

                $detailCommande = [];
                foreach ($lignes as $keyArticle => $valueArticle) {            
                    $detailCommande[] = [
                                            "code" => $valueArticle['code'],
                                            "designation" => isset($designations[$valueArticle['code']]) ? $designations[$valueArticle['code']] : $valueArticle['code'],
                                            "site" => $valueArticle['site'],
                                            "quantite" => $valueArticle['quantite'],
                                            "reliquat" => $valueArticle['reliquat'],
                                            "prix_tarif" => $valueArticle['prix_tarif'],
                                            "remise" => $valueArticle['remise'],
                                            "prix_net" => $valueArticle['prix_net']
                                        ];
                }

                $commande->setArticles(json_encode($detailCommande));




                $em->persist($commande);            
                $em->remove($value); 
                usleep(100000); 
                $i++;

                if($i%20 == 0) {
                    //$output->writeln("flush");  
                    $em->flush();
                    //var_dump($logger->queries);
                    $output->writeln($i." commandes à jour");
                    sleep(2);
                }

                if($i%1000 == 0) sleep(10);
                if($i%10000 == 0) sleep(240);

                // if($i > 960) {
                //     $now = new \DateTime();
                //     $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
                //     exit;
                // }
            }
            $em->flush(); 
            sleep(3);
            $commandesImport = $em->getRepository(CommandeImport::class)->findEntries();           
        }



        $output->writeln("Terminé");


    }

    /*
	protected function import_old(InputInterface $input, OutputInterface $output, $start) {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        // stocke les désignations des articles pour les réinjecter dans chaque ligne de commande
        $articles = $em->getRepository(Article::class)->findAll();
        foreach ($articles as $key => $value) {
            $designations[$value->getCodeX3()] = $value->getDesignation();
        }

        // On crée un tableau contenant les lignes de chaque commande
        // lignes['numero de commande'] = [[ligne1], [ligne2]...]
		$dataLignes = $this->get($input, $output, $this->fileNameLignes); // csv-> array
        foreach ($dataLignes as $key => $value) {
            $code = str_replace('"', '', $value[2]);
            $lignes[$value[0]][] =  [
                                        "code" => $code,
                                        "designation" => isset($designation[$code]) ? $designation[$code] : $code,
                                        "site" => $value[3],
                                        "quantite" => $value[4],
                                        "reliquat" => $value[5],
                                        "prix_tarif" => $value[6],
                                        "remise" => $value[7],
                                        "prix_net" => $value[8]
                                    ];
        }

        // On récupère les commandes
        $dataCommandes = $this->get($input, $output, $this->fileName);

        

  //       $output->writeln(count($dataCommandes));
		// $output->writeln(count($lignes));
        
  //       $output->writeln($dataCommandes[0]);
        //$output->writeln($lignes[0][0]);
        //var_dump($dataCommandes[0]);

        $batchSize = 20;
        $i = 0;

        // Supprime les entrées déjà traitées 
        if($start) {
            $dataCommandes = array_slice($dataCommandes, ($start-1));
        }


        foreach($dataCommandes as $row) { 
            $commande = $em->getRepository(Commande::class)->findOneBy(array('code'=>$row[0]));
						 
			if(!$commande){
                $commande = new Commande();
                $commande->setCode($row[0]);
            }
            $date = \DateTime::createFromFormat('Ymd', $row[3]);
			// Remplissage de l'objet commande			
            $commande->setClientCode($row[2]);
            $commande->setDate($date);
            $commande->setDateCreation($date);               
            $commande->setArticles(isset($lignes[$row[0]]) ? json_encode($lignes[$row[0]]) : '[]');
            $commande->setAdresseCode($row[9]);
            $commande->setSiteVente($row[10]);
            $commande->setSiteExpedition($row[11]);              
            $commande->setModeLivraison($row[8]);               
            $commande->setCommentaires($row[6]);               
            $commande->setRef($row[7]);               
            $commande->setFraisTransport($row[12]);               
            $commande->setTotalHT($row[13]);                        
            $commande->setStatut($row[14]);                     	


			
            $em->persist($commande);
            
            if (($i % $batchSize) === 0) { 
                $em->flush();
                $em->clear();       
            	$output->writeln($i.' imports terminés');  
    			usleep(500000); 
            } 

            $i++;
        }

        $now = new \DateTime;
        $newFile = str_replace(".CSV", "_termine_".$now->format('Y-m-d-his').".CSV", $this->fileName);

     //    $fs = new Filesystem();
     //    //$fs->rename($this->fileName, $newFile);

    	$output->writeln($i.' imports au total');  

	    $em->flush();
    	$em->clear();
	}
    */

	protected function get(InputInterface $input, OutputInterface $output, $file) 
    {
        // Getting the CSV from filesystem
        //$this->fileName = 'public/imports/ARTICLES.CSV';

        $csvService = new ImportCSV;
        $data = $csvService->convert($file, ',');
        


        return $data;
    }



}


