<?php

namespace App\Worker;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class TiersWorkerCommand extends AbstractWorkerCommand
{
    protected static $defaultName = 'worker:import:tiers';

    protected function getEntityClass(){
        return \App\Entity\Tiers::class;
    }
    protected function getEntityBuilder($repository){
        return new \App\Builder\TiersBuilder($repository);
    }
    protected function getTable(){
        return 'tiers_imports';
    }
}