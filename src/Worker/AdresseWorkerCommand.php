<?php

namespace App\Worker;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class AdresseWorkerCommand extends AbstractWorkerCommand
{
    protected static $defaultName = 'worker:import:adresse';

    protected function getEntityClass(){
        return \App\Entity\Adresse::class;
    }
    protected function getEntityBuilder($repository){
        return new \App\Builder\AdresseBuilder($repository);
    }
    protected function getTable(){
        return 'adresse_imports';
    }
}