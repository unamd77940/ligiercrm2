<?php

namespace App\Worker;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;

abstract class AbstractWorkerCommand extends Command
{
    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @return null|int null or 0 if everything went fine, or an error code
     *
     * @throws LogicException When this abstract method is not implemented
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sleep = 100;
        //$io = new SymfonyStyle($input, $output);
        /** @var $kernel KernelInterface */
        $rsm = new ResultSetMapping();
        $container = $this->getApplication()->getKernel()->getContainer();
        $entityManager = $container->get('doctrine')->getManager();;
        $repository = $entityManager->getRepository($this->getEntityClass());
        $builder = $this->getEntityBuilder($repository);
        $db = $entityManager->getConnection(); // Dans un Controller

        $start_time = time();
        while(true) {
            $stmt = $db->prepare('SELECT * FROM '.$this->getTable().' LIMIT 1');
            $stmt->execute();
            $result = $stmt->fetch();
            if($result === false){
                sleep(20);
                continue;
            }
            $entity = $builder->process($result);
            $entityManager->persist($entity);
            $entityManager->flush();

            $stmt = $db->prepare('DELETE FROM '.$this->getTable().' WHERE id = ?');
            $stmt->execute([$result['id']]);
            usleep($sleep*1000);
            if ((time() - $start_time) > 600) {
                return null;
            }
        }
    }

    abstract protected function getEntityClass();
    abstract protected function getEntityBuilder($repository);
    abstract protected function getTable();
}