<?php

namespace App\Builder;

// fichier TiersBuilder
class TiersBuilder implements EntityBuilderFromCsv
{

    private $repository;

    public function __construct(\App\Repository\TiersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Undocumented function
     *
     * @param array $tempTiers
     * @return Tiers
     */
    public function process(array $csvTiers)
    {
        $id = $csvTiers['code_x3'];    // ID
        $realTiers = $this->repository->findByCodeX3($id);
        
        if(!$realTiers){
            $realTiers = new \App\Entity\Tiers();
        }
        foreach ($csvTiers as $key => $value) {
            if($key=="last_mysql_update" || $key=="id"){
                continue;
            }
            if ($key=="last_x3_update" && is_string($value)) {
                $value = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
            }
            $methodeName = explode('_',$key);
            $methode = array_map('ucfirst', $methodeName);
            $methodeFinale = 'set'.implode('', $methode);
            $realTiers->$methodeFinale($value);
        }

        $date = new \DateTime('now');
        $realTiers->setLastMysqlUpdate($date);

        return $realTiers;

    }
}