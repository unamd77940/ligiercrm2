<?php

namespace App\Builder;

// fichier EntityBuilderFromCsv
interface EntityBuilderFromCsv
{
    public function process(array $csvRow);
}

