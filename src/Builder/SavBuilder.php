<?php

namespace App\Builder;

// fichier SavBuilder
class SavBuilder implements EntityBuilderFromCsv
{

    private $repository;

    public function __construct(\App\Repository\SavRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Undocumented function
     *
     * @param array $tempSav
     * @return Sav
     */
    public function process(array $csvSav)
    {
        $id = $csvSav['code_x3'];    // ID
        $realSav = $this->repository->findOneBy(array("code_x3"=>$id), array());
        
        if(!$realSav){
            $realSav = new \App\Entity\Sav();
        }
        foreach ($csvSav as $key => $value) {
            $methodeName = explode('_',$key);
            $methode = array_map('ucfirst', $methodeName);
            $methodeFinale = 'set'.implode('', $methode);
            $methodeGet = 'get'.implode('', $methode);
            if(is_object($realSav->$methodeGet()) || preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value)) {
                try {
                    $value = \DateTime::createFromFormat('Y-m-d', $value);
                } catch(\Exception $e) {
                    //var_dump($e->getMessage());
                }
            }
            
            $realSav->$methodeFinale($value);
        }

        //$date = new \DateTime('now');
        //$realSav->setLastMysqlUpdate($date);

        return $realSav;

    }
}