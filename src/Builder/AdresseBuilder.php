<?php

namespace App\Builder;

use \App\Repository\AdresseRepository;

// fichier AdresseBuilder
class AdresseBuilder implements EntityBuilderFromCsv
{

    private $repository;

    public function __construct(AdresseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Undocumented function
     *
     * @param array $tempAdresse
     * @return Adresse
     */
    public function process(array $csvAdresse)
    {
        $id = $csvAdresse['code_x3'];    // ID
        $idTiers = $csvAdresse['code_tiers_x3'];    // ID Tiers
        $realAdresse = $this->repository->findByCodeX3AndCodeTiersX3($id, $idTiers);

        if(!$realAdresse){
            $realAdresse = new \App\Entity\Adresse();
        }

        foreach ($csvAdresse as $key => $value) {
            if($key=="id"){
                continue;
            }
            $methodeName = explode('_',$key);
            $methode = array_map('ucfirst', $methodeName);
            $methodeFinale = 'set'.implode('', $methode);
            $realAdresse->$methodeFinale($value);
        }
        
        //$realAdresse->setLastMysqlUpdate(new \DateTime("now"));

        return $realAdresse;

    }
}