<?php

namespace App\Builder;

// fichier ContactBuilder
class ContactBuilder implements EntityBuilderFromCsv
{

    private $repository;

    public function __construct(\App\Repository\ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Undocumented function
     *
     * @param array $tempContact
     * @return Contact
     */
    public function process(array $csvContact)
    {
        $id = $csvContact['code_x3'];    // ID
        $idTiers = $csvContact['code_tiers_x3'];    // ID Tiers
        $realContact = $this->repository->findByCodeX3AndCodeTiersX3($id, $idTiers);

        if(!$realContact){
            $realContact = new \App\Entity\Contact();
        }


        foreach ($csvContact as $key => $value) {
            if($key=="last_mysql_update" || $key=="id" || $key=="tiers_id"){
                continue;
            }
            $methodeName = explode('_',$key);
            $methode = array_map('ucfirst', $methodeName);
            $methodeFinale = 'set'.implode('', $methode);
            $realContact->$methodeFinale($value);
        }
        
        $date = new \DateTime('now');
        $realContact->setLastMysqlUpdate($date);

        return $realContact;

    }
}
