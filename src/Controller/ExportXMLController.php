<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\DevisSynchro;
use App\Entity\ClientSynchro;
use App\Entity\AdresseSynchro;
use App\Entity\ContactSynchro;
use App\Entity\Tiers;
use App\Entity\Adresse;
use App\Entity\Contact;
use App\Entity\Fonction;
use App\Entity\Activite;
use App\Entity\Pays;
use App\Service\ExportXML;

Class ExportXMLController extends Controller {
    /**
     * @Route("/xml-client", name="xml_client")
     */ 
    function xmlClients() {
    	$clients = $this->getDoctrine()->getRepository(ClientSynchro::class)->findBySynchro(0);

    	$exportService = new ExportXML;
    	$fs = new Filesystem();

    	$now = new \Datetime;

    	$pays=[];
    	foreach ($this->getDoctrine()->getRepository(Pays::class)->findAll() as $key => $entpays) {
    		$pays[$entpays->getCode()] = $entpays->getNom();
    	}

    	$fonctions=[];
    	foreach ($this->getDoctrine()->getRepository(Fonction::class)->findAll() as $key => $fonction) {
    		$fonctions[$fonction->getId()] = $fonction->getTitre();
    	}

		foreach ($clients as $key => $client) {
			//dump($client);
			if($client->getProfession()) $professionTitre = $this->getDoctrine()->getRepository(Activite::class)->findOneByCode($client->getProfession())->getTitre();


			$config = [
				"BPC0_1" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="BPCNUM" TYPE="Char"',
	    					"value" => empty($client->getCodeX3()) ? '': $client->getCodeX3()
	    				],
	    				[
	    					"attr" =>  'NAME="BPCNAM" TYPE="Char"',
	    					"value" => empty($client->getRaisonSociale()) ? '': $client->getRaisonSociale()
	    				],
	    				[
	    					"attr" =>  'NAME="UPDDAT" TYPE="Char"',
	    					"value" => $now->format('Ymd')
	    				]
	    			]
	    		],
				"BPRC_1" => [
	    			"type" => "GRP",
	    			"lst" => [
	    				"attr" =>  'NAME="BPRNAM" SIZE="1" TYPE="Char"',
	    				"content" => "<ITM>".empty($client->getRaisonSociale()) ? '': $client->getRaisonSociale()."</ITM>"
	    			],
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="CRN" TYPE="Char"',
	    					"value" => $client->getNumSiret()
	    				],
	    				[
	    					"attr" =>  'NAME="NAF" TYPE="Char"',
	    					"value" => $client->getCodeNaf()
	    				]
	    			]
	    		],
				"BPC2_2" => [
	    			"type" => "GRP",
					"fld" => [
	    				[
	    					"attr" =>  'NAME="WOSTAUZ" TYPE="Decimal"',
	    					"value" => $client->getEncours()
	    				],
	    				[
	    					"attr" =>  'NAME="XOST" TYPE="Decimal"',
	    					"value" => $client->getMaxEncours()
	    				]
	    			]
				],
				"BPC2_6" => [
	    			"type" => "GRP",
					"lst" => [	
							]
				],
				"BPC3_3" => [
	    			"type" => "GRP",
					"fld" => [
	    				[
	    					"attr" =>  'NAME="PTE" TYPE="Char"',
	    					"value" => $client->getCodeCondPaie()
	    				]
	    			]
				],
				"BPC4_1" => [
	    			"type" => "TAB",
	    			"attr" =>  'DIM="200" SIZE="1"',	    			
					"lin" => [
						[
							"attr" => 'NUM="1"',
							"fld" => [
								[
									"attr" =>  'NAME="EECNUM" TYPE="Char"',
									"value" => $client->getTvaIntracom()
								]
							]
						]
					]

				],
				"BIDC_1" => [
	    			"type" => "TAB",
	    			"attr" =>  'DIM="30" SIZE="1"',	    			
					"lin" => [
						[
							"attr" => 'NUM="1"',
							"fld" => [
			    				[
			    					"attr" =>  'NAME="IBAN" TYPE="Char"',
			    					"value" => empty($client->getIban()) ? '': $client->getIban()
			    				],
			    				[
			    					"attr" =>  'NAME="BICCOD" TYPE="Char"',
			    					"value" => empty($client->getBic()) ? '': $client->getBic()
			    				],
			    				[
			    					"attr" =>  'NAME="BIDNUM" TYPE="Char"',
			    					"value" => substr($client->getIban(), 3) ? '': substr($client->getIban(), 3)
			    				]
			    			]
						]
					]

				]

			];


			if($client->getProfession()) {
				$config["BPC2_6"]["lst"][] = [
											"attr" =>  'NAME="TSCCOD" SIZE="1" TYPE="Char"',
						    				"content" => "<ITM>".$client->getProfession()."</ITM>"
											];
			}

			if(isset($professionTitre)) {
				$config["BPC2_6"]["lst"][] = [
											"attr" =>  'NAME="ZTSCCOD" SIZE="1" TYPE="Char"',
						    				"content" => "<ITM>".$professionTitre."</ITM>"
											];
			}

			/**
			 *
			 * Récupération et ajout des adresses si elles existent
			 *
			 */
			
			// On commence par récupérer toutes les adresses issues de X3
			$adresses = [];
			$adressesX3 = $this->getDoctrine()->getManager()->getRepository(Adresse::class)->findBy(array('code_tiers_x3'=>$client->getCodeX3()), array());
			foreach ($adressesX3 as $keyAd => $adX3) {
				$adresses[$adX3->getCodeX3()] = [
					"ADDLIG1" => $adX3->getAddress1(),
					"ADDLIG2" => $adX3->getAddress2(),
					"ADDLIG3" => $adX3->getAddress3(),
					"POSCOD" => $adX3->getZip(),
					"CTY" => $adX3->getCity(),
					"SAT" => $adX3->getChantier(),
					"TEL1" => $adX3->getTelephone(),
					"TEL2" => $adX3->getPortable(),
					"TEL3" => $adX3->getFax(),
					"WEB1" => $adX3->getEmail(),
					"FCYWEB" => $adX3->getAddress3(),
					"EXTNUM" => $adX3->getAddress3()
				];

				if(!empty($adX3->getPaysCode())) {
					$adresses[$adX3->getCodeX3()]["BPACRY"] = $adX3->getPaysCode();
					$adresses[$adX3->getCodeX3()]["CRYNAM"] = $pays[$adX3->getPaysCode()];
				}
			}

			// On récupère ensuite les données à synchroniser pour modifier ou compléter la liste ci-dessus
			$adressesSynchro = $this->getDoctrine()->getManager()->getRepository(AdresseSynchro::class)->findBy(array('code_tiers_x3'=>$client->getCodeX3()), array());
			foreach ($adressesSynchro as $keyAd => $adX3) {
				$adresses[$adX3->getCodeX3()] = [
					"ADDLIG1" => $adX3->getAddress1(),
					"ADDLIG2" => $adX3->getAddress2(),
					"ADDLIG3" => $adX3->getAddress3(),
					"POSCOD" => $adX3->getZip(),
					"CTY" => $adX3->getCity(),
					"SAT" => $adX3->getChantier(),
					"TEL1" => $adX3->getTelephone(),
					"TEL2" => $adX3->getPortable(),
					"TEL3" => $adX3->getFax(),
					"WEB1" => $adX3->getEmail(),
					"FCYWEB" => $adX3->getAddress3(),
					"EXTNUM" => $adX3->getAddress3()
				];

				if(!empty($adX3->getPaysCode())) {
					$adresses[$adX3->getCodeX3()]["BPACRY"] = $adX3->getPaysCode();
					$adresses[$adX3->getCodeX3()]["CRYNAM"] = $pays[$adX3->getPaysCode()];
				}

				$adX3->setSynchro(1);
				$this->getDoctrine()->getManager()->persist($adX3);
			}

			// La liste des adresses est à jour, on l'intègre dans le XML
			if(count($adresses)) {
				$config["BPAC_1"] = [
					"type" => "TAB",
	    			"attr" =>  'DIM="200" SIZE="'.count($adresses).'"',	 
	    			"lin" => []
				];

				$i = 1;
				foreach ($adresses as $keyAd => $adresse) {
					$newField = [];
					$newField[] = [
		    					"attr" =>  'NAME="CODADR" TYPE="Char"',
		    					"value" => $keyAd
		    				];

		    		foreach ($adresse as $keyUnitAdress => $valueUnitAdress) {
		    			$newField[] = [
		    					"attr" =>  'NAME="'.$keyUnitAdress.'" TYPE="Char"',
		    					"value" => $valueUnitAdress
		    				];
		    		}
		    		$config["BPAC_1"]["lin"][] = [
		    										"attr" => 'NUM="'.$i.'"', 
		    										"fld" => $newField
		    									];
		    		$i++;
				}
			}


			/**
			 *
			 * Récupération et ajout des contacts
			 *
			 */
			// On commence par récupérer tous les contacts issus de X3
			$contacts = [];
			$contactsX3 = $this->getDoctrine()->getManager()->getRepository(Contact::class)->findBy(array('code_tiers_x3'=>$client->getCodeX3()), array());
			foreach ($contactsX3 as $keyCont => $contX3) {
				$contacts[$contX3->getCodeX3()] = [
					"CNTTTL" => $contX3->getCivilite(),
					"CNTLNA" => $contX3->getNom(),
					"CNTFNA" => $contX3->getPrenom(),
					"CNTFLAG" => $contX3->getIsDefault(),
					"CNTADD" => $contX3->getContactAddressCode(),
					"CNTFNC" => $contX3->getFonction(),
					"CNTSRV" => $contX3->getService(),
					"CNTWEB" => $contX3->getMailc(),
					"CNTTEL" => $contX3->getTelc(),
					"CNTMOB" => $contX3->getMobilec(),
					"CNTFAX" => $contX3->getFaxc()
				];
			}

			// On récupère ensuite les données à synchroniser pour modifier ou compléter la liste ci-dessus
			$contactsSynchro = $this->getDoctrine()->getManager()->getRepository(ContactSynchro::class)->findBy(array('code_tiers_x3'=>$client->getCodeX3()), array());
			foreach ($contactsSynchro as $keyCont => $contX3) {
				$contacts[$contX3->getCodeX3()] = [
					"CNTTTL" => $contX3->getCivilite(),
					"CNTLNA" => $contX3->getNom(),
					"CNTFNA" => $contX3->getPrenom(),
					"CNTFLG" => $contX3->getIsDefault(),
					"CNTADD" => $contX3->getContactAddressCode(),
					"CNTFNC" => $contX3->getFonction(),
					"CNTSRV" => $contX3->getService(),
					"CNTWEB" => $contX3->getMailc(),
					"CNTTEL" => $contX3->getTelc(),
					"CNTMOB" => $contX3->getMobilec(),
					"CNTFAX" => $contX3->getFaxc()
				];

				$contX3->setSynchro(1);
				$this->getDoctrine()->getManager()->persist($contX3);
			}

			// La liste des contacts est à jour, on l'intègre dans le XML
			if(count($contacts)) {
				$config["CNTC_1"] = [
					"type" => "TAB",
	    			"attr" =>  'DIM="100" SIZE="'.count($contacts).'"',	 
	    			"lin" => []
				];

				$i = 1;
				foreach ($contacts as $key => $contact) {
					$newField = [];

					$newField[] = [
		    					"attr" =>  'NAME="CCNCRM" TYPE="Char"',
		    					"value" => $key
		    				];

		    		foreach ($contact as $keyUnitContact => $valueUnitContact) {

						// Cas particulier des attributs XML qui ne sont pas des Char
						if($keyUnitContact== "CNTFLG") {
							$attr = 'MENULAB="Oui" MENULOCAL="'.($valueUnitContact=="1" ? 'Non':'Oui').'" NAME="CNTFLG" TYPE="Integer"';
						} elseif ($keyUnitContact== "CNTTTL") {
							switch ($valueUnitContact) {
								case 1:
									$texte = "Monsieur";
									break;
								case 2:
									$texte = "Madame";
									break;
								case 3:
									$texte = "Mademoiselle";
									break;
								
								default:
									$texte = "";
									break;
							}
							$attr = 'MENULAB="'.$texte.'" MENULOCAL="941" NAME="CNTTTL" TYPE="Integer"';
						} elseif ($keyUnitContact== "CNTFNC") {
							//dump($fonctions);
							$attr = 'MENULAB="'.$fonctions[$valueUnitContact].'" MENULOCAL="233" NAME="CNTFNC" TYPE="Integer"';
						} else {
							$attr = 'NAME="'.$keyUnitContact.'" TYPE="Char"';
						}

		    			$newField[] = [
		    					"attr" =>  $attr,
		    					"value" => $valueUnitContact
		    				];
		    		}

		    		$config["CNTC_1"]["lin"][] = [
		    										"attr" => 'NUM="'.$i.'"', 
		    										"fld" => $newField
		    									];
		    		$i++;
				}
			}


			// Change les valeurs en base pour indiquer que la synchro est ok
			$client->setSynchro(1);
			$this->getDoctrine()->getManager()->persist($client);


			$fileContent = $exportService->xmlFromArray($config);

	    	$now = new \Datetime;
	    	$fs->dumpFile('exports/clients/'.$now->format('YmdHis').'-'.$key.'.xml', $fileContent); 

			dump($fileContent);
		}
		$this->getDoctrine()->getManager()->flush();

    }


    /**
     * @Route("/xml-devis", name="xml_devis")
     */ 

    function xmlDevis() {
    	$devis = $this->getDoctrine()->getRepository(DevisSynchro::class)->findBySynchro(0);

    	$exportService = new ExportXML;
    	$fs = new Filesystem();

		foreach ($devis as $key => $value) {
			if($value->getClientCode()) {
				$client = $this->getDoctrine()->getRepository(Tiers::class)->findByCodeX3($value->getClientCode());

				if($value->getAdresseCode()) {
					$chantier = $this->getDoctrine()->getRepository(Adresse::class)->findOneBy(array("code_tiers_x3"=>$value->getClientCode(), "code_x3"=>$value->getAdresseCode()), array());
				}
			}


    		if($value->getEtat()) {
    			$etatNum = $value->getEtat();
				$etatTexte = "";
    			switch ($value->getEtat()) {
    				case '1':
    					$etatTexte = "Non commandé";
    					break;

    				case '2':
    					$etatTexte = "Partiellement commandé";
    					break;

    				case '3':
    					$etatTexte = "Totalement commandé";
    					break;

    				case '4':
    					$etatTexte = "Relancé par l'itinérant";
    					break;

    				case '5':
    					$etatTexte = "Relancé par sédentaire";
    					break;    				
	   			}
    		}

    		if($value->getStatut()) {
    			$statutNum = $value->getStatut();
    			$statutTexte = '';
    			switch ($value->getStatut()) {
					case '1':
						$statutTexte = "En cours";
						break;

					case '2':
						$statutTexte = "Gagné";
						break;

					case '3':
						$statutTexte = "Gagné partiel";
						break;

					case '4':
						$statutTexte = "Perdu";
						break;   				
	   			}
    		}

    		if($value->getPerte()) {
    			$perteNum = $value->getPerte();
    			$perteTexte = "";
    			switch ($value->getPerte()) {
					case '1':
						$perteTexte = "";
						break;

					case '2':
						$perteTexte = "Trop cher";
						break;

					case '3':
						$perteTexte = "Délai trop long";
						break;

					case '4':
						$perteTexte = "Produit non-conforme";
						break; 

					case '4':
						$perteTexte = "Autre";
						break;   				
	   			}
    		}


    		//dump();
    		
	    	$config = [
	    		"SQH0_1" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="SALFCY" TYPE="Char"',
	    					"value" => empty($value->getSiteVente()) ? '': $value->getSiteVente()
	    				],
	    				[
	    					"attr" =>  'NAME="SQHNUM" TYPE="Char"',
	    					"value" => strpos($value->getCode(), 'DV') ? $value->getCode(): ''
	    				],
	    				[
	    					"attr" =>  'NAME="CREDAT" TYPE="Date"',
	    					"value" => empty($value->getDate()) ? '': $value->getDate()->format('Ymd')
	    				],
	    				[
	    					"attr" =>  'NAME="CUSQUOREF" TYPE="Char"',
	    					"value" => ''
	    				],
	    				[
	    					"attr" =>  'NAME="QUODAT" TYPE="Date"',
	    					"value" => empty($value->getDate()) ? '': $value->getDate()->format('Ymd')
	    				],
	    				[
	    					"attr" =>  'NAME="BPCORD" TYPE="Char"',
	    					"value" => empty($value->getClientCode()) ? '': $value->getClientCode()
	    				],
	    				[
	    					"attr" =>  'NAME="BPCNAM" TYPE="Char"',
	    					"value" => isset($client) ? $client->getRaisonSociale() : ''
	    				],
	    				[
	    					"attr" =>  'NAME="CUR" TYPE="Char"',
	    					"value" => 'EUR'
	    				]
	    			] 
	    		], 
	    		"SQH1_1" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="BPAADD" TYPE="Char" ',
	    					"value" => isset($chantier) ? $chantier->getCodeX3():'' 
	    				],
	    				[
	    					"attr" =>  'NAME="BPDNAM" TYPE="Char" ',
	    					"value" => isset($chantier)? $chantier->getChantier():'' 
	    				]
	    				
	    			] 
	    		], 
	    		"SQH1_2" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="STOFCY" TYPE="Char" ',
	    					"value" => empty($value->getSiteVente()) ? '': $value->getSiteVente() 
	    				]	    				
	    			] 
	    		], 
	    		"SQH1_3" => [
	    			"type" => "GRP",
	    			"lst" => [
	    				"attr" =>  ' NAME="REP" SIZE="1" TYPE="Char" ',
	    				"content" => "<ITM>CHT</ITM>"
	    			],
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="YRED" TYPE="Char"',
	    					"value" => empty($value->getRedacteurCode()) ? '': $value->getRedacteurCode() 
	    				]
	    				
	    			]  
	    		], 
	    		"SQH1_5" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
	    					"attr" =>  'NAME="VLYDAT" TYPE="Date"',
	    					"value" => empty($value->getDate()) ? '': $value->getDate()->format('Ymd')
	    				],
	    				[
	    					"attr" =>  ' MENULAB="'.(isset($etatTexte) ? $etatTexte : '').'" MENULOCAL="430" NAME="QUOSTA" TYPE="Integer" ',
	    					"value" => isset($etatNum) ? $etatNum : ''
	    				],
	    				[
	    					"attr" =>  ' MENULAB="'.(isset($statutTexte) ? $statutTexte : '').'"  MENULOCAL="1000" NAME="YSTATDEV" TYPE="Integer" ',
	    					"value" => isset($statutNum) ? $statutNum : ''
	    				],
	    				[
	    					"attr" =>  ' MENULAB="'.(isset($perteTexte) ? $perteTexte : '').'"  MENULOCAL="1004" NAME="YMOTIF" TYPE="Integer" ',
	    					"value" => isset($perteNum) ? $perteNum : ''
	    				]
	    				
	    			] 
	    		],
	    		"SQH3_4" => [
	    			"type" => "TAB",
	    			"attr" => 'DIM="30" SIZE="1"',
	    			"lin" => [
			    				[
			    				//"attr" => 'NUM="1"',
			    				"fld" => [
			    					[
			    						"attr" => ' NAME="SHO" TYPE="Char" ',
			    						"value" => "Port et emb"
			    					],
			    					[
			    						"attr" => 'NAME="INVDTAAMT" TYPE="Decimal"',
			    						"value" => empty($value->getFraisTransport()) ? '0': $value->getFraisTransport()
			    					],
			    					[
			    						"attr" => 'MENULAB="HT" MENULOCAL="2227" NAME="INVDTATYP" TYPE="Integer"',
			    						"value" => "1"
			    					]
			    				]

	    					]

	    			]

	    		],
	    		"SQH2_3" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
    						"attr" => 'NAME="QUONOT" TYPE="Decimal"',
    						"value" => $value->getTotalDevis() - $value->getFraisTransport()
    					]
	    			]
	    		],
	    		"SQH2_4" => [
	    			"type" => "GRP",
	    			"fld" => [
	    				[
    						"attr" => 'NAME="QUOINVNOT" TYPE="Decimal"',
    						"value" => $value->getTotalDevis()
    					]
	    			]
	    		],
	    		"SQH2_1" => [
	    			"type" => "TAB",
	    			"attr" => 'DIM="900" ',

	    		]
			// <GRP ID="SQH2_3" >
			// 	<FLD NAME="QUONOT" TYPE="Decimal" >65.21</FLD>
			// </GRP>

	    	];

	    	$articles = [];
	    	try {
	    		if($value->getArticles())
	    			$articles = json_decode($value->getArticles());
	    	} catch(\Exception $e) {
	    		//dump($e->getMessage());
	    	}


	    	$config["SQH2_1"]["attr"].= 'SIZE="'.count($articles).'"';

	    	$lines = [];
	    	foreach ($articles as $keyArticle => $ligne) {
	    		$lines[] = [
	    			"attr" => 'NUM="'.$keyArticle.'"',
	    			"fld" => [
	    				[
	    					"attr" => ' NAME="ITMREF" TYPE="Char"',
	    					"value" => $ligne->code
	    				],
	    				[
	    					"attr" => ' NAME="ITMDES1" TYPE="Char"',
	    					"value" => $ligne->designation
	    				],
	    				[
	    					"attr" => ' NAME="SAU" TYPE="Char"',
	    					"value" => (isset($ligne->uv) && !empty($ligne->uv)) ? $lignes->uv : 'UN'
	    				],
	    				[
	    					"attr" => ' MENULAB="1" MENULOCAL="1003" NAME="ZPCMUF" TYPE="Integer"',
	    					"value" => isset($ligne->uf) ? $lignes->uf : '1'
	    				],
	    				[
	    					"attr" => ' NAME="QTY" TYPE="Decimal"',
	    					"value" => $ligne->quantite
	    				],
	    				[
	    					"attr" => ' NAME="ZPXTARDUOBAT" TYPE="Decimal"',
	    					"value" => $ligne->prix_tarif
	    				],
	    				[
	    					"attr" => ' NAME="ZPCMNETPRI" TYPE="Decimal"',
	    					"value" => $ligne->prix_net
	    				],
	    				[
	    					"attr" => ' NAME="DISCRGVAL1" TYPE="Decimal"',
	    					"value" => $ligne->remise
	    				]
	    			]
	    		];

	    	}
	    	$config["SQH2_1"]["lin"] = $lines;

	    	$fileContent = $exportService->xmlFromArray($config);
    		//dump($config);

	    	$now = new \Datetime;
	    	$fs->dumpFile('exports/devis/'.$now->format('YmdHis').'-'.$key.'.xml', $fileContent); 

	    	$value->setSynchro(1);
	    	$this->getDoctrine()->getManager()->persist($value);
    	}
    	$this->getDoctrine()->getManager()->flush();

    	return new Response('Export DEVIS terminé'); 
    }



}