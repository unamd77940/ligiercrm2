<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use App\Entity\Article;
use App\Entity\Devis;
use App\Entity\DevisImport;
use App\Entity\Facture;
use App\Entity\FactureImport;
use App\Entity\ArticleImport;
use App\Entity\AdresseSynchro;

class ImportController extends Controller 
{
    
    /**
     * @Route("/import-devis-{standby}", name="import_devis", requirements={"standby"="\d+"}, defaults={"standby"=0})
     */  
    public function importDevis($standby) {
        /**
         * Pour forcer l'import du CSV dans la base, lancer /import-devis
         * Pour lancer l'import en vérifiant que le worker n'est pas déjà en cours
         * lancer /import-devis-300 
         * (vérification du nombre d'entrées à T et T+300sec. Si le nombre est différent, le script est déjà en cours)
         */

        $fileName = 'DEVIS.CSV';
        $fileNameLignes = 'LIGNESDEVIS.CSV';
        
        $directory = $this->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        $em = $this->getDoctrine()->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);

        if($standby) {
            // Vérifie que la table devis_import n'est pas déjà en cours de modif
            $count = $em->getRepository(DevisImport::class)->countEntries();
            sleep($standby);
            $count2 = $em->getRepository(DevisImport::class)->countEntries();
            if($count>$count2) return new Response("Import devis en cours");
        }


        // Crée une table temporaire des lignes de devis
        /*   
                
        */


        if(is_readable($directory.$fileNameLignes)){
            $fileContents = file_get_contents($directory.$fileNameLignes);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($directory.$fileNameLignes,utf8_encode($fileContents));
            }
        }


        
        $sql = "DROP TABLE IF EXISTS lignesdevis;
                
                CREATE TABLE lignesdevis ( 
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                    code_devis VARCHAR(25) NOT NULL , 
                    code VARCHAR(25) NOT NULL ,  
                    site VARCHAR(15) NOT NULL , 
                    quantite DOUBLE NOT NULL , 
                    prix_tarif DOUBLE NOT NULL , 
                    remise DOUBLE NOT NULL , 
                    prix_net DOUBLE NOT NULL ,
                    prix_revient DOUBLE NOT NULL ,
                    marge DOUBLE NOT NULL 
                );
                ALTER TABLE lignesdevis ADD INDEX(code_devis);

                LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '".$directory.$fileNameLignes."'
                INTO TABLE lignesdevis
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code_devis,
                    @LIGNE,
                    code,
                    site,
                    quantite,
                    prix_tarif,
                    remise,
                    prix_net,
                    prix_revient,
                    marge,
                    @REP1,
                    @REP2
                )
                ";
        try {
            $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $stmt->execute();
            $stmt->closeCursor();
            var_dump('<pre>'.$sql.'</pre>');

        } catch(\Exception $e) {
            var_dump($e->getReponse());
            exit;
        }

        sleep(3);


        if(is_readable($directory.$fileName)){
            $fileContents = file_get_contents($directory.$fileName);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($directory.$fileName,utf8_encode($fileContents));
            }
        }


        $sql = "TRUNCATE TABLE devis_import;
                LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '".$directory.$fileName."'
                INTO TABLE devis_import
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code,
                    client_code,
                    @date,
                    @REP1,
                    @REP2,
                    commentaires,
                    ref,
                    adresse_code,
                    site_vente,
                    site_expedition,
                    total_ht,
                    etat,
                    @datevalidite,
                    statut,
                    perte,
                    observations,
                    redacteur_code

                )
                SET date = DATE_FORMAT(@date, '%Y%m%d'), 
                date_validite = DATE_FORMAT(@datevalidite, '%Y%m%d'),
                commentaires = REPLACE(commentaires, '||', '')
                ";

        try {
            $stmt = $connexion->executeQuery($sql);
            var_dump('<pre>'.$sql.'</pre>');

        } catch(\Exception $e) {
            var_dump($e->getReponse());
            exit;
        }

        return new Response('Import du CSV devis terminé');
    }



    
    /**
     * @Route("/import-facture-{standby}", name="import_facture", requirements={"standby"="\d+"}, defaults={"standby"=0})
     */  
    public function importFacture($standby) {
        /**
         * Pour forcer l'import du CSV dans la base, lancer /import-facture
         * Pour lancer l'import en vérifiant que le worker n'est pas déjà en cours
         * lancer /import-facture-300 
         * (vérification du nombre d'entrées à T et T+300sec. Si le nombre est différent, le script est déjà en cours)
         */

        $fileName = 'FACTURES.CSV';
        $fileNameLignes = 'LIGNESFACTURES.CSV';
        
        $directory = $this->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        $em = $this->getDoctrine()->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);

        if($standby) {
            // Vérifie que la table facture_import n'est pas déjà en cours de modif
            $count = $em->getRepository(FactureImport::class)->countEntries();
            sleep($standby);
            $count2 = $em->getRepository(FactureImport::class)->countEntries();
            if($count>$count2) return new Response("Import facture en cours");
        }


        $fileContents = file_get_contents($directory.$fileNameLignes);        
        //Ré-encode en UTF8
        if (!mb_check_encoding($fileContents, 'UTF-8')) {
            $fileContents = utf8_encode($fileContents);
        }
        $fileContents = str_replace('\r"",0,"",0,0,0,0,0,0,0,0', '', $fileContents);
        $fileContents = str_replace('\n"",0,"",0,0,0,0,0,0,0,0', '', $fileContents);
        file_put_contents($directory.$fileNameLignes, $fileContents);


        // Crée une table temporaire des lignes de facture
        /*   
                
        */
        $sql = "DROP TABLE IF EXISTS lignesfacture;
                
                CREATE TABLE lignesfacture ( 
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                    code_facture VARCHAR(25) NOT NULL , 
                    code VARCHAR(25) NOT NULL ,  
                    quantite DOUBLE NOT NULL , 
                    prix_tarif DOUBLE NOT NULL , 
                    remise DOUBLE NOT NULL , 
                    prix_net DOUBLE NOT NULL ,
                    prix_revient DOUBLE NOT NULL ,
                    marge DOUBLE NOT NULL 
                );
                ALTER TABLE lignesfacture ADD INDEX(code_facture);

                LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '".$directory.$fileNameLignes."'
                INTO TABLE lignesfacture
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code_facture,
                    @LIGNE,
                    code,
                    quantite,
                    prix_tarif,
                    remise,
                    prix_net,
                    prix_revient,
                    marge,
                    @REP1,
                    @REP2
                )
                ";
        try {
            $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $stmt->execute();
            $stmt->closeCursor();
            var_dump('<pre>'.$sql.'</pre>');

        } catch(\Exception $e) {
            var_dump($e->getReponse());
            exit;
        }

        sleep(3);


        $sql = "TRUNCATE TABLE facture_import;
                LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '".$directory.$fileName."'
                INTO TABLE facture_import
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code,
                    site_vente, 
                    type, 
                    @date,
                    @devise,
                    client_code,
                    total_ht,
                    @REP1,
                    @REP2
                )
                SET date = DATE_FORMAT(@date, '%Y%m%d')
                ";

        try {
            $stmt = $connexion->executeQuery($sql);
            var_dump('<pre>'.$sql.'</pre>');

        } catch(\Exception $e) {
            var_dump($e->getReponse());
            exit;
        }

        return new Response('Import du CSV facture terminé');
    }

    
    /**
     * @Route("/import-commande-{standby}", name="import_commande", requirements={"standby"="\d+"}, defaults={"standby"=0})
     */  
    public function importCommandes($standby) {
        /**
         * Pour forcer l'import du CSV dans la base, lancer /import-commande
         * Pour lancer l'import en vérifiant que le worker n'est pas déjà en cours
         * lancer /import-commande-300 
         * (vérification du nombre d'entrées à T et T+300sec. Si le nombre est différent, le script est déjà en cours)
         */

        $fileName = 'COMMANDES.CSV';
        $fileNameLignes = 'LIGNESCOMMANDES.CSV';
        
        $directory = $this->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        $em = $this->getDoctrine()->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);

        if($standby) {
            // Vérifie que la table commande_import n'est pas déjà en cours de modif
            $count = $em->getRepository(CommandeImport::class)->countEntries();
            sleep($standby);
            $count2 = $em->getRepository(CommandeImport::class)->countEntries();
            if($count>$count2) return new Response("Import commande en cours");
        }


        // Crée une table temporaire des lignes de commandes
        /*   
                
        */
        $sql = "DROP TABLE IF EXISTS lignescommandes;
                
                CREATE TABLE lignescommandes ( 
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                    code_commande VARCHAR(25) NOT NULL , 
                    code VARCHAR(25) NOT NULL , 
                    designation VARCHAR(255) NULL , 
                    site VARCHAR(15) NOT NULL , 
                    quantite INT NOT NULL , 
                    reliquat INT NOT NULL , 
                    prix_tarif DOUBLE NOT NULL , 
                    remise TINYINT NOT NULL , 
                    prix_net DOUBLE NOT NULL 
                );
                ALTER TABLE lignescommandes ADD INDEX(code_commande);

                LOAD DATA LOCAL INFILE '".$directory.$fileNameLignes."'
                INTO TABLE lignescommandes
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code_commande,
                    @LIGNE,
                    code,
                    site,
                    quantite,
                    reliquat,
                    prix_tarif,
                    remise,
                    prix_net,
                    @REVIENT,
                    @MARGE,
                    @REP1,
                    @REP2
                )
                ";
        try {
            $stmt = $connexion->prepare($sql, array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
            $stmt->execute();
            $stmt->closeCursor();

        } catch(\Exception $e) {
            exit;
        }

        sleep(3);




        $sql = "TRUNCATE TABLE commande_import;
                LOAD DATA LOCAL INFILE '".$directory.$fileName."'
                INTO TABLE commande_import
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code, 
                    @type,
                    client_code,
                    @date,
                    @REP1,
                    @REP2,
                    commentaires,
                    ref,
                    mode_livraison,
                    adresse_code,
                    site_vente,
                    site_expedition,
                    frais_transport,
                    total_ht,
                    statut
                )
                SET date = DATE_FORMAT(@date, '%Y%m%d'), 
                date_creation = DATE_FORMAT(@date, '%Y%m%d')
                ";

        try {
            $stmt = $connexion->executeQuery($sql);
        } catch(\Exception $e) {
            exit;
        }

        return new Response('Import du CSV commande terminé');
    }



    /**
     * @Route("/import-article-{standby}", name="import_article", requirements={"standby"="\d+"}, defaults={"standby"=0})
     */  
    public function importArticles($standby) {
        /**
         * Pour forcer l'import du CSV dans la base, lancer /import-article
         * Pour lancer l'import en vérifiant que le worker n'est pas déjà en cours
         * lancer /import-article-300 
         * (vérification du nombre d'entrées à T et T+300sec. Si le nombre est différent, le script est déjà en cours)
         */

        $fileName = 'ARTICLES.CSV';
        
        $directory = $this->getParameter('importdirectory');
        $directory = str_replace("\\", "/",$directory);

        dump($directory.$fileName);

        $em = $this->getDoctrine()->getManager();
        $connexion = $em->getConnection();
        $connexion->getConfiguration()->setSQLLogger(null);

        if($standby) {
            // Vérifie que la table commande_import n'est pas déjà en cours de modif
            $count = $em->getRepository(ArticleImport::class)->countEntries();
            sleep($standby);
            $count2 = $em->getRepository(ArticleImport::class)->countEntries();
            if($count>$count2) return new Response("Import commande en cours");
        }



        if(is_readable($directory.$fileName) ){
            $fileContents = file_get_contents($directory.$fileName);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($directory.$fileName,utf8_encode($fileContents));
            }
        }

        $sql = "TRUNCATE TABLE article_import;
                LOAD DATA LOCAL INFILE '".$directory.$fileName."'
                INTO TABLE article_import
                CHARACTER SET UTF8
                FIELDS TERMINATED BY ',' ENCLOSED BY '\"' 
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                (
                    code_x3, 
                    site,
                    designation,
                    fournisseur,
                    uv,
                    uf,
                    cdt,
                    @PRIXHT,
                    prix_tarif,
                    prix_revient_rennes,
                    prix_revient_lille,
                    tenue,
                    stock_interne_a
                )
                
                ";

        try {
            $stmt = $connexion->executeQuery($sql);
        } catch(\Exception $e) {
            return new Response($e->getMessage());;
        }

        return new Response('Import du CSV articles terminé');
    }
}