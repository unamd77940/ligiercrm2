<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Entity\ConditionPaiement;
use App\Entity\Activite;
use App\Entity\Devis;
use App\Entity\DevisSynchro;
use App\Entity\Facture;
use App\Entity\Commande;
use App\Entity\Memo;
use App\Entity\Tiers;
use App\Entity\Smsmail;
use App\Entity\Article;
use App\Entity\Vehicule;
use App\Entity\Parametres;
use App\Entity\Profil;
use App\Entity\ProfilFonction;
use App\Entity\ParametresValeur;
use App\Entity\Agence;
use App\Entity\Distributeur;
use App\Entity\Pays;
use App\Entity\Codepostaux;
use App\Entity\Fonction;
use App\Entity\ClientSynchro;
use App\Entity\CommandeSynchro;
use App\Entity\CommandeImport;
use App\Entity\AdresseSynchro;
use App\Entity\Adresse;
use App\Entity\AdresseGeoloc;
use App\Entity\ContactSynchro;
use App\Entity\Sav;
use App\Entity\Enlevement;
use App\Entity\LogSynchro;
use App\Entity\User;
use App\Entity\SiteVente;
use App\Service\Offline;

//EDE 141118 - Pour un nouvel objet. Ajouter :
//Une fois la SGBD modifiée
// Creation de l'Entity                                         Ok smsmail
// Creation du repository                                       Ok smsmail
// Creation du Twig                                             Ok smsmail
// ajouts Get_fonction puis, fn synchro-ynchrofichiers main.js
// Ajout lien dans FrontController                              Ok smsmail
// dans ce fichier :
// appel à l'entity                                             Ok smsmail
// Ajout route                                                  Ok smsmail
// Ajout bloc récup dans la fonction     generatejson           Ok smsmail
// Ajout bloc gestion nouvelobjet dans   fonction synchro       Ok smsmail


class FrontController extends Controller 
{
    
    /**
     * @Route("/", name="identification")
     */   
    public function identification(Request $request) {
        $sent = [];

        $em = $this->getDoctrine()->getManager();

        $commerciaux = ["ALG", "CCG", "CHT", "CONF", "CRL", "CRN", "CRS", "CSS", "CSY", "DIV", "DLE", "DLL", "DRY", "FDR", "FFO", "FJD", "FRD", "GDY", "GME", "GPR", "HRS", "JDS", "JLN", "JRR"];

        foreach ($commerciaux as $key => $value) {
            $test = $em->getRepository(User::class)->findOneByCode($value);
            if(!$test) {
                $user = new User;
                $user->setCode($value);
                $user->setPassword(sha1("123"));
                $em->persist($user);
                $em->flush();
            }
        }



        // Vérifie l'existence du commercial
        if($request->request->get('login') && $request->request->get('password')) {
            $params = [
                        "code"=> $request->request->get('login'),
                        "password"=> sha1($request->request->get('password'))
                     ];
            $user = $em->getRepository(User::class)->findOneBy($params);

            if($user) {
                // Enregistre en base le contenu du localStorage actuel
                $log = new LogSynchro;
                $log->setType('login_'.$request->request->get('login'));
                $log->setStorage($request->request->get('storage'));
                $em->persist($log);
                $em->flush();   
                $sent['user']['code'] = $user->getCode();
                $sent['user']['nom'] = $user->getNom();
                $sent['user']['prenom'] = $user->getPrenom();
                if(!empty($user->getSite())) {
                    $sent['user']['site'] = $user->getSite();
                }
                $sent=[];
                
        $repoclients = $this->getDoctrine()->getRepository(Tiers::class);
        $clients = $repoclients->findBy([
            'rep_gestion' => "DLE",
        ]);
            
        $repo = $this->getDoctrine()->getRepository(Smsmail::class);
        $mails = $repo->findAll();
        return $this->render('front/home.html.twig', ['smsmail' => $mails, 'clients' => $clients ]);

            } else $sent['error'] = "Erreur dans le code commercial ou le mot de passe";
        } 
        

        return $this->render('front/identification.html.twig', $sent);
    }

    /**
     * @Route("/{_locale}/accueil", name="home", 
    
     * )
     */   
    public function home() 
    {
           
        $sent=[];

        $repoclients = $this->getDoctrine()->getRepository(Tiers::class);
        $clients = $repoclients->findBy([
            'rep_gestion' => "DLE",
        ]);
            
        $repo = $this->getDoctrine()->getRepository(Smsmail::class);
        $mails = $repo->findAll();
        return $this->render('front/home.html.twig', ['smsmail' => $mails, 'clients' => $clients ]);

    }
    
    /**
     * @Route("/{_locale}/parametrage",  name="parametrage")
     */  
    public function parametrage() {
      
        $repo = $this->getDoctrine()->getRepository(Parametres::class);
        $param = $repo->findAll();
        
        $repo2 = $this->getDoctrine()->getRepository(ParametresValeur::class);
        $paramval = $repo2->findAll();
         
        return $this->render('front/parametrages.html.twig', 
                 ['parametres' => $param, 
                    'parametresvaleur' => $paramval
                ]);
                
    }


    /**
     * @Route("/{_locale}/administration",  name="administration")
     */  
    public function administration() {
      
        $repo = $this->getDoctrine()->getRepository(Parametres::class);
        $param = $repo->findAll();
        
        $repocp = $this->getDoctrine()->getRepository(Codepostaux::class);
        $paracp = $repocp->findAll();
        
        $repo2 = $this->getDoctrine()->getRepository(ParametresValeur::class);
        $paramval = $repo2->findAll();
        
        $repo3 = $this->getDoctrine()->getRepository(Distributeur::class);
        $paramdistri = $repo3->findOneById(1);//ajouter lien Id avec user connecté
        
        $repo4 = $this->getDoctrine()->getRepository(Agence::class);
        $paramagence = $repo4->findAll();
        
        $repo5 = $this->getDoctrine()->getRepository(User::class);
        $paramusers = $repo5->findAll();
       
        $repo6 = $this->getDoctrine()->getRepository(Profil::class);
        $paramprofil = $repo6->findAll();
        
        $repo7 = $this->getDoctrine()->getRepository(ProfilFonction::class);
        $paramprofilfonction = $repo7->findAll();
        
        return $this->render('front/administration.html.twig', 
                 ['parametres' => $param, 
                    'parametresvaleur' => $paramval,
                    'distributeur' => $paramdistri,
                    'agence' => $paramagence,
                    'user' => $paramusers,
                    'profil' => $paramprofil,
                    'profilfonction' => $paramprofilfonction,
                    'codepostaux' => $paracp]);
                
    }

    /**
     * @Route("/{_locale}/admin-users/{id}",  name="admin_users")
     */  
    public function administrationusers($id) {
   
        $repo8 = $this->getDoctrine()->getRepository(User::class);
        $paramusers = $repo8->findOneById($id);
       
        $repo9 = $this->getDoctrine()->getRepository(Distributeur::class);
        $paramdistri = $repo9->findAll();
        
        $repo10 = $this->getDoctrine()->getRepository(Agence::class);
        $paramagence = $repo10->findAll();
        
        $repo11 = $this->getDoctrine()->getRepository(Profil::class);
        $paramprofil = $repo11->findAll();
        
        return $this->render('front/administration-users.html.twig', 
                 ['user' => $paramusers,
                     'distributeur' => $paramdistri,
                     'agence' => $paramagence,
                     'profil' => $paramprofil]);
                
    }

    /**
     * @Route("/{_locale}/admin-agence/{id}",  name="admin_agence")
     */  
    public function administrationagence($id) {
   
        $repo8 = $this->getDoctrine()->getRepository(Agence::class);
        $paramagence = $repo8->findOneById($id);
       
        $repocp = $this->getDoctrine()->getRepository(Codepostaux::class);
        $paracp = $repocp->findAll();
        
        return $this->render('front/administration-agence.html.twig', 
                 [
                     'agence' => $paramagence,
                     'codepostaux' => $paracp]);
                
    }
    
    /**
     * @Route("/{_locale}/admin-profil/{id}",  name="admin_profil")
     */  
    public function administrationprofil($id) {
   
        $repo8 = $this->getDoctrine()->getRepository(Profil::class);
        $paramprofil = $repo8->findOneById($id);
       
        
        
        return $this->render('front/administration-profil.html.twig', 
                 [
                     'profil' => $paramprofil]);
                
    }
    
    /**
     * @Route("/{_locale}/admin-profilfct/{id}",  name="admin_profilfct")
     */  
    public function administrationprofilfct($id) {
   
        $repo8 = $this->getDoctrine()->getRepository(ProfilFonction::class);
        $paramprofilfct = $repo8->findOneById($id);
       
        $repo11 = $this->getDoctrine()->getRepository(Profil::class);
        $paramprofil = $repo11->findAll();
        
        return $this->render('front/administration-profilfct.html.twig', 
                 [
                     'profilfonction' => $paramprofilfct,
                     'profil' => $paramprofil]);
                
    }
    
    /**
     * @Route("/{_locale}/geolocalisation", name="geolocalisation")
     */  
    public function geolocalisation() {
        


        return $this->render('front/geolocalisation.html.twig');
    }

    /**
     * @Route("/clients", name="client_listing")
     */  
    public function clientListing() {


        return $this->render('front/client-listing.html.twig');
    }

    /**
     * @Route("/{_locale}/smsmail", name="smsmail_listing")
     */  
    public function smsmailListing() {


        return $this->render('front/smsmail-listing.html.twig');
    }
    /**
     * @Route("/{_locale}/smsmail-fiche", name="smsmail_edit")
     */  
    public function smsmailEdit() 
    {
            $sent=[];
    
        return $this->render('front/smsmail-edit.html.twig', $sent);
    }

    /**
     * @Route("/{_locale}/vehicule", name="vehicule_listing")
     */  
    public function vehiculeListing() 
    {    
        /*if (!$client)
        {
            $client = "C0";
            $repo = $this->getDoctrine()->getRepository(Vehicule::class);
            $vehicule = $repo->findAll();
            return $this->render('front/vehicule-listing.html.twig', 
                ['vehicule' => $vehicule]);           
        }
        */
            return $this->render('front/vehicule-listing.html.twig'); 
                
    }

    
    /**
     * @Route("/vehicule-fiche/{id}", name="vehicule_edit")
     */  
    public function vehiculeEdit($id) 

    {
    $repo = $this->getDoctrine()->getRepository(Vehicule::class);
    $vehicule = $repo->find($id);
    return $this->render('front/vehicule-edit.html.twig', 
            ['vehicule' => $vehicule]);

    }
   
    /**
     * @Route("/client-fiche/{codex3}", name="client_edit")
     */  
    public function clientEdit($codex3) {
        $repo = $this->getDoctrine()->getRepository(Tiers::class);
        $client = $repo->findByCodex3($codex3);
        
        $repo2 = $this->getDoctrine()->getRepository(Vehicule::class);
        $vehicules = $repo2->findByIdclient($codex3);

        $repo1 = $this->getDoctrine()->getRepository(Memo::class);
        $memos = $repo1->findByClientprospect($codex3);
        
        $repo3 = $this->getDoctrine()->getRepository(Devis::class);
        $devis = $repo3->findByClientcode($codex3);
        
        $repo4 = $this->getDoctrine()->getRepository(ParametresValeur::class);
        $param = $repo4->findByIdparam(10);
        
        
        return $this->render('front/client-edit.html.twig', 
                 ['clients' => $client,
                    'vehicule' => $vehicules,
                    'memo' => $memos,
                    'devis' => $devis,
                    'parametresvaleur' => $param

                     
]);
    }
    
    
    /**
     * @Route("/client-fiche-vehicule/{codex3}", name="client_edit_vehicule")
     */  
    public function clientEditvehicule($codex3) {
        $repo = $this->getDoctrine()->getRepository(Tiers::class);
        $client = $repo->findByCodex3($codex3);
        
        $repo2 = $this->getDoctrine()->getRepository(Vehicule::class);
        $vehicules = $repo2->findByIdclient($codex3);

        return $this->render('front/client-edit-vehicule.html.twig', 
                 ['clients' => $client, 'vehicule' => $vehicules ]);
        
      
        
        //Ajouter le repo pour obtenir la liste des vehicules///

    }
    
    /**
     * @Route("/tache-client/{id}", name="tache_client")
     */  
    public function tacheclient($id) {
        $repo = $this->getDoctrine()->getRepository(Memo::class);
        $memo = $repo->findById($id);
        
        
        return $this->render('front/tache-client.html.twig', 
                 ['memo' => $memo]);
        
      
    }
    /**
     * @Route("/demarche-client/{id}", name="demarche_client")
     */  
    public function demarcheclient($id) {
        
        $repo = $this->getDoctrine()->getRepository(Devis::class);
        $devis = $repo->findById($id);
        
        $repo2 = $this->getDoctrine()->getRepository(Vehicule::class);
        $vehicule = $repo->findById($id);
        
        return $this->render('front/demarche-client.html.twig', 
                 ['devis' => $devis,
                     'vehicule' => $vehicule]);
        
      
        
        //Ajouter le repo pour obtenir la liste des vehicules///

    }
    /**
     * @Route("/vehicule-client/{idclient}", name="vehicule_listing_client")
     */  
    public function vehiculeListingClient($idclient) 
    {    
        
            $repo = $this->getDoctrine()->getRepository(Vehicule::class);
            $vehicules = $repo->findByIdclient($idclient);
                  
            return $this->render('front/vehicule-listing-client.html.twig', 
                ['vehicule' => $vehicules]);           
       
    }
    
   
    /**
     * @Route("/client-commande-liste", name="client_commande")
     */  
    // , requirements={"id"="\d+"}
    //public function clientEdit($id) {
    public function clientCommandeListing() {
        $sent=[];
        
        
        return $this->render('front/client-commande-listing.html.twig', $sent);
    }


    /**
     * @Route("/stock", name="stock")
     */  
    public function stock() {
        $sent=[];
        
        
        return $this->render('front/stock.html.twig', $sent);
    }



    /**
     * @Route("/devis-listing", name="devis_listing")
     */  
    public function devisListing() {
        $sent=[];
        
        
        return $this->render('front/devis-listing.html.twig', $sent);
    }



    /**
     * @Route("/devis", name="devis")
     */  
    public function devis() {
        $sent=[];
        
        
        return $this->render('front/devis-edit.html.twig', $sent);
    }



    /**
     * @Route("/commande-listing", name="commande_listing")
     */  
    public function commandeListing() {
        $sent=[];
        
        
        return $this->render('front/commande-listing.html.twig', $sent);
    }


    /**
     * @Route("/commande", name="commande")
     */  
    public function commande() {
        $sent=[];
        
        
        return $this->render('front/commande-edit.html.twig', $sent);
    }




    /**
     * @Route("/prospect-listing", name="prospect_listing")
     */  
    public function prospectListing() {
        $sent=[];
        
        return $this->render('front/prospect-listing.html.twig', $sent);
    }

    /**
     * @Route("/prospect", name="prospect")
     */  
    public function prospect() {
        $sent=[];        
        
        return $this->render('front/prospect-edit.html.twig', $sent);
    }




    /**
     * @Route("/facture-listing", name="facture_listing")
     */  
    public function factureListing() {
        $sent=[];
        
        return $this->render('front/facture-listing.html.twig', $sent);
    }

    /**
     * @Route("/facture", name="facture")
     */  
    public function facture() {
        $sent=[];        
        
        return $this->render('front/facture-edit.html.twig', $sent);
    }




    /**
     * @Route("/memo-listing", name="memo_listing")
     */  
    public function memoListing() {
        $sent=[];
        
        return $this->render('front/memo-listing.html.twig', $sent);
    }

    /**
     * @Route("/memo", name="memo")
     */  
    public function memo() {
        $sent=[];        
        
        return $this->render('front/memo-edit.html.twig', $sent);
    }

    /**
     * @Route("/agenda", name="agenda")
     */  
    public function agenda() {
        $sent=[];        
        
        return $this->render('front/agenda.html.twig', $sent);
    }





    /**
     * @Route("/sav-listing", name="sav_listing")
     */  
    public function savListing() {
        $sent=[];
        
        return $this->render('front/sav-listing.html.twig', $sent);
    }

    /**
     * @Route("/sav", name="sav")
     */  
    public function sav() {
        $sent=[];        
        
        return $this->render('front/sav-edit.html.twig', $sent);
    }

    /**
     * @Route("/demande-enlevement", name="enlevement")
     */  
    public function enlevement() {
        $sent=[];        
        
        return $this->render('front/enlevement-edit.html.twig', $sent);
    }


    /**
     * @Route("/synchro", name="user_synchro")
     */  
    public function userSynchro() {
        $sent=[];        
        
        return $this->render('front/synchro.html.twig', $sent);
    }



    /**
     * @Route("/ajax/generate-json", name="generate_json")
     */ 
    public function generateJson(Request $request, SessionInterface $session) {
        $fichiergeoloc = fopen('fichiertest.txt', 'w+');

        $fs = new Filesystem();
        $date = new \DateTime;
        $dateFile = $date->format('Ymdhis');
        $filesNames = []; // Ce tableau va stocker les noms des fichiers json à utiliser
        $em = $this->getDoctrine()->getManager();
        $commercial = $request->request->get('commercial');
        //$commercial = "DLE";
        $clients = [];
        // $tiers = $em->getRepository(Tiers::class)->findBy(["rep_gestion"=> $commercial, "type_tiers" => "C"]);

        // Récupère les VEHICULES et les intègre dans un fichier
        $vehicules = $em->getRepository(Vehicule::class)->getVehicules();
        $fileVehicules = $commercial.'_'.$dateFile.'_vehicules.json';
        try {
            $fs->dumpFile('json/'.$fileVehicules, json_encode($vehicules)); 
            $filesNames['vehicule'] = $fileVehicules;         
        } catch(Exception $e) {

        }
        
        // Récupère les SMS et mails et les intègre dans un fichier
        $smsmail = $em->getRepository(Smsmail::class)->getSmsmail();
        $fileSmsmail = $commercial.'_'.$dateFile.'_smsmail.json';
        try {
            $fs->dumpFile('json/'.$fileSmsmail, json_encode($smsmail)); 
            $filesNames['smsmail'] = $fileSmsmail;         
        } catch(Exception $e) {

        }
        // Récupère les contacts et les intègre dans un fichier
        $tiers = $em->getRepository(Tiers::class)->getTiersInfo($commercial);
                    //fwrite($fichiergeoloc, json_encode($tiers[3])."\n");
        $fileClients = $commercial.'_'.$dateFile.'_client.json';
        try {
            $fs->dumpFile('json/'.$fileClients, json_encode($tiers)); 
           //EDE 

            $filesNames['clients'] = $fileClients;         
        } catch(Exception $e) {

        }

        // Récupère les Informations nécessaires à la géolocalisation dans les adresses
        $adressegeoloc = $em->getRepository(AdresseGeoloc::class)->getAdresseGeoloc();
        fwrite($fichiergeoloc, json_encode($adressegeoloc)."\n");
        $fileAdresseGeoloc = $commercial.'_'.$dateFile.'_adressegeoloc.json';
        try {
            $fs->dumpFile('json/'.$fileAdresseGeoloc, json_encode($adressegeoloc)); 
           //EDE 

            $filesNames['adresse'] = $fileAdresseGeoloc;         
        } catch(Exception $e) {

        }
        
        // Récupère les SAV et les intègre dans un fichier
        $sav = $em->getRepository(Sav::class)->getSavForClients($tiers);
        $fileSav = $commercial.'_'.$dateFile.'_or.json';
        try {
            $fs->dumpFile('json/'.$fileSav, json_encode($sav)); 
            $filesNames['ors'] = $fileSav;         
        } catch(Exception $e) {

        }

        // Récupère les ARTICLES et les intègre dans un fichier
        $article = $em->getRepository(Article::class)->getArticles();
        $fileArticle = $commercial.'_'.$dateFile.'_article.json';
        try {
            $fs->dumpFile('json/'.$fileArticle, json_encode($article)); 
            $filesNames['articles'] = $fileArticle;         
        } catch(Exception $e) {

        }
        

        // Récupère les MEMO et les intègre dans un fichier
        $memo = $em->getRepository(Memo::class)->getMemoForClients($tiers);
        $fileMemo = $commercial.'_'.$dateFile.'_memo.json';
        // try {
            $fs->dumpFile('json/'.$fileMemo, json_encode($memo)); 
            $filesNames['memos'] = $fileMemo;         
        // } catch(Exception $e) {

        // }
        

        // Récupère les DEVIS et les intègre dans un fichier
        //$memo = $em->getRepository(Memo::class)->getMemoForClients($tiers);
        $fileDevis = $commercial.'_'.$dateFile.'_devis.json';
        $devis = $em->getRepository(Devis::class)->getDevisForClients($tiers);

        try {
            $fs->dumpFile('json/'.$fileDevis, json_encode($devis)); 
            $filesNames['devis'] = $fileDevis;         
        } catch(Exception $e) {

        }
        

        // Récupère les FACTURES et les intègre dans un fichier
        $fileFactures = $commercial.'_'.$dateFile.'_factures.json';
        $factures = $em->getRepository(Facture::class)->getFactureForClients($tiers);

        try {
            $fs->dumpFile('json/'.$fileFactures, json_encode($factures)); 
            $filesNames['factures'] = $fileFactures;         
        } catch(Exception $e) {

        }
        

        // Récupère les PAYS et les intègre dans un fichier
        $pays = $em->getRepository(Pays::class)->getAllPays();
        try {
            $fs->dumpFile('json/pays.json', json_encode($pays)); 
            $filesNames['pays'] = "pays.json";         
        } catch(Exception $e) {

        }
        

        // Récupère les COMMANDES et les intègre dans un fichier
        //$memo = $em->getRepository(Memo::class)->getMemoForClients($tiers);
        $fileCommande = $commercial.'_'.$dateFile.'_commande.json';
        $commande = $em->getRepository(Commande::class)->getCommandeForClients($tiers);

        try {
            $fs->dumpFile('json/'.$fileCommande, json_encode($commande)); 
            $filesNames['commandes'] = $fileCommande;         
        } catch(Exception $e) {

        }
        
        // Récupère les parametres et les intègre dans un fichier
        $parametres = $em->getRepository(Parametres::class)->getAllParametres();
        try {
            $fs->dumpFile('json/parametres.json', json_encode($parametres)); 
            $filesNames['parametres'] = "parametres.json";         
        } catch(Exception $e) {

        }
        
        // Récupère les parametresvaleurs et les intègre dans un fichier
        $parametresvaleur = $em->getRepository(ParametresValeur::class)->getAllParametresValeur();
        try {
            $fs->dumpFile('json/parametresvaleur.json', json_encode($parametresvaleur)); 
            $filesNames['parametresvaleur'] = "parametresvaleur.json";         
        } catch(Exception $e) {

        }
        
        // Récupère les agences et les intègre dans un fichier
        $agence = $em->getRepository(Agence::class)->getAllAgence();
        try {
            $fs->dumpFile('json/agence.json', json_encode($agence)); 
            $filesNames['agence'] = "agence.json";         
        } catch(Exception $e) {

        }
        
        // Récupère les agences et les intègre dans un fichier
        $distributeur = $em->getRepository(Distributeur::class)->getAllDistributeur();
        try {
            $fs->dumpFile('json/distributeur.json', json_encode($distributeur)); 
            $filesNames['distributeur'] = "distributeur.json";         
        } catch(Exception $e) {

        }
        
        // Récupère les profils et les intègre dans un fichier
        $profil = $em->getRepository(Profil::class)->getAllProfil();
        try {
            $fs->dumpFile('json/profil.json', json_encode($profil)); 
            $filesNames['profil'] = "profil.json";         
        } catch(Exception $e) {

        }
        
        // Récupère les profilsfonctions et les intègre dans un fichier
        $profilfonction = $em->getRepository(ProfilFonction::class)->getAllProfilFonction();
        try {
            $fs->dumpFile('json/profilfonction.json', json_encode($profilfonction)); 
            $filesNames['profilfonction'] = "profilfonction.json";         
        } catch(Exception $e) {

        }
        
        // Récupère les codepostaux et les intègre dans un fichier
        $codepostaux = $em->getRepository(Codepostaux::class)->getAllCodepostaux();
        try {
            $fs->dumpFile('json/codepostaux.json', json_encode($codepostaux)); 
            $filesNames['codepostaux'] = "codepostaux.json";         
        } catch(Exception $e) {

        }
        
        // Supprime les anciens JSON
        $allFiles = scandir($this->getParameter('jsondirectory'));
        $pattern = '/^'.$commercial.'_[0-9]*_[a-z]*.json$/';
        foreach ($allFiles as $key => $file) {
            if(preg_match($pattern, $file) && strpos($file, $dateFile)=== false) {
                $fs->remove('json/'.$file); 
            }
        }
        

        try {
            $fileAppCache = $commercial.'.appcache';
            $offline = new Offline;
            $fs->dumpFile($fileAppCache, $offline->generateAppcache($commercial, $dateFile)); 
            $filesNames['appcache'] = $fileAppCache;    
            $session->set('appcache', $fileAppCache);
        } catch(Exception $e) {

        }

        
        
        return $this->json($filesNames);
    }




    /**
     * @Route("/ajax/synchro", name="synchro")
     */  
    public function synchro(Request $request) {
        $em = $this->getDoctrine()->getManager();

        /*===============================================================
        =            Backup du localStorage de l'utilisateur            =
        ===============================================================*/ 
        if($request->request->get("storage")) {
            $log = new LogSynchro;
            $log->setStorage($request->request->get("storage"));
            $log->setType("userToMySQL");
            $em->persist($log);
            $em->flush();
        }

        /*=====  End of Backup du localStorage de l'utilisateur  ======*/




        /*=========================================
        =            Gestion des mémos            =
        =========================================*/        
        if($request->request->get("memos")) {
            $memos = json_decode($request->request->get("memos"));
            
            foreach ($memos as $keyMemo => $valueMemo) {
                if(substr($valueMemo->code, 0, 1)=="_") {
                    // Le code est généré sur le modèle "_datedecreation", il est donc nouveau
                    $newMemo = new Memo;

                    if(!empty($valueMemo->date_creation)) {
                        $dateCreation = \DateTime::createFromFormat('d-m-Y', $valueMemo->date_creation);
                        $newMemo->setDateCreation($dateCreation);
                    }

                    if(!empty($valueMemo->prochain_rdv)) {
                        $dateRDV = \DateTime::createFromFormat('d-m-Y', $valueMemo->prochain_rdv);
                        $newMemo->setProchainRdv($dateRDV);                        
                    }

                    if(!empty($valueMemo->prochain_rdv_heure)) {
                        $heureRDV = \DateTime::createFromFormat('H:i', $valueMemo->prochain_rdv_heure);
                        $newMemo->setProchainRdvHeure($heureRDV);                        
                    }

                    // if(!empty($valueMemo->client_prospect) && $valueMemo->client_prospect!="undefined")
                    //     $newMemo->setInterlocuteur($valueMemo->client_prospect);

                    if(!empty($valueMemo->interlocuteur) && $valueMemo->interlocuteur!="undefined")
                        $newMemo->setInterlocuteur($valueMemo->interlocuteur);

                    if(!empty($valueMemo->livraison) && $valueMemo->livraison!="undefined")
                        $newMemo->setLivraison($valueMemo->livraison);

                    if(!empty($valueMemo->type))
                        $newMemo->setType($valueMemo->type);

                    if(!empty($valueMemo->objectif))
                        $newMemo->setObjectif($valueMemo->objectif);

                    $newMemo->setClientProspect($valueMemo->client_prospect);
                    $newMemo->setObjPrincipalProduits($valueMemo->obj_principal_produits);
                    $newMemo->setObjPrincipalOperations($valueMemo->obj_principal_operations);
                    $newMemo->setObjPrincipalActions($valueMemo->obj_principal_actions);
                    $newMemo->setObjRepliProduits($valueMemo->obj_repli_produits);
                    $newMemo->setObjRepliOperations($valueMemo->obj_repli_operations);
                    $newMemo->setObjRepliActions($valueMemo->obj_repli_actions);
                    $newMemo->setContenu($valueMemo->contenu);

                    //dump($newMemo);
                    $em->persist($newMemo);
                }
            }
            $em->flush();
            $em->getRepository(Memo::class)->generateMissingCodes();
        }
        /*=====  End of Gestion des mémos  ======*/



        /*=========================================
        =            Gestion des devis            =
        =========================================*/        
        if($request->request->get("devis")) {
            $devis = json_decode($request->request->get("devis"));
            
            foreach ($devis as $keyDevis => $valueDevis) {
                if(substr($valueDevis->code, 0, 1)=="_") {
                    // Création de devis
                    $entityDevis = new DevisSynchro;
                } else {
                    // Modification d'un devis existant
                    $entityDevis = $em->getRepository(DevisSynchro::class)->findOneByCode($valueDevis->code);
                    if(!$entityDevis) $entityDevis = new DevisSynchro;
                }

                $entityDevis->setFromSynchro($valueDevis);
                dump($valueDevis);
                dump($entityDevis);

                $em->persist($entityDevis);
            }
            $em->flush();
        }
        /*=====  End of Gestion des devis  ======*/



        /*=============================================
        =            Gestion des commandes            =
        =============================================*/        
        if($request->request->get("commandes")) {
            $commandes = json_decode($request->request->get("commandes"));
            
            foreach ($commandes as $keyCommande => $valueCommande) {
                $entityCommande = new CommandeSynchro;
                $entityCommande->setFromSynchro($valueCommande);
                $em->persist($entityCommande);
            }
            $em->flush();
        }
        /*=====  End of Gestion des commandes  ======*/



        /*===============================================
        =            Gestion des enlèvements            =
        ===============================================*/        
        if($request->request->get("enlevements")) {
            $enlevements = json_decode($request->request->get("enlevements"));
            $toExclude = $em->getRepository(Enlevement::class)->getCodesAlreadyIn();

            foreach ($enlevements as $keyElv => $valueElv) {
                if(!in_array($valueElv->code, $toExclude)) {
                    $entityElv = new Enlevement;
                    $entityElv->setFromSynchro($valueElv);
                    $em->persist($entityElv);                    
                }

            }
            $em->flush();
        }
        /*=====  End of Gestion des enlèvements  ======*/
        
        /*=============================================
        =            Gestion des vehicules            =
        =============================================*/        
        if($request->request->get("vehicule")) {
            $vehicule = json_decode($request->request->get("vehicule"));
            
            foreach ($vehicule as $keyVehicule => $valueVehicule) {
                $entityVehicule = new VehiculeSynchro;
                $entityVehicule->setFromSynchro($valueVehicule);
                $em->persist($entityVehicule);
            }
            $em->flush();
        }
        /*=====  End of Gestion des vehicules  ======*/

        /*=============================================
        =            Gestion des sms et mails            =
        =============================================*/        
        if($request->request->get("smsmail")) {
            $smsmail = json_decode($request->request->get("smsmail"));
            
            foreach ($smsmail as $keySmsmail => $valueSmsmail) {
                $entitySmsmail = new SmsmailSynchro;
                $entitySmsmail->setFromSynchro($valueSmsmail);
                $em->persist($entitySmsmail);
            }
            $em->flush();
        }
        /*=====  End of Gestion des sms et mails  ======*/

        /*=============================================
        =            Gestion parametres          =
        =============================================*/        
        if($request->request->get("parametres")) {
            $parametres = json_decode($request->request->get("parametres"));
            
            foreach ($parametres as $keyParametres => $valueParametres) {
                $entityParametres = new ParametresSynchro;
                $entityParametres->setFromSynchro($valueParametres);
                $em->persist($entityParametres);
            }
            $em->flush();
        }
        /*=====  End of Gparametres  ======*/

   /*=============================================
        =            Gestion parametresvaleur          =
        =============================================*/        
        if($request->request->get("parametresvaleur")) {
            $parametresvaleur = json_decode($request->request->get("parametresvaleur"));
            
            foreach ($parametresvaleur as $keyParametresvaleur => $valueParametresvaleur) {
                $entityParametresValeur = new ParametresValeurSynchro;
                $entityParametresValeur->setFromSynchro($valueParametresValeur);
                $em->persist($entityParametresValeur);
            }
            $em->flush();
        }
        /*=====  End of Gparametresvaleur  ======*/

        
        /*=============================================
        =            Gestion agence          =
        =============================================*/        
        if($request->request->get("agence")) {
            $agence = json_decode($request->request->get("agence"));
            
            foreach ($agence as $keyAgence => $valueAgence) {
                $entityAgence = new AgenceSynchro;
                $entityAgence->setFromSynchro($valueAgence);
                $em->persist($entityAgence);
            }
            $em->flush();
        }
        /*=====  End of agences  ======*/


        /*=============================================
        =            Gestion distributeur          =
        =============================================*/        
        if($request->request->get("distributeur")) {
            $distributeur = json_decode($request->request->get("distributeur"));
            
            foreach ($distributeur as $keyDistributeur => $valueDistributeur) {
                $entityDistributeur = new DistributeurSynchro;
                $entityDistributeur->setFromSynchro($valueDistributeur);
                $em->persist($entityDistributeur);
            }
            $em->flush();
        }
        /*=====  End of distributeur  ======*/


        /*=============================================
        =            Gestion profil          =
        =============================================*/        
        if($request->request->get("profil")) {
            $profil = json_decode($request->request->get("profil"));
            
            foreach ($profil as $keyProfil => $valueProfil) {
                $entityProfil = new ProfilSynchro;
                $entityProfil->setFromSynchro($valueProfil);
                $em->persist($entityProfil);
            }
            $em->flush();
        }
        /*=====  End of profil  ======*/

        /*=============================================
        =            Gestion profil foncton         =
        =============================================*/        
        if($request->request->get("profilfonction")) {
            $profilfonction = json_decode($request->request->get("profilfonction"));
            
            foreach ($profilfonction as $keyProfilfonction => $valueProfilfonction) {
                $entityProfilfonction = new ProfilSynchro;
                $entityProfilfonction->setFromSynchro($valueProfilfonction);
                $em->persist($entityProfilfonction);
            }
            $em->flush();
        }
        /*=====  End of profil fonction  ======*/

        /*=============================================
        =            Gestion profil codepostaux         =
        =============================================*/        
        if($request->request->get("Codepostaux")) {
            $codepostaux = json_decode($request->request->get("codepostaux"));
            
            foreach ($codepostaux as $keyCodepostaux => $valueCodepostaux) {
                $entityCodepostaux = new CodepostauxSynchro;
                $entityCodepostaux->setFromSynchro($valueCodepostaux);
                $em->persist($entityCodepostaux);
            }
            $em->flush();
        }
        /*=====  End of profil odepostaux  ======*/
        
        /*=================================================
        =            Gestion des tiers clients            =
        =================================================*/
        //dump($request->request);

        if($request->request->get("clients")) {
            $clients = json_decode($request->request->get("clients"));
            //dump($clients);
            foreach ($clients as $keyClient => $valueClient) {
                // Table TIERS
                $entityClient = new ClientSynchro;
                $entityClient->setFromSynchro($valueClient);
                //dump($entityClient);
                $em->persist($entityClient); 

                // Table ADRESSES
                if(isset($valueClient->adresses)) {
                    foreach ($valueClient->adresses as $keyAdresse => $valueAdresse) {
                        $entityAdresse = new AdresseSynchro;
                        $entityAdresse->setFromSynchro($valueAdresse, $valueClient->code); 
                        $em->persist($entityAdresse);      
                    }
                //$data = GmapApi::geocodeAddress($valueAdresse);
    
                

                    
                }

                // Table CONTACTS 
                if(isset($valueClient->contacts)) {
                    foreach ($valueClient->contacts as $keyContact => $valueContact) {
                        $entityContact = new ContactSynchro;
                        $entityContact->setFromSynchro($valueContact, $valueClient->code); 
                        $em->persist($entityContact);                        
                    }                    
                } 
            }
            $em->flush();
        }
        /*=====  End of Gestion des tiers clients  ======*/
        





        // Conditions de paiement
        $conditions = $em->getRepository(ConditionPaiement::class)->findAll();        
        $arrayConditions = [];

        foreach ($conditions as $key => $value) {
            $arrayConditions[] = [$value->getCode(), $value->getTitre()];
        }
        $toReturn['conditionsPaiement'] = $arrayConditions;


        // Activités
        $activite = $em->getRepository(Activite::class)->findAll();        
        $arrayActivite = [];
        foreach ($activite as $key => $value) {
            $arrayActivite[] = [$value->getCode(), $value->getTitre()];
        }
        $toReturn['activites'] = $arrayActivite;


        // Site de vente
        $toReturn['sitevente'] = $em->getRepository(SiteVente::class)->getSiteVente();

        // Site de vente
        $toReturn['fonctions'] = $em->getRepository(Fonction::class)->getFonctions();


        // Pays
        $toReturn['pays'] = $em->getRepository(Pays::class)->getAllPays();


        return $this->json($toReturn);

    }


    /**
     * @Route("/import", name="import")
     */  
    public function import() {
       /* $em = $this->getDoctrine()->getManager();
        $conditions = $em->getRepository(\App\Entity\Contact::class)->findByCodeX3AndCodeTiersX3('000000000000386', 'C00001');
        var_dump($conditions); */

        $sent=[];        
        $bUseFtp = false;
        $sLocalDir = "../public/imports";

        //Récupération des fichiers
//CODE TIERS,ACTIF,RAISON SOCIALE,CLIENT,PROSPECT,ENCOURS,MAX ENCOURS,RETARD PAIEMENT,BLOCAGE,SIGLE,TAILLE,PROFESSION,REP1,REP2,FRANCO,SIRET,NAF,BIC,RIB,IBAN,DOMICILIATION,CDT PAIEMENT,DATE MAJ
        //Injection des tiers dans la table temporaire
        $sFile = $sLocalDir.'/INFOTIERS.CSV';
        $sFileAbsolute = str_replace("\\", "/",$this->get('kernel')->getRootDir()).'/'.$sFile;

        /**/
        if(is_readable($sFile)){
            $fileContents = file_get_contents($sFileAbsolute);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($sFileAbsolute,utf8_encode($fileContents));
            }

            $sQuery = "
            TRUNCATE TABLE tiers_imports;
            LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '$sFileAbsolute'
            INTO TABLE tiers_imports
            CHARACTER SET UTF8
            FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            IGNORE 1 LINES
            (
                code_x3,
                @statut_tiers,
                raison_sociale,
                @client,
                @prospect,
                site,
                encours,
                max_encours,
                retard_paiement,
                blocage,
                sigle,
                taille,
                profession,
                rep_gestion,
                rep_gestion2,
                franco,
                num_siret,
                code_naf,
                bic,
                tva_intracom,
                @iban1,
                @iban2,
                domiciliation,
                code_cond_paie,
                @last_x3_update,
                last_contact
            )
            SET
            statut_tiers = IF(@statut_tiers = 2,'1','0'),
            type_tiers = IF(@client = 2,'C','P'),
            iban = concat(@iban2,@iban1),
            last_x3_update = DATE_FORMAT(@last_x3_update, '%Y-%m-%d %H:%i:%s')
            ";
           // $result =mysql_query($sQuery);
            $db = $this->getDoctrine()->getManager()->getConnection(); // Dans un Controller
            $stmt = $db->prepare($sQuery);
            $stmt->execute();
        }

        //Injection des contacts dans la table temporaire
        $sFile = $sLocalDir.'/CTCTIERS.CSV';
        $sFileAbsolute = str_replace("\\", "/",$this->get('kernel')->getRootDir()).'/'.$sFile;



        if(is_readable($sFile)){
            $fileContents = file_get_contents($sFileAbsolute);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($sFileAbsolute,utf8_encode($fileContents));
            }

            $sQuery = "
            TRUNCATE TABLE contact_imports;
             LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '$sFileAbsolute'
            INTO TABLE contact_imports
            CHARACTER SET UTF8
            FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            IGNORE 1 LINES
            (
                code_tiers_x3,
                code_x3,
                @default,
                civilite,
                nom,
                prenom,
                fonction,
                service,
                telc,
                faxc,
                mobilec,
                mailc,
                contact_address_code
            )
            SET
            actif = '1',
            is_default = IF(@default = 2, 1, 0)
            ";
           // $result =mysql_query($sQuery);
            $db = $this->getDoctrine()->getManager()->getConnection(); // Dans un Controller
            $stmt = $db->prepare($sQuery);
            $stmt->execute();
        }
        /**/

        //Injection des adresses dans la table temporaire
        $sFile = $sLocalDir.'/ADRTIERS.CSV';
        $sFileAbsolute = str_replace("\\", "/",$this->get('kernel')->getRootDir()).'/'.$sFile;


        if(is_readable($sFile)){
            $fileContents = file_get_contents($sFileAbsolute);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($sFileAbsolute,utf8_encode($fileContents));
            }

            $sQuery = " 
            TRUNCATE TABLE adresse_imports;
            LOAD DATA ".($_SERVER['REMOTE_ADDR']!='127.0.0.1' ? 'LOCAL':'')." INFILE '$sFileAbsolute'
            INTO TABLE adresse_imports
            CHARACTER SET UTF8
            FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            IGNORE 1 LINES
            (
                code_tiers_x3,
                code_x3,
                @is_default,
                @intitule,
                address1,
                address2,
                address3,
                zip,
                city,
                @canton,
                @pays,
                chantier,
                telephone,
                fax,
                portable,
                email,
                @datemaj
            )";
           // $result =mysql_query($sQuery);
            $db = $this->getDoctrine()->getManager()->getConnection(); // Dans un Controller
            $stmt = $db->prepare($sQuery);
            $stmt->execute();
        }
        /*
         //Injection des OR dans la table temporaire
        $sFile = $sLocalDir.'/OR.CSV';
        $sFileAbsolute = str_replace("\\", "/",$this->get('kernel')->getRootDir()).'/'.$sFile;
        if(is_readable($sFile)){

            $fileContents = file_get_contents($sFileAbsolute);
            if (!mb_check_encoding($fileContents, 'UTF-8')) {
                file_put_contents($sFileAbsolute,utf8_encode($fileContents));
            }

            $sQuery = " LOAD DATA INFILE '$sFileAbsolute'
            INTO TABLE sav_imports
            CHARACTER SET UTF8
            FIELDS TERMINATED BY ',' ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            IGNORE 1 LINES
            (
                code_x3,
                code_tiers_x3,
                date,
                @rep1,
                @rep2,
                etat,
                titre,
                titre_atelier,
                commentaire
            )";
           // $result =mysql_query($sQuery);
            $db = $this->getDoctrine()->getManager()->getConnection(); // Dans un Controller
            $stmt = $db->prepare($sQuery);
            $stmt->execute();
        }
    */
        return $this->json($sent);
    }
}
