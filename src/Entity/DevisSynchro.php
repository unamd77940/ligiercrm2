<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DevisSynchroRepository")
 */
class DevisSynchro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $code;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $client_code;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $redacteur_code;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $etat;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_etat;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $statut;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $perte;
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $articles;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $adresse_id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $adresse_code;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $site_vente;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaires;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ref;
        
    /**
     * @ORM\Column(type="float")
     */
    private $frais_transport;
        
    /**
     * @ORM\Column(type="float")
     */
    private $totaldevis;
        
    /**
     * @ORM\Column(type="float")
     */
    private $totalHT;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_creation;
        
    /**
     * @ORM\Column(type="boolean")
     */
    private $synchro;


    public function __construct() {
    	$this->totalHT = 0;
    	$this->totaldevis = 0;
        $this->synchro = false;
    	$this->frais_transport = 0;
    	$this->date_creation = new \Datetime;
    }	


    public function setFromSynchro($valueDevis) {  
        // Ajout des valeurs du devis à l'entité
        if(isset($valueDevis->code)) $this->setCode($valueDevis->code);
        if(isset($valueDevis->client_code)) $this->setClientCode($valueDevis->client_code);
        if(!isset($valueDevis->date) && isset($valueDevis->date_creation)) $valueDevis->date = $valueDevis->date_creation;
        if(isset($valueDevis->date)) {
        	$formated = (new \Datetime)::createFromFormat('d-m-Y', $valueDevis->date);
            dump($formated);
        	if($formated) $this->setDate($formated);
        	else $this->setDateEtat(null);
        } else {
        	$this->setDate(null);
        }
        if(isset($valueDevis->etat)) $this->setEtat($valueDevis->etat);
        if(isset($valueDevis->date_etat)) {
        	$formated = (new \Datetime)::createFromFormat('d-m-Y', $valueDevis->date_etat);
        	if($formated) $this->setDateEtat($formated);
        	else $this->setDateEtat(null);
        } else {
        	$this->setDateEtat(null);
        }
        if(isset($valueDevis->statut)) $this->setStatut($valueDevis->statut);
        if(isset($valueDevis->perte)) $this->setPerte($valueDevis->perte);
        if(isset($valueDevis->observations)) $this->setObservations($valueDevis->observations);                
        if(isset($valueDevis->articles)) $this->setArticles(json_encode($valueDevis->articles));
        if(isset($valueDevis->adresse_id)) $this->setAdresseId($valueDevis->adresse_id);
        if(isset($valueDevis->adresse_code)) $this->setAdresseCode($valueDevis->adresse_code);
        if(isset($valueDevis->commentaires)) $this->setCommentaires($valueDevis->commentaires);
        if(isset($valueDevis->ref)) $this->setRef($valueDevis->ref); 
        if(isset($valueDevis->frais_transport)) $this->setFraisTransport($valueDevis->frais_transport);
        if(isset($valueDevis->totaldevis)) $this->setTotaldevis($valueDevis->totaldevis);
        if(isset($valueDevis->date_creation)) {
        	$formated = (new \Datetime)::createFromFormat('Y-m-d', $valueDevis->date_creation);
            if(!$formated) {
                $formated = (new \Datetime)::createFromFormat('d-m-Y', $valueDevis->date_creation);
            }
        	$this->setDateCreation($formated);
        }
        if(isset($valueDevis->totalHT)) $this->setTotalHT($valueDevis->totalHT);
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientCode()
    {
        return $this->client_code;
    }

    /**
     * @param mixed $client_code
     *
     * @return self
     */
    public function setClientCode($client_code)
    {
        $this->client_code = $client_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedacteurCode()
    {
        return $this->redacteur_code;
    }

    /**
     * @param mixed $redacteur_code
     *
     * @return self
     */
    public function setRedacteurCode($redacteur_code)
    {
        $this->redacteur_code = $redacteur_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     *
     * @return self
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEtat()
    {
        return $this->date_etat;
    }

    /**
     * @param mixed $date_etat
     *
     * @return self
     */
    public function setDateEtat($date_etat)
    {
        $this->date_etat = $date_etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     *
     * @return self
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerte()
    {
        return $this->perte;
    }

    /**
     * @param mixed $perte
     *
     * @return self
     */
    public function setPerte($perte)
    {
        $this->perte = $perte;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * @param mixed $observations
     *
     * @return self
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     *
     * @return self
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseId()
    {
        return $this->adresse_id;
    }

    /**
     * @param mixed $adresse_id
     *
     * @return self
     */
    public function setAdresseId($adresse_id)
    {
        $this->adresse_id = $adresse_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseCode()
    {
        return $this->adresse_code;
    }

    /**
     * @param mixed $adresse_code
     *
     * @return self
     */
    public function setAdresseCode($adresse_code)
    {
        $this->adresse_code = $adresse_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteVente()
    {
        return $this->site_vente;
    }

    /**
     * @param mixed $site_vente
     *
     * @return self
     */
    public function setSiteVente($site_vente)
    {
        $this->site_vente = $site_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     *
     * @return self
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFraisTransport()
    {
        return $this->frais_transport;
    }

    /**
     * @param mixed $frais_transport
     *
     * @return self
     */
    public function setFraisTransport($frais_transport)
    {
        $this->frais_transport = $frais_transport;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotaldevis()
    {
        return $this->totaldevis;
    }

    /**
     * @param mixed $totaldevis
     *
     * @return self
     */
    public function setTotaldevis($totaldevis)
    {
        $this->totaldevis = $totaldevis;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param mixed $totalHT
     *
     * @return self
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     *
     * @return self
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSynchro()
    {
        return $this->synchro;
    }

    /**
     * @param mixed $synchro
     *
     * @return self
     */
    public function setSynchro($synchro)
    {
        $this->synchro = $synchro;

        return $this;
    }
}
