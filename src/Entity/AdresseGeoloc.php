<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;



/**
 * @ORM\Entity(repositoryClass="App\Repository\AdresseGeolocRepository")
 * @Table(indexes={@Index(name="codetiersx3_idx", columns={"code_tiers_x3"})})
 */
class AdresseGeoloc
{
     /** @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
       
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address3;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $pays_code;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
                    fwrite($fichiergeoloc, "blabla");

    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    
    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     *
     * @return self
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     *
     * @return self
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * @param mixed $address3
     *
     * @return self
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaysCode()
    {
        return $this->pays_code;
    }

    /**
     * @param mixed $zip
     *
     * @return self
     */
    public function setPaysCode($pays_code)
    {
        $this->pays_code = $pays_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

}