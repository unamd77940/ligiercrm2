<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 * @Table(indexes={@Index(name="codetiersx3_idx", columns={"code_tiers_x3"})})
 */
class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_x3;

    
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_tiers_x3;
    
    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $civilite;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $nom;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $prenom;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $is_default;
    
    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $fonction;
    
    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $mailc;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $contact_address_code;
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $service;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telc;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mobilec;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $faxc;

    
    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $addedit_flag;

    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_mysql_update;

/**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heure_pref;

   

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;
    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeTiersX3()
    {
        return $this->code_tiers_x3;
    }

    /**
     * @param mixed $code_tiers_x3
     *
     * @return self
     */
    public function setCodeTiersX3($code_tiers_x3)
    {
        $this->code_tiers_x3 = $code_tiers_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     *
     * @return self
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     *
     * @return self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     *
     * @return self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * @param mixed $is_default
     *
     * @return self
     */
    public function setIsDefault($is_default)
    {
        $this->is_default = $is_default;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * @param mixed $fonction
     *
     * @return self
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMailc()
    {
        return $this->mailc;
    }

    /**
     * @param mixed $mailc
     *
     * @return self
     */
    public function setMailc($mailc)
    {
        $this->mailc = $mailc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactAddressCode()
    {
        return $this->contact_address_code;
    }

    /**
     * @param mixed $contact_address_code
     *
     * @return self
     */
    public function setContactAddressCode($contact_address_code)
    {
        $this->contact_address_code = $contact_address_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     *
     * @return self
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelc()
    {
        return $this->telc;
    }

    /**
     * @param mixed $telc
     *
     * @return self
     */
    public function setTelc($telc)
    {
        $this->telc = $telc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobilec()
    {
        return $this->mobilec;
    }

    /**
     * @param mixed $mobilec
     *
     * @return self
     */
    public function setMobilec($mobilec)
    {
        $this->mobilec = $mobilec;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaxc()
    {
        return $this->faxc;
    }

    /**
     * @param mixed $faxc
     *
     * @return self
     */
    public function setFaxc($faxc)
    {
        $this->faxc = $faxc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param mixed $actif
     *
     * @return self
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddeditFlag()
    {
        return $this->addedit_flag;
    }

    /**
     * @param mixed $addedit_flag
     *
     * @return self
     */
    public function setAddeditFlag($addedit_flag)
    {
        $this->addedit_flag = $addedit_flag;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastMysqlUpdate()
    {
        return $this->last_mysql_update;
    }

    /**
     * @param mixed $last_mysql_update
     *
     * @return self
     */
    public function setLastMysqlUpdate($last_mysql_update)
    {
        $this->last_mysql_update = $last_mysql_update;

        return $this;
    }
/**
     * @return mixed
     */
    public function getHeurepref()
    {
        return $this->heure_pref;
    }

    /**
     * @param mixed $heure_pref
     *
     * @return self
     */
    public function setHeurepref($heure_pref)
    {
        $this->heure_pref = $heure_pref;

        return $this;
    }

}
