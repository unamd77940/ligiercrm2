<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MemoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Memo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
       
    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $etape;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $etat;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $code;
        
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $clientprospect;
        
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $interlocuteur;

        
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $livraison;
        
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $type;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $objectif;
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenu;
        
    /**
     * @ORM\Column(type="date")
     */
    private $datecreation;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $prochain_rdv;
        
    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $prochain_rdv_heure;
        
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $obj_principal_produits;
        
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $obj_principal_operations;
        
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $obj_principal_actions;
        
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $obj_repli_produits;
        
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $obj_repli_operations;
        
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $obj_repli_actions;



  
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     *
     * @return self
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getEtape()
    {
        return $this->etape;
    }

    /**
     * @param mixed $etape
     *
     * @return self
     */
    public function setEtape($etape)
    {
        $this->etape = $etape;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientProspect()
    {
        return $this->clientprospect;
    }

    /**
     * @param mixed $clientprospect
     *
     * @return self
     */
    public function setClientProspect($clientprospect)
    {
        $this->clientprospect = $clientprospect;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterlocuteur()
    {
        return $this->interlocuteur;
    }

    /**
     * @param mixed $interlocuteur
     *
     * @return self
     */
    public function setInterlocuteur($interlocuteur)
    {
        $this->interlocuteur = $interlocuteur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLivraison()
    {
        return $this->livraison;
    }

    /**
     * @param mixed $livraison
     *
     * @return self
     */
    public function setLivraison($livraison)
    {
        $this->livraison = $livraison;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * @param mixed $objectif
     *
     * @return self
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param mixed $contenu
     *
     * @return self
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->datecreation;
    }

    /**
     * @param mixed $datecreation
     *
     * @return self
     */
    public function setDateCreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProchainRdv()
    {
        return $this->prochain_rdv;
    }

    /**
     * @param mixed $prochain_rdv
     *
     * @return self
     */
    public function setProchainRdv($prochain_rdv)
    {
        $this->prochain_rdv = $prochain_rdv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProchainRdvHeure()
    {
        return $this->prochain_rdv_heure;
    }

    /**
     * @param mixed $prochain_rdv_heure
     *
     * @return self
     */
    public function setProchainRdvHeure($prochain_rdv_heure)
    {
        $this->prochain_rdv_heure = $prochain_rdv_heure;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjPrincipalProduits()
    {
        return $this->obj_principal_produits;
    }

    /**
     * @param mixed $obj_principal_produits
     *
     * @return self
     */
    public function setObjPrincipalProduits($obj_principal_produits)
    {
        $this->obj_principal_produits = $obj_principal_produits;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjPrincipalOperations()
    {
        return $this->obj_principal_operations;
    }

    /**
     * @param mixed $obj_principal_operations
     *
     * @return self
     */
    public function setObjPrincipalOperations($obj_principal_operations)
    {
        $this->obj_principal_operations = $obj_principal_operations;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjPrincipalActions()
    {
        return $this->obj_principal_actions;
    }

    /**
     * @param mixed $obj_principal_actions
     *
     * @return self
     */
    public function setObjPrincipalActions($obj_principal_actions)
    {
        $this->obj_principal_actions = $obj_principal_actions;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjRepliProduits()
    {
        return $this->obj_repli_produits;
    }

    /**
     * @param mixed $obj_repli_produits
     *
     * @return self
     */
    public function setObjRepliProduits($obj_repli_produits)
    {
        $this->obj_repli_produits = $obj_repli_produits;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjRepliOperations()
    {
        return $this->obj_repli_operations;
    }

    /**
     * @param mixed $obj_repli_operations
     *
     * @return self
     */
    public function setObjRepliOperations($obj_repli_operations)
    {
        $this->obj_repli_operations = $obj_repli_operations;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjRepliActions()
    {
        return $this->obj_repli_actions;
    }

    /**
     * @param mixed $obj_repli_actions
     *
     * @return self
     */
    public function setObjRepliActions($obj_repli_actions)
    {
        $this->obj_repli_actions = $obj_repli_actions;

        return $this;
    }


    /**
	 * @ORM\PostPersist
	 */
	public function setCodeValue()
	{
		$this->code = "MEM".$this->id;
				
	}

}
