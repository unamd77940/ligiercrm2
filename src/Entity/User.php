<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $code;

    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;
    
    /**
     * @ORM\Column(type="string", length=35)
     */
    private $nom;
    
    /**
     * @ORM\Column(type="string", length=35)
     */
    private $prenom;
    

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $site;
    
    /**
     * @ORM\Column(type="integer", length=5)
     */
    private $distributeur_id;

    /**
     * @ORM\Column(type="integer", length=5)
     */
    private $agence_id;

    /**
     * @ORM\Column(type="integer", length=5)
     */
    private $profil_id;


    public function __construct() {

    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     *
     * @return self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     *
     * @return self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     *
     * @return self
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getDistributeur_id()
    {
        return $this->distributeur_id;
    }

    /**
     * @param mixed $distributeur_id
     *
     * @return self
     */
    public function setDistributeur_id($distributeur_id)
    {
        $this->distributeur_id = $distributeur_id;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getAgence_id()
    {
        return $this->agence_id;
    }

    /**
     * @param mixed $agence_id
     *
     * @return self
     */
    public function setAgence_id($agence_id)
    {
        $this->agence_id = $agence_id;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getProfil_id()
    {
        return $this->profil_id;
    }

    /**
     * @param mixed $profil_id
     *
     * @return self
     */
    public function setProfil_id($profil_id)
    {
        $this->profil_id = $profil_id;

        return $this;
    }
}
