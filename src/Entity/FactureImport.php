<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FactureImportRepository")
 * @Table(indexes={@Index(name="code_idx", columns={"id", "code", "client_code"})})
 */
class FactureImport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $code;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $client_code;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;
                
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $articles;
        
    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $site_vente;
        
    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $type;
                
        
    /**
     * @ORM\Column(type="float")
     */
    private $totalHT;
        


    public function __construct() {
    }	

    

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientCode()
    {
        return $this->client_code;
    }

    /**
     * @param mixed $client_code
     *
     * @return self
     */
    public function setClientCode($client_code)
    {
        $this->client_code = $client_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     *
     * @return self
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteVente()
    {
        return $this->site_vente;
    }

    /**
     * @param mixed $site_vente
     *
     * @return self
     */
    public function setSiteVente($site_vente)
    {
        $this->site_vente = $site_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param mixed $totalHT
     *
     * @return self
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }
}
