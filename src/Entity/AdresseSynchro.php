<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AdresseSynchroRepository")
 */
class AdresseSynchro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_x3;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_tiers_x3;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address3;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $pays_code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $chantier;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $portable;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $fax;
   
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $synchro;




    public function __construct()
    {
        $this->synchro = 0;
        $this->pays_code = 'FR';
    }


    public function setFromSynchro($adresse, $codeTiers) {
        $this->setCodeTiersX3($codeTiers);
        if(isset($adresse->code)) $this->setCodeX3($adresse->code);
        if(isset($adresse->adresse1)) $this->setAddress1($adresse->adresse1);
        if(isset($adresse->adresse2)) $this->setAddress2($adresse->adresse2);
        if(isset($adresse->adresse3)) $this->setAddress3($adresse->adresse3);
        if(isset($adresse->postal)) $this->setZip($adresse->postal);
        if(isset($adresse->ville)) $this->setCity($adresse->ville);
        if(isset($adresse->pays_code)) $this->setPaysCode($adresse->pays_code);
        if(isset($adresse->livchantier)) $this->setChantier($adresse->livchantier);
        if(isset($adresse->telephone)) $this->setTelephone($adresse->telephone);
        if(isset($adresse->portable)) $this->setPortable($adresse->portable);
        if(isset($adresse->fax)) $this->setFax($adresse->fax);
        if(isset($adresse->email)) $this->setEmail($adresse->email);
    }



    public function getAllVariables($exclude = []) {
        $objToArray = get_object_vars($this);
        // Si besoin, supprime certaines clés inutiles
        foreach ($exclude as $key => $toRemove) {
            try {
                unset($objToArray[$toRemove]);
            } catch(Exception $e) {

            }
        }

        // conversion des noms des champs en clés utilisées dans le JSON
        $conv = [
                    ["code_x3", "code"],
                    ["address1", "adresse1"],
                    ["address2", "adresse2"],
                    ["address3", "adresse3"],
                    ["zip", "postal"],
                    ["city", "ville"],
                    ["chantier", "livchantier"],
                ];

        foreach ($conv as $key => $value) {
            if(isset($objToArray[$value[0]])) {
                $objToArray[$value[1]] = $objToArray[$value[0]];
                unset($objToArray[$value[0]]);
            }
        }

        return $objToArray;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;
    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeTiersX3()
    {
        return $this->code_tiers_x3;
    }

    /**
     * @param mixed $code_tiers_x3
     *
     * @return self
     */
    public function setCodeTiersX3($code_tiers_x3)
    {
        $this->code_tiers_x3 = $code_tiers_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     *
     * @return self
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     *
     * @return self
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * @param mixed $address3
     *
     * @return self
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaysCode()
    {
        return $this->pays_code;
    }

    /**
     * @param mixed $zip
     *
     * @return self
     */
    public function setPaysCode($pays_code)
    {
        $this->pays_code = $pays_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChantier()
    {
        return $this->chantier;
    }

    /**
     * @param mixed $chantier
     *
     * @return self
     */
    public function setChantier($chantier)
    {
        $this->chantier = $chantier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     *
     * @return self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPortable()
    {
        return $this->portable;
    }

    /**
     * @param mixed $portable
     *
     * @return self
     */
    public function setPortable($portable)
    {
        $this->portable = $portable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     *
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSynchro()
    {
        return $this->synchro;
    }

    /**
     * @param mixed $synchro
     *
     * @return self
     */
    public function setSynchro($synchro)
    {
        $this->synchro = $synchro;

        return $this;
    }
}
