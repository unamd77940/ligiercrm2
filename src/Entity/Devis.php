<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DevisRepository")
 * @Table(indexes={@Index(name="code_idx", columns={"id", "code", "clientcode"})})
 */
class Devis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $code;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $clientcode;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $redacteur_code;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_validite;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $etat;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_etat;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $statut;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $perte;
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $articles;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $adresse_id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $adresse_code;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $site_vente;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $site_expedition;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaires;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ref;
        
    /**
     * @ORM\Column(type="float")
     */
    private $frais_transport;
        
    /**
     * @ORM\Column(type="float")
     */
    private $totaldevis;
        
    /**
     * @ORM\Column(type="float")
     */
    private $totalHT;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_creation;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $source;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idvehicule;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentairesd;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $daterelance;
    

    public function __construct() {
    	$this->totalHT = 0;
    	$this->totaldevis = 0;
    	$this->frais_transport = 0;
    	$this->date_creation = new \Datetime;
    }	


    

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientCode()
    {
        return $this->clientcode;
    }

    /**
     * @param mixed $clientcode
     *
     * @return self
     */
    public function setClientCode($clientcode)
    {
        $this->clientcode = $clientcode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedacteurCode()
    {
        return $this->redacteur_code;
    }

    /**
     * @param mixed $redacteur_code
     *
     * @return self
     */
    public function setRedacteurCode($redacteur_code)
    {
        $this->redacteur_code = $redacteur_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateValidite()
    {
        return $this->date_validite;
    }

    /**
     * @param mixed $date_validite
     *
     * @return self
     */
    public function setDateValidite($date_validite)
    {
        $this->date_validite = $date_validite;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     *
     * @return self
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEtat()
    {
        return $this->date_etat;
    }

    /**
     * @param mixed $date_etat
     *
     * @return self
     */
    public function setDateEtat($date_etat)
    {
        $this->date_etat = $date_etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     *
     * @return self
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerte()
    {
        return $this->perte;
    }

    /**
     * @param mixed $perte
     *
     * @return self
     */
    public function setPerte($perte)
    {
        $this->perte = $perte;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * @param mixed $observations
     *
     * @return self
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     *
     * @return self
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseId()
    {
        return $this->adresse_id;
    }

    /**
     * @param mixed $adresse_id
     *
     * @return self
     */
    public function setAdresseId($adresse_id)
    {
        $this->adresse_id = $adresse_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseCode()
    {
        return $this->adresse_code;
    }

    /**
     * @param mixed $adresse_code
     *
     * @return self
     */
    public function setAdresseCode($adresse_code)
    {
        $this->adresse_code = $adresse_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteVente()
    {
        return $this->site_vente;
    }

    /**
     * @param mixed $site_vente
     *
     * @return self
     */
    public function setSiteVente($site_vente)
    {
        $this->site_vente = $site_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteExpedition()
    {
        return $this->site_expedition;
    }

    /**
     * @param mixed $site_expedition
     *
     * @return self
     */
    public function setSiteExpedition($site_expedition)
    {
        $this->site_expedition = $site_expedition;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     *
     * @return self
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFraisTransport()
    {
        return $this->frais_transport;
    }

    /**
     * @param mixed $frais_transport
     *
     * @return self
     */
    public function setFraisTransport($frais_transport)
    {
        $this->frais_transport = $frais_transport;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotaldevis()
    {
        return $this->totaldevis;
    }

    /**
     * @param mixed $totaldevis
     *
     * @return self
     */
    public function setTotaldevis($totaldevis)
    {
        $this->totaldevis = $totaldevis;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param mixed $totalHT
     *
     * @return self
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     *
     * @return self
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     *
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getIdvehicule()
    {
        return $this->idvehicule;
    }

    /**
     * @param mixed $idvehicule
     *
     * @return self
     */
    public function setIdvehicule($idvehicule)
    {
        $this->idvehicule = $idvehicule;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getDaterelance()
    {
        return $this->daterelance;
    }

    /**
     * @param mixed $daterelance
     *
     * @return self
     */
    public function setDaterelance($daterelance)
    {
        $this->daterelance = $daterelance;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCommentairesd()
    {
        return $this->commentairesd;
    }

    /**
     * @param mixed $commentairesd
     *
     * @return self
     */
    public function setCommentairesd($commentairesd)
    {
        $this->commentairesd = $commentairesd;

        return $this;
    }
}
