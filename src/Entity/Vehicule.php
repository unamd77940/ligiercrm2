<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VehiculeRepository")
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $marque;
  

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $modele;

/**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $options;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $no_serie;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;

    
    /**
     * @ORM\Column(type="date")
     */
    private $date1eremise;


    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $immat;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $commentaires;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $energie;


    /**
     * @ORM\Column(type="integer")
     */
    private $kilometrage;


    /**
     * @ORM\Column(type="integer")
     */
    private $puissance;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $idclient;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nom_client;

    /**
     * @ORM\Column(type="date")
     */
    private $date_achat;


    /**
     * @ORM\Column(type="integer")
     */
    private $prix_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $frais_etat;


    /**
     * @ORM\Column(type="date")
     */
    private $date_vente;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type_vente;


    /**
     * @ORM\Column(type="integer")
     */
    private $prix_vente;

    /**
     * @ORM\Column(type="date")
     */
    private $date_fin_credit;


    /**
     * @ORM\Column(type="date")
     */
    private $date_fin_garantie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $dispo_vente;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     *
     * @return self
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     *
     * @return self
     */
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param mixed $couleur
     *
     * @return self
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     *
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNo_serie()
    {
        return $this->no_serie;
    }

    /**
     * @param mixed $no_serie
     *
     * @return self
     */
    public function setNo_serie($no_serie)
    {
        $this->no_serie = $no_serie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate1eremise()
    {
        return $this->date1eremise;
    }

    /**
     * @param mixed $date1eremise
     *
     * @return self
     */
    public function setDate1eremise($date1eremise)
    {
        $this->date1eremise = $date1eremise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImmat()
    {
        return $this->immat;
    }

    /**
     * @param mixed $immat
     *
     * @return self
     */
    public function setImmat($immat)
    {
        $this->immat = $immat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnergie()
    {
        return $this->energie;
    }

    /**
     * @param mixed $energie
     *
     * @return self
     */
    public function setEnergie($energie)
    {
        $this->energie = $energie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKilometrage()
    {
        return $this->kilometrage;
    }

    /**
     * @param mixed $kilometrage
     *
     * @return self
     */
    public function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * @param mixed $puissance
     *
     * @return self
     */
    public function setPuissance($puissance)
    {
        $this->puissance = $puissance;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * @param mixed $idclient
     *
     * @return self
     */
    public function setIdclient($idclient)
    {
        $this->idclient = $idclient;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom_client()
    {
        return $this->nom_client;
    }

    /**
     * @param mixed $nom_client
     *
     * @return self
     */
    public function setNom_client($nom_client)
    {
        $this->nom_client = $nom_client;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate_achat()
    {
        return $this->date_achat;
    }

    /**
     * @param mixed $date_achat
     *
     * @return self
     */
    public function setDate_achat($date_achat)
    {
        $this->date_achat = $date_achat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrix_achat()
    {
        return $this->prix_achat;
    }

    /**
     * @param mixed $prix_achat
     *
     * @return self
     */
    public function setPrix_achat($prix_achat)
    {
        $this->prix_achat = $prix_achat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrais_etat()
    {
        return $this->frais_etat;
    }

    /**
     * @param mixed $frais_etat
     *
     * @return self
     */
    public function setFrais_etat($frais_etat)
    {
        $this->frais_etat = $frais_etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate_vente()
    {
        return $this->date_vente;
    }

    /**
     * @param mixed $date_vente
     *
     * @return self
     */
    public function setDate_vente($date_vente)
    {
        $this->date_vente = $date_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType_vente()
    {
        return $this->type_vente;
    }

    /**
     * @param mixed $type_vente
     *
     * @return self
     */
    public function setType_vente($type_vente)
    {
        $this->type_vente = $type_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrix_vente()
    {
        return $this->prix_vente;
    }

    /**
     * @param mixed $prix_vente
     *
     * @return self
     */
    public function setPrix_vente($prix_vente)
    {
        $this->prix_vente = $prix_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate_fin_credit()
    {
        return $this->date_fin_credit;
    }

    /**
     * @param mixed $date_fin_credit
     *
     * @return self
     */
    public function setDate_fin_credit($date_fin_credit)
    {
        $this->date_fin_credit = $date_fin_credit;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate_fin_garantie()
    {
        return $this->date_fin_garantie;
    }

    /**
     * @param mixed $date_fin_garantie
     *
     * @return self
     */
    public function setDate_fin_garantie($date_fin_garantie)
    {
        $this->date_fin_garantie = $date_fin_garantie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDispo_vente()
    {
        return $this->dispo_vente;
    }

    /**
     * @param mixed $dispo_vente
     *
     * @return self
     */
    public function setDispo_vente($dispo_vente)
    {
        $this->dispo_vente = $dispo_vente;

        return $this;
    }
}
