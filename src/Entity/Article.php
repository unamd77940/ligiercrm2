<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_x3;
  

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $designation;


    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $fournisseur;


    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $uv;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $uf;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cdt;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix_tarif;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix_revient_rennes;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix_revient_lille;


    /**
     * @ORM\Column(type="float")
     */
    private $prix_median;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_ch2;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_ch2;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_ch3;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_ch3;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_ch4;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_ch4;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_dsi;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_dsi;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_li1;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_li1;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_na1;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_na1;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue_ve1;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a_ve1;

    
    public function __construct() {
        $this->tenue_ve1 = 0;
        $this->stock_interne_a_ve1 = 0;
        $this->tenue_na1 = 0;
        $this->stock_interne_a_na1 = 0;
        $this->tenue_li1 = 0;
        $this->stock_interne_a_li1 = 0;
        $this->tenue_dsi = 0;
        $this->stock_interne_a_dsi = 0;
        $this->tenue_ch2 = 0;
        $this->stock_interne_a_ch2 = 0;
        $this->tenue_ch3 = 0;
        $this->stock_interne_a_ch3 = 0;
        $this->tenue_ch4 = 0;
        $this->stock_interne_a_ch4 = 0;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;
    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     *
     * @return self
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * @param mixed $fournisseur
     *
     * @return self
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUv()
    {
        return $this->uv;
    }

    /**
     * @param mixed $uv
     *
     * @return self
     */
    public function setUv($uv)
    {
        $this->uv = $uv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     *
     * @return self
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCdt()
    {
        return $this->cdt;
    }

    /**
     * @param mixed $cdt
     *
     * @return self
     */
    public function setCdt($cdt)
    {
        $this->cdt = $cdt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixTarif()
    {
        return $this->prix_tarif;
    }

    /**
     * @param mixed $prix_tarif
     *
     * @return self
     */
    public function setPrixTarif($prix_tarif)
    {
        $this->prix_tarif = $prix_tarif;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixRevientRennes()
    {
        return $this->prix_revient_rennes;
    }

    /**
     * @param mixed $prix_revient_rennes
     *
     * @return self
     */
    public function setPrixRevientRennes($prix_revient_rennes)
    {
        $this->prix_revient_rennes = $prix_revient_rennes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixRevientLille()
    {
        return $this->prix_revient_lille;
    }

    /**
     * @param mixed $prix_revient_lille
     *
     * @return self
     */
    public function setPrixRevientLille($prix_revient_lille)
    {
        $this->prix_revient_lille = $prix_revient_lille;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixMedian()
    {
        return $this->prix_median;
    }

    /**
     * @param mixed $prix_median
     *
     * @return self
     */
    public function setPrixMedian($prix_median)
    {
        $this->prix_median = $prix_median;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueCh2()
    {
        return $this->tenue_ch2;
    }

    /**
     * @param mixed $tenue_ch2
     *
     * @return self
     */
    public function setTenueCh2($tenue_ch2)
    {
        $this->tenue_ch2 = $tenue_ch2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneACh2()
    {
        return $this->stock_interne_a_ch2;
    }

    /**
     * @param mixed $stock_interne_a_ch2
     *
     * @return self
     */
    public function setStockInterneACh2($stock_interne_a_ch2)
    {
        $this->stock_interne_a_ch2 = $stock_interne_a_ch2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueCh3()
    {
        return $this->tenue_ch3;
    }

    /**
     * @param mixed $tenue_ch3
     *
     * @return self
     */
    public function setTenueCh3($tenue_ch3)
    {
        $this->tenue_ch3 = $tenue_ch3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneACh3()
    {
        return $this->stock_interne_a_ch3;
    }

    /**
     * @param mixed $stock_interne_a_ch3
     *
     * @return self
     */
    public function setStockInterneACh3($stock_interne_a_ch3)
    {
        $this->stock_interne_a_ch3 = $stock_interne_a_ch3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueCh4()
    {
        return $this->tenue_ch4;
    }

    /**
     * @param mixed $tenue_ch4
     *
     * @return self
     */
    public function setTenueCh4($tenue_ch4)
    {
        $this->tenue_ch4 = $tenue_ch4;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneACh4()
    {
        return $this->stock_interne_a_ch4;
    }

    /**
     * @param mixed $stock_interne_a_ch4
     *
     * @return self
     */
    public function setStockInterneACh4($stock_interne_a_ch4)
    {
        $this->stock_interne_a_ch4 = $stock_interne_a_ch4;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueDsi()
    {
        return $this->tenue_dsi;
    }

    /**
     * @param mixed $tenue_dsi
     *
     * @return self
     */
    public function setTenueDsi($tenue_dsi)
    {
        $this->tenue_dsi = $tenue_dsi;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneADsi()
    {
        return $this->stock_interne_a_dsi;
    }

    /**
     * @param mixed $stock_interne_a_dsi
     *
     * @return self
     */
    public function setStockInterneADsi($stock_interne_a_dsi)
    {
        $this->stock_interne_a_dsi = $stock_interne_a_dsi;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueLi1()
    {
        return $this->tenue_li1;
    }

    /**
     * @param mixed $tenue_li1
     *
     * @return self
     */
    public function setTenueLi1($tenue_li1)
    {
        $this->tenue_li1 = $tenue_li1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneALi1()
    {
        return $this->stock_interne_a_li1;
    }

    /**
     * @param mixed $stock_interne_a_li1
     *
     * @return self
     */
    public function setStockInterneALi1($stock_interne_a_li1)
    {
        $this->stock_interne_a_li1 = $stock_interne_a_li1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueNa1()
    {
        return $this->tenue_na1;
    }

    /**
     * @param mixed $tenue_na1
     *
     * @return self
     */
    public function setTenueNa1($tenue_na1)
    {
        $this->tenue_na1 = $tenue_na1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneANa1()
    {
        return $this->stock_interne_a_na1;
    }

    /**
     * @param mixed $stock_interne_a_na1
     *
     * @return self
     */
    public function setStockInterneANa1($stock_interne_a_na1)
    {
        $this->stock_interne_a_na1 = $stock_interne_a_na1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenueVe1()
    {
        return $this->tenue_ve1;
    }

    /**
     * @param mixed $tenue_ve1
     *
     * @return self
     */
    public function setTenueVe1($tenue_ve1)
    {
        $this->tenue_ve1 = $tenue_ve1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneAVe1()
    {
        return $this->stock_interne_a_ve1;
    }

    /**
     * @param mixed $stock_interne_a_ve1
     *
     * @return self
     */
    public function setStockInterneAVe1($stock_interne_a_ve1)
    {
        $this->stock_interne_a_ve1 = $stock_interne_a_ve1;

        return $this;
    }
}
