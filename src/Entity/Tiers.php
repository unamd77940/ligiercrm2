<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TiersRepository")
 * @Table(indexes={@Index(name="codex3_idx", columns={"codex3"})})
 */
class Tiers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    
    /**
     * @ORM\Column(type="string", length=15)
     */
    private $codex3;

    
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $type_tiers;

    
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $code_categorie;

    
    /**
     * @ORM\Column(type="string", length=71, nullable=true)
     */
    private $raison_sociale;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $encours;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $max_encours;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $retard_paiement;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $blocage;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $sigle;

    
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $site;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $taille;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $profession;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $franco;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $num_siret;

    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $code_naf;

    
    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $iban;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $bic;

    
    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $domiciliation;

    
    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $rep_gestion;

    
    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $rep_gestion2;

    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_activation;

    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_desactivation;

    
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $statut_tiers;

    
    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $code_cond_paie;

    
    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $code_origine_tiers;

    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $type_prospect;

    
    /**
     * @ORM\Column(type="string", length=3)
     */
    private $langue_tiers;

    
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $tva_intracom;

    
    /**
     * @ORM\Column(type="string", length=3)
     */
    private $pays_tiers;

    
    /**
     * @ORM\Column(type="datetime")
     */
    private $last_x3_update;

    
    /**
     * @ORM\Column(type="datetime")
     */
    private $last_mysql_update;

    /**
     * @ORM\Column(type="date")
     */
    private $date_dernier_contact;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $addedit_flag;

/**
     * @ORM\Column(type="boolean")
     */
    private $envoi_sms;
   
    /**
     * @ORM\Column(type="boolean")
     */
    private $envoi_email;
    
    /**Champs spé Ligier TIERS***********************************************/
    
    /**
     * @ORM\Column(type="integer")
     */
    private $telephone;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $portable;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $date_naissance;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $situation_familiale;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $logement;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $hebergement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $curatelle_tutelle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mode_transport;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $loisirs;

    /**
     * @ORM\Column(type="boolean")
     */
    private $noctambule;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $compte_twitter;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $compte_facebook;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $compte_google;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $commentaires;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $statut_client;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $csp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type_revenu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tranche_revenu;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $taux_endettement;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $prenom;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse3;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ville;
    
    /**
     * @ORM\Column(type="time")
     */
    private $visite_pref_debut;
    
    /**
     * @ORM\Column(type="time")
     */
    private $visite_pref_fin;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $distributeur_id;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $canton;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $secteur;
    

    
    /************************************************************************/
    
    
    
    public function __construct()
    {
        // $this->contacts = new ArrayCollection();
        // $this->memos = new ArrayCollection();
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->codex3;
    }

    /**
     * @param mixed $codex3
     *
     * @return self
     */
    public function setCodeX3($codex3)
    {
        $this->codex3 = $codex3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeTiers()
    {
        return $this->type_tiers;
    }

    /**
     * @param mixed $type_tiers
     *
     * @return self
     */
    public function setTypeTiers($type_tiers)
    {
        $this->type_tiers = $type_tiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeCategorie()
    {
        return $this->code_categorie;
    }

    /**
     * @param mixed $code_categorie
     *
     * @return self
     */
    public function setCodeCategorie($code_categorie)
    {
        $this->code_categorie = $code_categorie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRaisonSociale()
    {
        return $this->raison_sociale;
    }

    /**
     * @param mixed $raison_sociale
     *
     * @return self
     */
    public function setRaisonSociale($raison_sociale)
    {
        $this->raison_sociale = $raison_sociale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEncours()
    {
        return $this->encours;
    }

    /**
     * @param mixed $encours
     *
     * @return self
     */
    public function setEncours($encours)
    {
        $this->encours = $encours;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxEncours()
    {
        return $this->max_encours;
    }

    /**
     * @param mixed $max_encours
     *
     * @return self
     */
    public function setMaxEncours($max_encours)
    {
        $this->max_encours = $max_encours;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetardPaiement()
    {
        return $this->retard_paiement;
    }

    /**
     * @param mixed $retard_paiement
     *
     * @return self
     */
    public function setRetardPaiement($retard_paiement)
    {
        $this->retard_paiement = $retard_paiement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlocage()
    {
        return $this->blocage;
    }

    /**
     * @param mixed $blocage
     *
     * @return self
     */
    public function setBlocage($blocage)
    {
        $this->blocage = $blocage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    /**
     * @param mixed $sigle
     *
     * @return self
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     *
     * @return self
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     *
     * @return self
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param mixed $profession
     *
     * @return self
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFranco()
    {
        return $this->franco;
    }

    /**
     * @param mixed $franco
     *
     * @return self
     */
    public function setFranco($franco)
    {
        $this->franco = $franco;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumSiret()
    {
        return $this->num_siret;
    }

    /**
     * @param mixed $num_siret
     *
     * @return self
     */
    public function setNumSiret($num_siret)
    {
        $this->num_siret = $num_siret;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeNaf()
    {
        return $this->code_naf;
    }

    /**
     * @param mixed $code_naf
     *
     * @return self
     */
    public function setCodeNaf($code_naf)
    {
        $this->code_naf = $code_naf;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param mixed $iban
     *
     * @return self
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param mixed $bic
     *
     * @return self
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomiciliation()
    {
        return $this->domiciliation;
    }

    /**
     * @param mixed $domiciliation
     *
     * @return self
     */
    public function setDomiciliation($domiciliation)
    {
        $this->domiciliation = $domiciliation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepGestion()
    {
        return $this->rep_gestion;
    }

    /**
     * @param mixed $rep_gestion
     *
     * @return self
     */
    public function setRepGestion($rep_gestion)
    {
        $this->rep_gestion = $rep_gestion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepGestion2()
    {
        return $this->rep_gestion2;
    }

    /**
     * @param mixed $rep_gestion2
     *
     * @return self
     */
    public function setRepGestion2($rep_gestion2)
    {
        $this->rep_gestion2 = $rep_gestion2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateActivation()
    {
        return $this->date_activation;
    }

    /**
     * @param mixed $date_activation
     *
     * @return self
     */
    public function setDateActivation($date_activation)
    {
        $this->date_activation = $date_activation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateDesactivation()
    {
        return $this->date_desactivation;
    }

    /**
     * @param mixed $date_desactivation
     *
     * @return self
     */
    public function setDateDesactivation($date_desactivation)
    {
        $this->date_desactivation = $date_desactivation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatutTiers()
    {
        return $this->statut_tiers;
    }

    /**
     * @param mixed $statut_tiers
     *
     * @return self
     */
    public function setStatutTiers($statut_tiers)
    {
        $this->statut_tiers = $statut_tiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeCondPaie()
    {
        return $this->code_cond_paie;
    }

    /**
     * @param mixed $code_cond_paie
     *
     * @return self
     */
    public function setCodeCondPaie($code_cond_paie)
    {
        $this->code_cond_paie = $code_cond_paie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeOrigineTiers()
    {
        return $this->code_origine_tiers;
    }

    /**
     * @param mixed $code_origine_tiers
     *
     * @return self
     */
    public function setCodeOrigineTiers($code_origine_tiers)
    {
        $this->code_origine_tiers = $code_origine_tiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeProspect()
    {
        return $this->type_prospect;
    }

    /**
     * @param mixed $type_prospect
     *
     * @return self
     */
    public function setTypeProspect($type_prospect)
    {
        $this->type_prospect = $type_prospect;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLangueTiers()
    {
        return $this->langue_tiers;
    }

    /**
     * @param mixed $langue_tiers
     *
     * @return self
     */
    public function setLangueTiers($langue_tiers)
    {
        $this->langue_tiers = $langue_tiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTvaIntracom()
    {
        return $this->tva_intracom;
    }

    /**
     * @param mixed $tva_intracom
     *
     * @return self
     */
    public function setTvaIntracom($tva_intracom)
    {
        $this->tva_intracom = $tva_intracom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaysTiers()
    {
        return $this->pays_tiers;
    }

    /**
     * @param mixed $pays_tiers
     *
     * @return self
     */
    public function setPaysTiers($pays_tiers)
    {
        $this->pays_tiers = $pays_tiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastX3Update()
    {
        return $this->last_x3_update;
    }

    /**
     * @param mixed $last_x3_update
     *
     * @return self
     */
    public function setLastX3Update($last_x3_update)
    {
        $this->last_x3_update = $last_x3_update;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastMysqlUpdate()
    {
        return $this->last_mysql_update;
    }

    /**
     * @param mixed $last_mysql_update
     *
     * @return self
     */
    public function setLastMysqlUpdate($last_mysql_update)
    {
        $this->last_mysql_update = $last_mysql_update;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate_dernier_contact()
    {
        return $this->date_dernier_contact;
    }

    /**
     * @param mixed $date_dernier_contact
     *
     * @return self
     */
    public function setDate_dernier_contact($date_dernier_contact)
    {
        $this->last_contact = $date_dernier_contact;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAddeditFlag()
    {
        return $this->addedit_flag;
    }

    /**
     * @param mixed $addedit_flag
     *
     * @return self
     */
    public function setAddeditFlag($addedit_flag)
    {
        $this->addedit_flag = $addedit_flag;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getEnvoi_sms()
    {
        return $this->envoi_sms;
    }

    /**
     * @param mixed $sms
     *
     * @return self
     */
    public function setEnvoi_sms($envoi_sms)
    {
        $this->envoi_sms = $envoi_sms;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getEnvoi_email()
    {
        return $this->envoi_email;
    }

    /**
     * @param mixed $envoi_email
     *
     * @return self
     */
    public function setEnvoi_email($envoi_email)
    {
        $this->envoi_email = $envoi_email;

        return $this;
    }
    
    /**Champs spé Ligier TIERS***********************************************/
    /**
     * @return mixed
     */
    
    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     *
     * @return self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getPortable()
    {
        return $this->portable;
    }

    /**
     * @param mixed $portable
     *
     * @return self
     */
    public function setPortable($portable)
    {
        $this->portable = $portable;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
    
    public function getDate_naissance()
    {
        return $this->date_naissance;
    }

    /**
     * @param mixed $date_naissance
     *
     * @return self
     */
    public function setDate_naissance($date_naissance)
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getSituation_familliale()
    {
        return $this->situation_familliale;
    }

    /**
     * @param mixed $situation_familliale
     *
     * @return self
     */
    public function setSituation_familliale($situation_familliale)
    {
        $this->situation_familliale = $situation_familliale;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getLogement()
    {
        return $this->logement;
    }

    /**
     * @param mixed $logement
     *
     * @return self
     */
    public function setLogement($logement)
    {
        $this->logement = $logement;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getHebergement()
    {
        return $this->hebergement;
    }

    /**
     * @param mixed $hebergement
     *
     * @return self
     */
    public function setHebergement($hebergement)
    {
        $this->hebergement = $hebergement;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getCuratelle_tutelle()
    {
        return $this->curatelle_tutelle;
    }

    /**
     * @param mixed $curatelle_tutelle
     *
     * @return self
     */
    public function setCuratelle_tutelle($curatelle_tutelle)
    {
        $this->curatelle_tutelle = $curatelle_tutelle;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getMode_transport()
    {
        return $this->mode_transport;
    }

    /**
     * @param mixed $mode_transport
     *
     * @return self
     */
    public function setMode_transport($mode_transport)
    {
        $this->mode_transport = $mode_transport;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getLoisirs()
    {
        return $this->loisirs;
    }

    /**
     * @param mixed $loisirs
     *
     * @return self
     */
    public function setLoisirs($loisirs)
    {
        $this->loisirs = $loisirs;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getNoctambule()
    {
        return $this->noctambule;
    }

    /**
     * @param mixed $noctambule
     *
     * @return self
     */
    public function setNoctambule($noctambule)
    {
        $this->noctambule = $noctambule;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getCompte_twitter()
    {
        return $this->compte_twitter;
    }

    /**
     * @param mixed $compte_twitter
     *
     * @return self
     */
    public function setCompte_twitter($compte_twitter)
    {
        $this->compte_twitter = $compte_twitter;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getCompte_facebook()
    {
        return $this->compte_facebook;
    }

    /**
     * @param mixed $compte_facebook
     *
     * @return self
     */
    public function setCompte_facebook($compte_facebook)
    {
        $this->compte_facebook = $compte_facebook;

        return $this;
    } 
      
    /**
     * @return mixed
     */
    public function getCompte_google()
    {
        return $this->compte_google;
    }

    /**
     * @param mixed $compte_google
     *
     * @return self
     */
    public function setCompte_google($compte_google)
    {
        $this->compte_google = $compte_google;

        return $this;
    } 
      
    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    } 
       
    /**
     * @return mixed
     */
    public function getStatut_client()
    {
        return $this->statut_client;
    }

    /**
     * @param mixed $statut_client
     *
     * @return self
     */
    public function setStatut_client($statut_client)
    {
        $this->statut_client = $statut_client;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getCsp()
    {
        return $this->csp;
    }

    /**
     * @param mixed $csp
     *
     * @return self
     */
    public function setCsp($csp)
    {
        $this->csp = $csp;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getType_revenu()
    {
        return $this->type_revenu;
    }

    /**
     * @param mixed $type_revenu
     *
     * @return self
     */
    public function setType_revenu($type_revenu)
    {
        $this->type_revenu = $type_revenu;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getTranche_revenu()
    {
        return $this->tranche_revenu;
    }

    /**
     * @param mixed $tranche_revenu
     *
     * @return self
     */
    public function setTranche_revenu($tranche_revenu)
    {
        $this->tranche_revenu = $tranche_revenu;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getTaux_endettement()
    {
        return $this->taux_endettement;
    }

    /**
     * @param mixed $taux_endettement
     *
     * @return self
     */
    public function setTaux_endettement($taux_endettement)
    {
        $this->taux_endettement = $taux_endettement;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getVisite_pref_debut()
    {
        return $this->visite_pref_debut;
    }

    /**
     * @param mixed $visite_pref_debut
     *
     * @return self
     */
    public function setVisite_pref_debut($visite_pref_debut)
    {
        $this->visite_pref_debut = $visite_pref_debut;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getVisite_pref_fin()
    {
        return $this->visite_pref_fin;
    }

    /**
     * @param mixed $visite_pref_fin
     *
     * @return self
     */
    public function setVisite_pref_fin($visite_pref_fin)
    {
        $this->visite_pref_fin = $visite_pref_fin;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     *
     * @return self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     *
     * @return self
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     *
     * @return self
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * @param mixed $adresse1
     *
     * @return self
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param mixed $adresse2
     *
     * @return self
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAdresse3()
    {
        return $this->adresse3;
    }

    /**
     * @param mixed $adresse3
     *
     * @return self
     */
    public function setAdresse3($adresse3)
    {
        $this->adresse3 = $adresse3;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCode_postal()
    {
        return $this->code_postal;
    }

    /**
     * @param mixed $code_postal
     *
     * @return self
     */
    public function setCode_postal($code_postal)
    {
        $this->code_postal = $code_postal;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     *
     * @return self
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getDistributeur_id()
    {
        return $this->distributeur_id;
    }

    /**
     * @param mixed $distributeur_id
     *
     * @return self
     */
    public function setDistributeur_id($distributeur_id)
    {
        $this->distributeur_id = $distributeur_id;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCanton()
    {
        return $this->canton;
    }

    /**
     * @param mixed $canton
     *
     * @return self
     */
    public function setCanton($canton)
    {
        $this->canton = $canton;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getSecteur()
    {
        return $this->secteur;
    }

    /**
     * @param mixed $secteur
     *
     * @return self
     */
    public function setSecteur($secteur)
    {
        $this->secteur = $secteur;

        return $this;
    }
    /**********************************************************************/
    
    

    
}
