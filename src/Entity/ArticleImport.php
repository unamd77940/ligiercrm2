<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleImportRepository")
 */
class ArticleImport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_x3;
  

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $site;


    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $fournisseur;


    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $uv;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $uf;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cdt;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix_tarif;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix_revient_rennes;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix_revient_lille;


    /**
     * @ORM\Column(type="float")
     */
    private $prix_median;


    /**
     * @ORM\Column(type="integer")
     */
    private $tenue;


    /**
     * @ORM\Column(type="integer")
     */
    private $stock_interne_a;

    


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;
    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     *
     * @return self
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     *
     * @return self
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * @param mixed $fournisseur
     *
     * @return self
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUv()
    {
        return $this->uv;
    }

    /**
     * @param mixed $uv
     *
     * @return self
     */
    public function setUv($uv)
    {
        $this->uv = $uv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     *
     * @return self
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCdt()
    {
        return $this->cdt;
    }

    /**
     * @param mixed $cdt
     *
     * @return self
     */
    public function setCdt($cdt)
    {
        $this->cdt = $cdt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixTarif()
    {
        return $this->prix_tarif;
    }

    /**
     * @param mixed $prix_tarif
     *
     * @return self
     */
    public function setPrixTarif($prix_tarif)
    {
        $this->prix_tarif = $prix_tarif;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixRevientRennes()
    {
        return $this->prix_revient_rennes;
    }

    /**
     * @param mixed $prix_revient_rennes
     *
     * @return self
     */
    public function setPrixRevientRennes($prix_revient_rennes)
    {
        $this->prix_revient_rennes = $prix_revient_rennes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixRevientLille()
    {
        return $this->prix_revient_lille;
    }

    /**
     * @param mixed $prix_revient_lille
     *
     * @return self
     */
    public function setPrixRevientLille($prix_revient_lille)
    {
        $this->prix_revient_lille = $prix_revient_lille;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrixMedian()
    {
        return $this->prix_median;
    }

    /**
     * @param mixed $prix_median
     *
     * @return self
     */
    public function setPrixMedian($prix_median)
    {
        $this->prix_median = $prix_median;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTenue()
    {
        return $this->tenue;
    }

    /**
     * @param mixed $tenue
     *
     * @return self
     */
    public function setTenue($tenue)
    {
        $this->tenue = $tenue;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockInterneA()
    {
        return $this->stock_interne_a;
    }

    /**
     * @param mixed $stock_interne_a
     *
     * @return self
     */
    public function setStockInterneA($stock_interne_a)
    {
        $this->stock_interne_a = $stock_interne_a;

        return $this;
    }
}
