<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientSynchroRepository")
 */
class ClientSynchro
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    
    /**
     * @ORM\Column(type="string", length=15)
     */
    private $code_x3;

    
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $type_tiers;

    
    /**
     * @ORM\Column(type="string", length=71, nullable=true)
     */
    private $raison_sociale;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $encours;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $max_encours;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $retard_paiement;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $profession;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $franco;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $num_siret;

    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $code_naf;

    
    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $iban;

    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $bic;

    
    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $code_cond_paie;

    
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $tva_intracom;
   
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $synchro;
    
    /**
     * @ORM\Column(type="date")
     */
    private $date_dernier_contact;
/**Champs spé Ligier TIERS***********************************************
     * 
     * @ORM\Column(type="datetime")
     */
    private $date_naissance;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $situation_familiale;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $logement;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $hebergement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $curatelle_tutelle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mode_transport;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $loisirs;

    /**
     * @ORM\Column(type="boolean")
     */
    private $noctambule;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $compte_twitter;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $compte_facebook;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $compte_google;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $commentaires;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $statut_client;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $csp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type_revenu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tranche_revenu;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $taux_endettement;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $prenom;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;
    

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse3;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ville;
    
    /**
     * @ORM\Column(type="time")
     */
    private $visite_pref_debut;
    
    /**
     * @ORM\Column(type="time")
     */
    private $visite_pref_fin;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $distributeur_id;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $canton;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $secteur;
    
   
  /******************************************/
    



    public function __construct()
    {
        // $this->contacts = new ArrayCollection();
        // $this->memos = new ArrayCollection();
        $this->synchro = 0;
        $this->type_tiers ="C";
    }



    public function setFromSynchro($client, $type = 'C') {  
        // Ajout des valeurs du client à l'entité
    	if(isset($client->code)) $this->setCodeX3($client->code);
    	if(isset($client->raisonsociale)) $this->setRaisonSociale($client->raisonsociale);
    	if(isset($client->encours)) $this->setEncours($client->encours);
    	if(isset($client->max)) $this->setMaxEncours($client->max);
    	if(isset($client->retardpaiement)) $this->setRetardPaiement($client->retardpaiement);
    	// if(isset($client->etat)) $this->setEtat($client->etat);
    	if(isset($client->activite_id)) $this->setProfession($client->activite_id);
    	if(isset($client->franco)) $this->setFranco($client->franco);
    	if(isset($client->siret)) $this->setNumSiret($client->siret);
    	if(isset($client->naf)) $this->setCodeNaf($client->naf);
    	if(isset($client->tva)) $this->setTvaIntracom($client->tva);
    	if(isset($client->iban)) $this->setIban($client->iban);
    	if(isset($client->bic)) $this->setBic($client->bic);
    	if(isset($client->condition_id)) $this->setCodeCondPaie($client->condition_id);
    	//Ajout champs spé Tiers ligier
        if(isset($client->date_dernier_contact)) $this->setDate_dernier_contact($client->last_contact);
        
        if(isset($client->date_naissance)) $this->setDate_naissance($client->date_naissance);
        if(isset($client->situation_familliale)) $this->setSituation_familliale($client->situation_familliale);
        if(isset($client->logement)) $this->setLogement($client->logement);
        if(isset($client->hebergement)) $this->setHebergement($client->hebergement);
        if(isset($client->curatelle_tutelle)) $this->setCuratelle_tutelle($client->curatelle_tutelle);
        if(isset($client->mode_transport)) $this->setMode_transport($client->mode_transport);
        if(isset($client->loisirs)) $this->setLoisirs($client->loisirs);
        if(isset($client->noctambule)) $this->setNoctambule($client->noctambule);
        if(isset($client->compte_twitter)) $this->setCompte_twitter($client->compte_twitter);
        if(isset($client->compte_facebook)) $this->setCompte_facebook($client->compte_facebook);
        if(isset($client->compte_google)) $this->setCompte_google($client->compte_google);
        if(isset($client->commentaires)) $this->setCommentaires($client->commentaires);
        if(isset($client->statut_client)) $this->setStatut_client($client->statut_client);
        if(isset($client->csp)) $this->setCsp($client->csp);
        if(isset($client->type_revenu)) $this->setType_revenu($client->type_revenu);
        if(isset($client->tranche_revenu)) $this->setTranche_revenu($client->tranche_revenu);
        if(isset($client->taux_endettement)) $this->setTaux_endettement($client->taux_endettement);
        if(isset($client->latitude)) $this->setLatitudet($client->latitude);
        if(isset($client->longitude)) $this->setLongitude($client->longitude);

        
        //
        
        
    	return $this;
    }


    public function getAllVariables($exclude = []) {
        $objToArray = get_object_vars($this);
        // Si besoin, supprime certaines clés inutiles
        foreach ($exclude as $key => $toRemove) {
            try {
                unset($objToArray[$toRemove]);
            } catch(Exception $e) {

            }
        }

        // conversion des noms des champs en clés utilisées dans le JSON
        // $conv = [
        //             ["code_x3", "code"],
        //             ["is_default", "default"],
        //             ["mailc", "email"],
        //             ["telc", "telephone"],
        //             ["mobilec", "portable"],
        //             ["faxc", "fax"],
        //             ["mailc", "email"],
        //         ];

        // foreach ($conv as $key => $value) {
        //     if(isset($objToArray[$value[0]])) {
        //         $objToArray[$value[1]] = $objToArray[$value[0]];
        //         unset($objToArray[$value[0]]);
        //     }
        // }

        return $objToArray;
    }


    

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;
    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeTiers()
    {
        return $this->type_tiers;
    }

    /**
     * @param mixed $type_tiers
     *
     * @return self
     */
    public function setTypeTiers($type_tiers)
    {
        $this->type_tiers = $type_tiers;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRaisonSociale()
    {
        return $this->raison_sociale;
    }

    /**
     * @param mixed $raison_sociale
     *
     * @return self
     */
    public function setRaisonSociale($raison_sociale)
    {
        $this->raison_sociale = $raison_sociale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEncours()
    {
        return $this->encours;
    }

    /**
     * @param mixed $encours
     *
     * @return self
     */
    public function setEncours($encours)
    {
        $this->encours = $encours;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxEncours()
    {
        return $this->max_encours;
    }

    /**
     * @param mixed $max_encours
     *
     * @return self
     */
    public function setMaxEncours($max_encours)
    {
        $this->max_encours = $max_encours;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetardPaiement()
    {
        return $this->retard_paiement;
    }

    /**
     * @param mixed $retard_paiement
     *
     * @return self
     */
    public function setRetardPaiement($retard_paiement)
    {
        $this->retard_paiement = $retard_paiement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param mixed $profession
     *
     * @return self
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFranco()
    {
        return $this->franco;
    }

    /**
     * @param mixed $franco
     *
     * @return self
     */
    public function setFranco($franco)
    {
        $this->franco = $franco;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumSiret()
    {
        return $this->num_siret;
    }

    /**
     * @param mixed $num_siret
     *
     * @return self
     */
    public function setNumSiret($num_siret)
    {
        $this->num_siret = $num_siret;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeNaf()
    {
        return $this->code_naf;
    }

    /**
     * @param mixed $code_naf
     *
     * @return self
     */
    public function setCodeNaf($code_naf)
    {
        $this->code_naf = $code_naf;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param mixed $iban
     *
     * @return self
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param mixed $bic
     *
     * @return self
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeCondPaie()
    {
        return $this->code_cond_paie;
    }

    /**
     * @param mixed $code_cond_paie
     *
     * @return self
     */
    public function setCodeCondPaie($code_cond_paie)
    {
        $this->code_cond_paie = $code_cond_paie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTvaIntracom()
    {
        return $this->tva_intracom;
    }

    /**
     * @param mixed $tva_intracom
     *
     * @return self
     */
    public function setTvaIntracom($tva_intracom)
    {
        $this->tva_intracom = $tva_intracom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSynchro()
    {
        return $this->synchro;
    }

    /**
     * @param mixed $synchro
     *
     * @return self
     */
    public function setSynchro($synchro)
    {
        $this->synchro = $synchro;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getLast_contact()
    {
        return $this->last_contact;
    }

    /**
     * @param mixed $last_contact
     *
     * @return self
     */
    public function setLast_contact($last_contact)
    {
        $this->last_contact = $last_contact;

        return $this;
    }
    
    /**Champs spé Ligier TIERS***********************************************/
    /**
     * @return mixed
     */
    public function getDate_naissance()
    {
        return $this->date_naissance;
    }

    /**
     * @param mixed $date_naissance
     *
     * @return self
     */
    public function setDate_naissance($date_naissance)
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getSituation_familliale()
    {
        return $this->situation_familliale;
    }

    /**
     * @param mixed $situation_familliale
     *
     * @return self
     */
    public function setSituation_familliale($situation_familliale)
    {
        $this->situation_familliale = $situation_familliale;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getLogement()
    {
        return $this->logement;
    }

    /**
     * @param mixed $logement
     *
     * @return self
     */
    public function setLogement($logement)
    {
        $this->logement = $logement;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getHebergement()
    {
        return $this->hebergement;
    }

    /**
     * @param mixed $hebergement
     *
     * @return self
     */
    public function setHebergement($hebergement)
    {
        $this->hebergement = $hebergement;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getCuratelle_tutelle()
    {
        return $this->curatelle_tutelle;
    }

    /**
     * @param mixed $curatelle_tutelle
     *
     * @return self
     */
    public function setCuratelle_tutelle($curatelle_tutelle)
    {
        $this->curatelle_tutelle = $curatelle_tutelle;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getMode_transport()
    {
        return $this->mode_transport;
    }

    /**
     * @param mixed $mode_transport
     *
     * @return self
     */
    public function setMode_transport($mode_transport)
    {
        $this->mode_transport = $mode_transport;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getLoisirs()
    {
        return $this->loisirs;
    }

    /**
     * @param mixed $loisirs
     *
     * @return self
     */
    public function setLoisirs($loisirs)
    {
        $this->loisirs = $loisirs;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getNoctambule()
    {
        return $this->noctambule;
    }

    /**
     * @param mixed $noctambule
     *
     * @return self
     */
    public function setNoctambule($noctambule)
    {
        $this->noctambule = $noctambule;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getCompte_twitter()
    {
        return $this->compte_twitter;
    }

    /**
     * @param mixed $compte_twitter
     *
     * @return self
     */
    public function setCompte_twitter($compte_twitter)
    {
        $this->compte_twitter = $compte_twitter;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getCompte_facebook()
    {
        return $this->compte_facebook;
    }

    /**
     * @param mixed $compte_facebook
     *
     * @return self
     */
    public function setCompte_facebook($compte_facebook)
    {
        $this->compte_facebook = $compte_facebook;

        return $this;
    } 
      
    /**
     * @return mixed
     */
    public function getCompte_google()
    {
        return $this->compte_google;
    }

    /**
     * @param mixed $compte_google
     *
     * @return self
     */
    public function setCompte_google($compte_google)
    {
        $this->compte_google = $compte_google;

        return $this;
    } 
      
    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    } 
       
    /**
     * @return mixed
     */
    public function getStatut_client()
    {
        return $this->statut_client;
    }

    /**
     * @param mixed $statut_client
     *
     * @return self
     */
    public function setStatut_client($statut_client)
    {
        $this->statut_client = $statut_client;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getCsp()
    {
        return $this->csp;
    }

    /**
     * @param mixed $csp
     *
     * @return self
     */
    public function setCsp($csp)
    {
        $this->csp = $csp;

        return $this;
    } 
     
    /**
     * @return mixed
     */
    public function getType_revenu()
    {
        return $this->type_revenu;
    }

    /**
     * @param mixed $type_revenu
     *
     * @return self
     */
    public function setType_revenu($type_revenu)
    {
        $this->type_revenu = $type_revenu;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getTranche_revenu()
    {
        return $this->tranche_revenu;
    }

    /**
     * @param mixed $tranche_revenu
     *
     * @return self
     */
    public function setTranche_revenu($tranche_revenu)
    {
        $this->tranche_revenu = $tranche_revenu;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getTaux_endettement()
    {
        return $this->taux_endettement;
    }

    /**
     * @param mixed $taux_endettement
     *
     * @return self
     */
    public function setTaux_endettement($taux_endettement)
    {
        $this->taux_endettement = $taux_endettement;

        return $this;
    } 
    
    /**
     * @return mixed
     */
    public function getVisite_pref_debut()
    {
        return $this->visite_pref_debut;
    }

    /**
     * @param mixed $visite_pref_debut
     *
     * @return self
     */
    public function setVisite_pref_debut($visite_pref_debut)
    {
        $this->visite_pref_debut = $visite_pref_debut;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getVisite_pref_fin()
    {
        return $this->visite_pref_fin;
    }

    /**
     * @param mixed $visite_pref_fin
     *
     * @return self
     */
    public function setVisite_pref_fin($visite_pref_fin)
    {
        $this->visite_pref_fin = $visite_pref_fin;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     *
     * @return self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     *
     * @return self
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     *
     * @return self
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * @param mixed $adresse1
     *
     * @return self
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param mixed $adresse2
     *
     * @return self
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getAdresse3()
    {
        return $this->adresse3;
    }

    /**
     * @param mixed $adresse3
     *
     * @return self
     */
    public function setAdresse3($adresse3)
    {
        $this->adresse3 = $adresse3;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCode_postal()
    {
        return $this->code_postal;
    }

    /**
     * @param mixed $code_postal
     *
     * @return self
     */
    public function setCode_postal($code_postal)
    {
        $this->code_postal = $code_postal;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     *
     * @return self
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getDistributeur_id()
    {
        return $this->distributeur_id;
    }

    /**
     * @param mixed $distributeur_id
     *
     * @return self
     */
    public function setDistributeur_id($distributeur_id)
    {
        $this->distributeur_id = $distributeur_id;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getCanton()
    {
        return $this->canton;
    }

    /**
     * @param mixed $canton
     *
     * @return self
     */
    public function setCanton($canton)
    {
        $this->canton = $canton;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getSecteur()
    {
        return $this->secteur;
    }

    /**
     * @param mixed $secteur
     *
     * @return self
     */
    public function setSecteur($secteur)
    {
        $this->secteur = $secteur;

        return $this;
    }
    /**********************************************************************/
     
}
