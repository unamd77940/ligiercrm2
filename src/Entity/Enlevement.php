<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnlevementRepository")
 */
class Enlevement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

            
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $code;

            
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $client_code;

            
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $client_raisonsociale;
            
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $adresse_code;
            
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $demande;
        
    /**
     * @ORM\Column(type="boolean")
     */
    private $mail_envoye;

    public function __construct() {
        $this->mail_envoye = 0;
    }


    public function setFromSynchro($valueEnlevement) {  
    	if(isset($valueEnlevement->clients)) $this->setClientCode($valueEnlevement->clients);
    	if(isset($valueEnlevement->adressecode)) $this->setAdresseCode($valueEnlevement->adressecode);
    	if(isset($valueEnlevement->demande)) $this->setDemande($valueEnlevement->demande);
    	if(isset($valueEnlevement->code)) $this->setCode($valueEnlevement->code);
    	if(isset($valueEnlevement->date)) {
    		$date = (new \Datetime)::createFromFormat('Y-m-d', $valueEnlevement->date);
    		$this->setDate($date);
    	}
    	if(isset($valueEnlevement->raisonsociale)) $this->setClientRaisonsociale($valueEnlevement->raisonsociale);
    	if(isset($valueEnlevement->adresse_nom)) $this->setAdresse($valueEnlevement->adresse_nom);

    	return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientCode()
    {
        return $this->client_code;
    }

    /**
     * @param mixed $client_code
     *
     * @return self
     */
    public function setClientCode($client_code)
    {
        $this->client_code = $client_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientRaisonsociale()
    {
        return $this->client_raisonsociale;
    }

    /**
     * @param mixed $client_raisonsociale
     *
     * @return self
     */
    public function setClientRaisonsociale($client_raisonsociale)
    {
        $this->client_raisonsociale = $client_raisonsociale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseCode()
    {
        return $this->adresse_code;
    }

    /**
     * @param mixed $adresse_code
     *
     * @return self
     */
    public function setAdresseCode($adresse_code)
    {
        $this->adresse_code = $adresse_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     *
     * @return self
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * @param mixed $demande
     *
     * @return self
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;

        return $this;
    }
}
