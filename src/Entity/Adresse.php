<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;



/**
 * @ORM\Entity(repositoryClass="App\Repository\AdresseRepository")
 * @Table(indexes={@Index(name="codetiersx3_idx", columns={"code_tiers_x3"})})
 */
class Adresse
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_x3;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_tiers_x3;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $address3;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $pays_code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $chantier;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $portable;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $fax;


    public function __construct() {
        $this->pays_code = 'FR';
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
                    fwrite($fichiergeoloc, "blabla");

    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;

    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeTiersX3()
    {
        return $this->code_tiers_x3;
    }

    /**
     * @param mixed $code_tiers_x3
     *
     * @return self
     */
    public function setCodeTiersX3($code_tiers_x3)
    {
        $this->code_tiers_x3 = $code_tiers_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     *
     * @return self
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     *
     * @return self
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * @param mixed $address3
     *
     * @return self
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaysCode()
    {
        return $this->pays_code;
    }

    /**
     * @param mixed $zip
     *
     * @return self
     */
    public function setPaysCode($pays_code)
    {
        $this->pays_code = $pays_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChantier()
    {
        return $this->chantier;
    }

    /**
     * @param mixed $chantier
     *
     * @return self
     */
    public function setChantier($chantier)
    {
        $this->chantier = $chantier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     *
     * @return self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPortable()
    {
        return $this->portable;
    }

    /**
     * @param mixed $portable
     *
     * @return self
     */
    public function setPortable($portable)
    {
        $this->portable = $portable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     *
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

}
