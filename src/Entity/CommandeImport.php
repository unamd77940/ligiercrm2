<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandeImportRepository")
 * @Table(indexes={@Index(name="code_idx", columns={"id", "code"})})
 */
class CommandeImport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $code;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $client_code;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $statut;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_etat;
        
        
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $articles;
        
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $adresse_id;
        
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $adresse_code;
        
    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $site_vente;
        
    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $site_expedition;
        
    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $mode_livraison;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaires;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ref;
        
    /**
     * @ORM\Column(type="float")
     */
    private $frais_transport;
        
    /**
     * @ORM\Column(type="float")
     */
    private $totalHT;
        
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_creation;


    public function __construct() {
    	$this->totalHT = 0;
    	$this->totalcommande = 0;
    	$this->frais_transport = 0;
    	$this->date_creation = new \Datetime;
    }	

    /*
    public function setFromSynchro($valueCommande) {  
        // Ajout des valeurs du devis à l'entité
        if(isset($valueCommande->code)) $this->setCode($valueCommande->code);
        if(isset($valueCommande->client_code)) $this->setClientCode($valueCommande->client_code);
        if(isset($valueCommande->date)) {
        	$formated = (new \Datetime)::createFromFormat('d-m-Y', $valueCommande->date);
        	if($formated) $this->setDate($formated);
        	else $this->setDateEtat(null);
        } else {
        	$this->setDate(null);
        }
        if(isset($valueCommande->etat)) $this->setEtat($valueCommande->etat);
        if(isset($valueCommande->date_etat)) {
        	$formated = (new \Datetime)::createFromFormat('d-m-Y', $valueCommande->date_etat);
        	if($formated) $this->setDateEtat($formated);
        	else $this->setDateEtat(null);
        } else {
        	$this->setDateEtat(null);
        }
        if(isset($valueCommande->statut)) $this->setStatut($valueCommande->statut);
        if(isset($valueCommande->perte)) $this->setPerte($valueCommande->perte);
        if(isset($valueCommande->observations)) $this->setObservations($valueCommande->observations);                
        if(isset($valueCommande->articles)) $this->setArticles($valueCommande->articles);
        if(isset($valueCommande->adresse_id)) $this->setAdresseId($valueCommande->adresse_id);
        if(isset($valueCommande->adresse_code)) $this->setAdresseCode($valueCommande->adresse_code);
        if(isset($valueCommande->commentaires)) $this->setCommentaires($valueCommande->commentaires);
        if(isset($valueCommande->ref)) $this->setRef($valueCommande->ref); 
        if(isset($valueCommande->frais_transport)) $this->setFraisTransport($valueCommande->frais_transport);
        if(isset($valueCommande->totalcommande)) $this->setTotaldevis($valueCommande->totalcommande);
        if(isset($valueCommande->date_creation)) {
        	$formated = (new \Datetime)::createFromFormat('Y-m-d', $valueCommande->date_creation);
        	$this->setDateCreation($formated);
        }
        if(isset($valueCommande->totalHT)) $this->setTotalHT($valueCommande->totalHT);
    }
    */





    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientCode()
    {
        return $this->client_code;
    }

    /**
     * @param mixed $client_code
     *
     * @return self
     */
    public function setClientCode($client_code)
    {
        $this->client_code = $client_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     *
     * @return self
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEtat()
    {
        return $this->date_etat;
    }

    /**
     * @param mixed $date_etat
     *
     * @return self
     */
    public function setDateEtat($date_etat)
    {
        $this->date_etat = $date_etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     *
     * @return self
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseId()
    {
        return $this->adresse_id;
    }

    /**
     * @param mixed $adresse_id
     *
     * @return self
     */
    public function setAdresseId($adresse_id)
    {
        $this->adresse_id = $adresse_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdresseCode()
    {
        return $this->adresse_code;
    }

    /**
     * @param mixed $adresse_code
     *
     * @return self
     */
    public function setAdresseCode($adresse_code)
    {
        $this->adresse_code = $adresse_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteVente()
    {
        return $this->site_vente;
    }

    /**
     * @param mixed $site_vente
     *
     * @return self
     */
    public function setSiteVente($site_vente)
    {
        $this->site_vente = $site_vente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteExpedition()
    {
        return $this->site_expedition;
    }

    /**
     * @param mixed $site_expedition
     *
     * @return self
     */
    public function setSiteExpedition($site_expedition)
    {
        $this->site_expedition = $site_expedition;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModeLivraison()
    {
        return $this->mode_livraison;
    }

    /**
     * @param mixed $mode_livraison
     *
     * @return self
     */
    public function setModeLivraison($mode_livraison)
    {
        $this->mode_livraison = $mode_livraison;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     *
     * @return self
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFraisTransport()
    {
        return $this->frais_transport;
    }

    /**
     * @param mixed $frais_transport
     *
     * @return self
     */
    public function setFraisTransport($frais_transport)
    {
        $this->frais_transport = $frais_transport;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param mixed $totalHT
     *
     * @return self
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     *
     * @return self
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }
}
