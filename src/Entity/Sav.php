<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SavRepository")
 * @Table(indexes={@Index(name="code_idx", columns={"id", "code_x3", "code_tiers_x3"})})
 */
class Sav
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
        
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $code_x3;
        
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $code_tiers_x3;
        
    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $etat;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre_atelier;

        
    /**
     * @ORM\Column(type="text")
     */
    private $commentaire;
        
    /**
     * @ORM\Column(type="date")
     */
    private $date;

    


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeX3()
    {
        return $this->code_x3;
    }

    /**
     * @param mixed $code_x3
     *
     * @return self
     */
    public function setCodeX3($code_x3)
    {
        $this->code_x3 = $code_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeTiersX3()
    {
        return $this->code_tiers_x3;
    }

    /**
     * @param mixed $code_tiers_x3
     *
     * @return self
     */
    public function setCodeTiersX3($code_tiers_x3)
    {
        $this->code_tiers_x3 = $code_tiers_x3;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     *
     * @return self
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     *
     * @return self
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitreAtelier()
    {
        return $this->titre_atelier;
    }

    /**
     * @param mixed $titre_atelier
     *
     * @return self
     */
    public function setTitreAtelier($titre_atelier)
    {
        $this->titre_atelier = $titre_atelier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     *
     * @return self
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }
}
