<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParametresValeurRepository")
 */
class ParametresValeur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $idparam;

    
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $valeur;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $codecouleur;

    /**
     * @ORM\Column(type="string")
     */
    private $commentaires;

    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    
    /**
     * @return mixed
     */
    public function getIdparam()
    {
        return $this->idparam;
    }

    /**
     * @param mixed $idparam
     *
     * @return self
     */
    public function setIdparam($idparam)
    {
        $this->idparam = $idparam;

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param mixed $valeur
     *
     * @return self
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodecouleur()
    {
        return $this->codecouleur;
    }

    /**
     * @param mixed $codecouleur
     *
     * @return self
     */
    public function setCodecouleur($codecouleur)
    {
        $this->codecouleur = $codecouleur;

        return $this;
    }
    
    
    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     *
     * @return self
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    }
    

}
