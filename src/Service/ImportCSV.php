<?php
namespace App\Service;

Class ImportCSV {
        
    public function convert($filename, $delimiter = ',')  {

        if(!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $fileContents = file_get_contents($filename);
        if (!mb_check_encoding($fileContents, 'UTF-8')) {
            file_put_contents($filename,utf8_encode($fileContents));
        }


        /*=======================================================================
        =            RECHERCHE DES DOUBLE GUILLEMETS DANS LES CHAMPS            =
        =======================================================================*/
        
        // TODO : VOIR AVEC DJO POUR AVOIR UN IMPORT PROPRE


        /*
        $arrayContent = preg_split("/\r\n|\r|\n/", $fileContents);

        foreach ($arrayContent as $key => $value) {
            
            $line = preg_split('/","|",|\r\n"|\r"|\n"/', $value);
            foreach ($line as $keyF => $field) {
                if($keyF) {
                    //var_dump($field);
                    $count = substr_count($field, '"');
                    if($count%2 !== 0) {
                        // Si on a un nombre impair de guillemet, on supprime le dernier
                        if(strrpos($field, '"')>=0) {
                            $field = substr_replace($field, '', strrpos($field, '"'), 1);
                            $arrayContent[$key] = str_replace($line[$keyF], $field, $arrayContent[$key]);
                        }

                    }
                }
            }

        }
        $fileContents = implode(PHP_EOL, $arrayContent);
        
        file_put_contents($filename,$fileContents);
        /**/
        /*=====  End of RECHERCHE DES DOUBLE GUILLEMETS DANS LES CHAMPS  ======*/



        
        $header = NULL;
        $data = array();
        $count = 0;

        
        if (($handle = fopen($filename, 'r')) !== FALSE) {

            while (($row = fgetcsv($handle, 1000, $delimiter, '"')) !== FALSE) {
                // Vérifie et force l'UTF 8
                foreach ($row as $key => $value) {
                    if (!mb_check_encoding($value, 'UTF-8')) {
                        $row[$key] = utf8_encode($value);
                    }
                }

                if($count==0) {
                    $header = $row;
                    
                } else {
                    //$data[] = array_combine($header, $row);
                    $data[] = $row;

                    //var_dump(count($data));
                }
                $count++;
            }
            fclose($handle);
        }


        return $data;
    }

}