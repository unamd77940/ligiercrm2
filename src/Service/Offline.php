<?php

namespace App\Service;

class Offline {

	public function generateAppcache($commercial, $dateFile) {
		$commonFiles = [
					"/",
					"/accueil",
					"/clients",	
					"/client-fiche",	
					"/stock",	
					"/devis-listing",	
					"/devis",	
					"/commande-listing",	
					"/commande",	
					"/prospect-listing",	
					"/prospect",	
					"/memo-listing",	
					"/memo",	
					"/agenda",	
					"/sav-listing",	
					"/sav",	
					"/demande-enlevement",	
					"/css/normalize.css",
					"/css/main.css",
					"/css/duobat.css",
					"/css/fontawesome-all.css",
					"/js/vendor/jquery-ui.min.css",
					"/js/vendor/sumoselect.min.css",
					"/css/fontawesome-all.css",
					"/js/vendor/jquery.dataTables.css",
					"/js/vendor/modernizr-3.5.0.min.js",
					"/js/vendor/jquery-3.2.1.min.js",
					"/js/vendor/jquery.dataTables.min.js",
					"/js/vendor/jquery-ui.min.js",
					"/js/vendor/jquery.sumoselect.min.js",
					"/js/plugins.js",
					"/js/main.js",
					"/webfonts/fa-solid-900.woff2",
					"/webfonts/fa-solid-900.woff",
					"/webfonts/fa-solid-900.ttf",
					"/img/bg-pages.png",
					"/img/header.png",
					"/img/logo.png",
					"/img/logo2.png"
				];

		$jsonFiles = ["commande", "devis", "memo", "client", "or"];

		$content = "CACHE MANIFEST\r";
		$content.= "#".$dateFile."\r";
		foreach ($commonFiles as $key => $value) {			
			$content.= $value."\r";
		}
		foreach ($jsonFiles as $key => $value) {			
			$content.= "/json/".$commercial."_".$dateFile."_".$value.".json\r";
		}

		return $content;
	}



}