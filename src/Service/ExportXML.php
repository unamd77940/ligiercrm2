<?php
namespace App\Service;

Class ExportXML {
        
    public function xmlFromArray($data)  {
        //dump($data);

        $xml = "<PARAM>";

        foreach ($data as $key => $groupe) {
            $xml.= '<'.strtoupper($groupe['type']).' ID="'.$key.'" '.(isset($groupe['attr'])?$groupe['attr']:'').'>';

            if(isset($groupe['lst'])) {
                if(isset($groupe['lst']['attr'])) {
                    $xml.= '<LST '.trim($groupe['lst']['attr']).'>'.trim($groupe['lst']['content']).'</LST>';                    
                } elseif(isset($groupe['lst'][0]) && is_array($groupe['lst'][0])) {
                    foreach ($groupe['lst'] as $keyLst => $valueLst) {
                         $xml.= '<LST '.(isset($valueLst['attr']) ? trim($valueLst['attr']):'').'>'.trim($valueLst['content']).'</LST>'; 
                    }
                }
            }

            if(isset($groupe['fld'])) {
                $xml.= $this->xmlFromFLD($groupe['fld']);
            }

            if(isset($groupe['lin'])) {
                $i = 1;
                foreach ($groupe['lin'] as $keyLine => $line) {
                    $xml.= '<LIN NUM="'.$i.'">';
                    //dump($key);
                    //dump($groupe['lin']);
                    if(isset($line['fld'])) {
                        //dump($line['fld']);
                        $xml.= $this->xmlFromFLD($line['fld']);
                    }
                    $xml.= '</LIN>';
                    $i++;
                }
                //$xml.= $this->xmlFromFLD($groupe['fld']);
            }




            $xml.= '</'.strtoupper($groupe['type']).'>';
        }

        $xml.= "</PARAM>";

        //dump($xml);
        //return $data;
        return $xml;
    }

    private function xmlFromFLD($fields) {
        //dump($fields);
        $xml = "";
        foreach ($fields as $key => $field) {
            //if(!empty(trim($field['value']))) {
                $xml.= '<FLD '.trim($field['attr']).'>'.trim($field['value']).'</FLD>';                
            //}
        }
        return $xml;
    }

}