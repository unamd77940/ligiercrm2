<?php

namespace App\Repository;

use App\Entity\Enlevement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EnlevementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Enlevement::class);
    }

    // Crée la liste des codes à exclure
    public function getCodesAlreadyIn() {
        $toExclude = [];

        $results = $this->createQueryBuilder('e')
            ->select('e.code')
            ->orderBy('e.code', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        foreach ($results as $key => $value) {
            $toExclude[] = $value['code'];
        }

        return $toExclude;
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('e')
            ->where('e.something = :value')->setParameter('value', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
