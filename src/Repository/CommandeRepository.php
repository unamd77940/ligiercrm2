<?php

namespace App\Repository;

use App\Entity\Commande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CommandeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Commande::class);
    }



    public function getCommandeForClients($clients = [])
    {
        $codesClients = [];
        //$codesClients[] = "C00302";
        foreach ($clients as $key => $value) {
            $codesClients[] = $value['code'];
        }

        $commandeResult = $this->findBy(array('client_code' => $codesClients));
        
        $commandeArray = [];
        foreach ($commandeResult as $key => $value) {
            $commande['code'] = $value->getCode();
            $commande['client_code'] = $value->getClientCode();
            $commande['articles'] = $value->getArticles();
            $commande['adresse_code'] = $value->getAdresseCode();
            $commande['commentaires'] = $value->getCommentaires();
            $commande['ref'] = $value->getRef();
            $commande['frais_transport'] = $value->getFraisTransport();
            $commande['totalcommande'] = $value->getTotalHt();
            $commande['totalHT'] = $value->getTotalHt();
            $commande['date_creation'] = $value->getDateCreation()->format('d-m-Y');
            $commande['statut'] = preg_replace('/\r/', '', $value->getStatut());

            // Ajoute le calcul des montants pour les lignes de commandes
            $articles = json_decode($commande['articles']);
            foreach ($articles as $key => $value) {
                $articles[$key]->total_ligne = $value->quantite*$value->prix_net;
            }
            $commande['articles'] = json_encode($articles);

            $commandeArray[] = $commande;
        }

        return $commandeArray;
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('d')
            ->where('d.something = :value')->setParameter('value', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
