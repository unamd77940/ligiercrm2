<?php

namespace App\Repository;

use App\Entity\Sav;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SavRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sav::class);
    }


    public function getSavForClients($clients = [])
    {
        $codesClients = [];
        //$codesClients[] = "C00302";
        foreach ($clients as $key => $value) {
            $codesClients[] = $value['code'];
        }

        $savResult = $this->findBy(array('code_tiers_x3' => $codesClients));
        
        $savArray = [];
        foreach ($savResult as $key => $value) {
            if(preg_match('/^[\w+]*$/', $value->getCodeX3())) {
                $or['code'] = $value->getCodeX3();
                $or['client'] = $value->getCodeTiersX3();
                $or['etat'] = $value->getEtat();
                $or['titre'] = $value->getTitre();
                $or['titre_atelier'] = $value->getTitreAtelier();
                $or['commentaire'] = $value->getCommentaire();
                $or['date'] = $value->getDate()->format("d-m-Y");
                $savArray[] = $or;                
            }

        }

        return $savArray;
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('o')
            ->where('o.something = :value')->setParameter('value', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
