<?php

namespace App\Repository;

use App\Entity\Vehicule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VehiculeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vehicule::class);
    }



    public function getVehicules()
    {
        $vehiculeResult = $this->findAll();
        $savvehicule = [];

        foreach ($vehiculeResult as $key => $value) {
            $vehicule['id'] = $value->getId();
            $vehicule['marque'] = $value->getMarque();
            $vehicule['modele'] = $value->getModele();
            $vehicule['couleur'] = $value->getCouleur();            
            $vehicule['options'] = $value->getOptions();
            $vehicule['no_serie'] = $value->getNo_serie();
            $vehicule['type'] = $value->getType();
            $vehicule['date1eremise'] = $value->getDate1eremise();
            $vehicule['immat'] = $value->getImmat();            
            $vehicule['commentaires'] = $value->getCommentaires();
            $vehicule['energie'] = $value->getEnergie();
            $vehicule['kilometrage'] = $value->getKilometrage();
            $vehicule['puissance'] = $value->getPuissance();
            $vehicule['idclient'] = $value->getIdclient();            
            $vehicule['nom_client'] = $value->getNom_client();
            $vehicule['date_achat'] = $value->getDate_achat();
            $vehicule['prix_achat'] = $value->getPrix_achat();
            $vehicule['frais_etat'] = $value->getFrais_etat();
            $vehicule['date_vente'] = $value->getDate_vente();            
            $vehicule['type_vente'] = $value->getType_vente();
            $vehicule['prix_vente'] = $value->getPrix_vente();
            $vehicule['date_fin_garantie'] = $value->getDate_fin_garantie();
            $vehicule['date_fin_credit'] = $value->getDate_fin_credit();
            $vehicule['dispo_vente'] = $value->getDispo_vente();            
       
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savvehicule[] = $vehicule;
        }
        return $savvehicule;
    }

    public function getVehiculesForClients()
    {
        $vehiculeResult = $this->findByIdclient('C03094');
        $savvehicule = [];

        foreach ($vehiculeResult as $key => $value) {
            $vehicule['id'] = $value->getId();
            $vehicule['marque'] = $value->getMarque();
            $vehicule['modele'] = $value->getModele();
            $vehicule['couleur'] = $value->getCouleur();            
            $vehicule['options'] = $value->getOptions();
            $vehicule['no_serie'] = $value->getNo_serie();
            $vehicule['type'] = $value->getType();
            $vehicule['date1eremise'] = $value->getDate1eremise();
            $vehicule['immat'] = $value->getImmat();            
            $vehicule['commentaires'] = $value->getCommentaires();
            $vehicule['energie'] = $value->getEnergie();
            $vehicule['kilometrage'] = $value->getKilometrage();
            $vehicule['puissance'] = $value->getPuissance();
            $vehicule['idclient'] = $value->getIdclient();            
            $vehicule['nom_client'] = $value->getNom_client();
            $vehicule['date_achat'] = $value->getDate_achat();
            $vehicule['prix_achat'] = $value->getPrix_achat();
            $vehicule['frais_etat'] = $value->getFrais_etat();
            $vehicule['date_vente'] = $value->getDate_vente();            
            $vehicule['type_vente'] = $value->getType_vente();
            $vehicule['prix_vente'] = $value->getPrix_vente();
            $vehicule['date_fin_garantie'] = $value->getDate_fin_garantie();
            $vehicule['date_fin_credit'] = $value->getDate_fin_credit();
            $vehicule['dispo_vente'] = $value->getDispo_vente();            
       
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savvehicule[] = $vehicule;
        }
        return $savvehicule;
    }

    
    
    
}


