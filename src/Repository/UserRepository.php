<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getAllUser()
    {
        $userResult = $this->findAll();
        $savuser = [];

        foreach ($agenceResult as $key => $value) {
            $user['id'] = $value->getId();
            $user['code'] = $value->getCode();
            $user['password'] = $value->getPassword();
            $user['nom'] = $value->getNom();            
            $user['prenom'] = $value->getPrenom();
            $user['site'] = $value->getSite();
            $user['distributeur_id'] = $value->getDistributeur_id();
            $user['agence_id'] = $value->getAgence_id();
            $user['profil_id'] = $value->getProfil_id();
           

            $savuser[] = $user;
        }
        return $savuser;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.something = :value')->setParameter('value', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
