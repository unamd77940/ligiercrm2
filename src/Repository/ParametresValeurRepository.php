<?php

namespace App\Repository;

use App\Entity\ParametresValeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ParametresValeurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ParametresValeur::class);
    }

    public function getAllParametresValeur()
    {
        $parametresvaleurResult = $this->findAll();
        $savparametresvaleur = [];

        foreach ($parametresvaleurResult as $key => $value) {
            $parametresvaleur['id'] = $value->getId();
            $parametresvaleur['idparam'] = $value->getIdparam();
            $parametresvaleur['valeur'] = $value->getValeur();
            $parametresvaleur['codecouleur'] = $value->getCodecouleur();
            $parametresvaleur['commentaires'] = $value->getCommentaires();
           
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savparametresvaleur[] = $parametresvaleur;
        }
        return $savparametresvaleur;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
