<?php

namespace App\Repository;

use App\Entity\Tiers;
use App\Entity\Contact;
use App\Entity\Adresse;
use App\Entity\ClientSynchro;
use App\Entity\ContactSynchro;
use App\Entity\AdresseSynchro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
// use Symfony\Component\Serializer\Serializer;
// use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
// use Symfony\Component\Serializer\Encoder\JsonEncoder;

class TiersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tiers::class);
    }



    public function getTiersInfo($commercial, $type = 'C') {
        // $normalizer = new ObjectNormalizer();
        // $encoder = new JsonEncoder();
        // $serializer = new Serializer(array($normalizer), array($encoder));


        $results = $this->createQueryBuilder('t')
                    ->where('t.rep_gestion = :commercialCode OR t.rep_gestion2 = :commercialCode')
                        ->setParameter('commercialCode', $commercial)
                    ->andWhere('t.type_tiers = :type')
                        ->setParameter('type', $type)
/*
                    ->leftJoin(Contact::class, 'c', 'WITH', 'c.code_tiers_x3 = t.codex3')
                        ->addSelect('c')                    
                    
                    ->leftJoin(Adresse::class, 'a', 'WITH', 'a.code_tiers_x3 = t.codex3')
                        ->addSelect('a')
*/                
                    ->getQuery()
                    ->getResult();


        $tiers = [];
        foreach ($results as $key => $value) {
            $last = null;
            
            if($value) {
                $class = explode("\\", get_class($value));
                $last = array_pop($class);                
            }

            if($last == "Tiers") {
                // Il s'agit d'un tiers, on intègre les clés-valeurs directement dans le tableau des tiers

                // Ci dessous : résultat trop bourrin, nécessité de limiter au max les données renvoyées
                // $tiersArray = $serializer->normalize($value, null, array('groups' => array('toJson')));
                // foreach ($tiersArray as $keyTiers => $valueTiers) {
                //     $tiers[$tiersArray['codeX3']][$keyTiers] = $valueTiers;
                // }
                $codex3 = $value->getCodeX3();
                $tiers[$codex3]['id'] = $value->getId();
                $tiers[$codex3]['type_tiers'] = $value->getTypeTiers();
                $tiers[$codex3]['code'] = $value->getCodeX3();
                $tiers[$codex3]['raisonsociale'] = $value->getRaisonSociale();
                $tiers[$codex3]['encours'] = $value->getEncours();
                $tiers[$codex3]['max'] = $value->getMaxEncours();
                $tiers[$codex3]['retardpaiement'] = $value->getRetardPaiement();
                $tiers[$codex3]['etat'] = $value->getStatutTiers();
                /*
                if(!isset($tiers[$codex3]['adresses'])) {
                    $tiers[$codex3]['adresses'] = [];                    
                }
                if(!isset($tiers[$codex3]['contacts'])) {
                    $tiers[$codex3]['contacts'] = [];
                }
                
                */
                $tiers[$codex3]['activite_id'] = $value->getProfession();
                $tiers[$codex3]['franco'] = $value->getFranco();
                $tiers[$codex3]['siret'] = $value->getNumSiret();
                $tiers[$codex3]['naf'] = $value->getCodeNaf();
                $tiers[$codex3]['tva'] = $value->getTvaIntracom();
                $tiers[$codex3]['iban'] = $value->getIban();
                $tiers[$codex3]['bic'] = $value->getBic();
                $tiers[$codex3]['blocage'] = $value->getBlocage();

                $tiers[$codex3]['envoi_email'] = $value->getEnvoi_email();
                $tiers[$codex3]['envoi_sms'] = $value->getEnvoi_sms();
                $tiers[$codex3]['date_dernier_contact'] = $value->getDate_dernier_contact();

                $tiers[$codex3]['prenom'] = $value->getPrenom();
                $tiers[$codex3]['longitude'] = $value->getLongitude();
                $tiers[$codex3]['latitude'] = $value->getLatitude();
                $tiers[$codex3]['adresse1'] = $value->getAdresse1();
                $tiers[$codex3]['adresse2'] = $value->getAdresse2();
                $tiers[$codex3]['adresse3'] = $value->getAdresse3();
                $tiers[$codex3]['code_postal'] = $value->getCode_postal();
                $tiers[$codex3]['ville'] = $value->getVille();
                $tiers[$codex3]['telephone'] = $value->getTelephone();
                $tiers[$codex3]['portable'] = $value->getPortable();    
                $tiers[$codex3]['email'] = $value->getEmail();    
                
                $tiers[$codex3]['visite_pref_debut'] = $value->getVisite_pref_debut();
                $tiers[$codex3]['visite_pref_fin'] = $value->getVisite_pref_fin();
                $tiers[$codex3]['distributeur_id'] = $value->getDistributeur_id();
                $tiers[$codex3]['canton'] = $value->getCanton();
                $tiers[$codex3]['secteur'] = $value->getSecteur();
                
                

            } 
            /*
            elseif($last == "Adresse") {                
                $adresse = [];
                $adresse['id'] = $value->getId();
                $adresse['code'] = $value->getCodeX3();
                $adresse['adresse1'] = $value->getAddress1();
                $adresse['adresse2'] = $value->getAddress2();
                $adresse['adresse3'] = $value->getAddress3();
                $adresse['postal'] = $value->getZip();
                $adresse['ville'] = $value->getCity();
                $adresse['livchantier'] = $value->getChantier();
                $adresse['telephone'] = $value->getTelephone();
                $adresse['portable'] = $value->getPortable();
                $adresse['fax'] = $value->getFax();
                $adresse['email'] = $value->getEmail();

                $tiers[$value->getCodeTiersX3()]['adresses'][] = $adresse;

            } elseif($last == "Contact") {
                //if($value->getCodeX3()=="000000000005153") dump($value);
                $contact = [];
                $contact['id'] = $value->getId();
                $contact['civilite'] = $value->getCivilite();
                $contact['code'] = $value->getCodeX3();
                $contact['nom'] = $value->getNom();
                $contact['prenom'] = $value->getPrenom();
                $contact['service'] = $value->getService();
                if(!empty($value->getFonction()) && !is_null($value->getFonction())) $contact['fonction'] = $value->getFonction();
                else $contact['fonction'] = "12";
                $contact['email'] = $value->getMailc();
                $contact['telephone'] = $value->getTelc();
                $contact['portable'] = $value->getMobilec();
                $contact['fax'] = $value->getFaxc();
                $contact['defaut'] = $value->getIsDefault();
                $contact['heure_pref'] = $value->getHeurepref();

                
                $tiers[$value->getCodeTiersX3()]['contacts'][] = $contact;
            }
            
            */
        }

        /*
        // Récupère les résultats en base de synchro mais toujours en attente de synchronisation pour les ré-intégrer aux données exportées
        $tiersTempSynchro = $this->getEntityManager()->getRepository(ClientSynchro::class)->findBySynchro(0);        
        foreach ($tiersTempSynchro as $keyTiersTemp => $valueTiersTemp) {
            // Un tier existe dans la table "Tiers", en conflit avec les données en attente de synchro
            if(isset($tiers[$valueTiersTemp->getCodeX3()])) {
                foreach ($valueTiersTemp->getAllVariables(['id', 'synchro']) as $keySource => $valueSource) {
                    if(isset($tiers[$valueTiersTemp->getCodeX3()][$keySource])) {
                        // Modifie les valeurs du tiers importé depuis la base
                        $tiers[$valueTiersTemp->getCodeX3()][$keySource] = $valueSource;
                    }
                }
            }
        }
        /*
        // IDEM avec les contacts
        $contactsTempSynchro = $this->getEntityManager()->getRepository(ContactSynchro::class)->findBySynchro(0);
        foreach($contactsTempSynchro as $keyContactTemp => $valueContactTemp) {

            if(isset($tiers[$valueContactTemp->getCodeTiersX3()])) {
                if(isset($tiers[$valueContactTemp->getCodeTiersX3()]['contacts'])) {
                    // On cherche dans le tiers concerné si le contact existe 
                    $exists = false;
                    foreach ($tiers[$valueContactTemp->getCodeTiersX3()]['contacts'] as $keyTiersContact => $valueTiersContact) {
                        
                        //dump($valueTiersContact['code']." / ".$valueContactTemp->getCodeX3());
                        if($valueTiersContact['code'] == $valueContactTemp->getCodeX3()) {
                            $exists = true;
                            // Contact trouvé dans la liste. On modifie les valeurs 
                            // $tiers[$valueContactTemp->getCodeTiersX3()]['contacts'][$keyTiersContact]

                            foreach ($valueContactTemp->getAllVariables(['id', 'synchro', 'code_tiers_x3']) as $keySource => $valueSource) {
                                
                                if(isset($tiers[$valueContactTemp->getCodeTiersX3()]['contacts'][$keyTiersContact][$keySource])) {
                                    // Modifie les valeurs du tiers importé depuis la base
                                    $tiers[$valueContactTemp->getCodeTiersX3()]['contacts'][$keyTiersContact][$keySource] = $valueSource;
                                }
                            }
                        } 
                    }  

                    // Contact non trouvé, on l'ajoute
                    if(!$exists) {
                        $tiers[$valueContactTemp->getCodeTiersX3()]['contacts'][] = $valueContactTemp->getAllVariables(['id', 'synchro', 'code_tiers_x3']);
                    }  

                } else {
                    $tiers[$valueContactTemp->getCodeTiersX3()]['contacts'] = [$valueContactTemp->getAllVariables(['id', 'synchro', 'code_tiers_x3'])];
                }

            }
        }
        

        // IDEM avec les adresses
        $adressesTempSynchro = $this->getEntityManager()->getRepository(AdresseSynchro::class)->findBySynchro(0);
        foreach($adressesTempSynchro as $keyAdresseTemp => $valueAdresseTemp) {

            if(isset($tiers[$valueAdresseTemp->getCodeTiersX3()])) {
                if(isset($tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'])) {
                    // On cherche dans le tiers concerné si le contact existe 
                    $exists = false;
                    foreach ($tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'] as $keyTiersAdresse => $valueTiersAdresse) {
                        
                        //dump($valueTiersAdresse['code']." / ".$valueAdresseTemp->getCodeX3());
                        if($valueTiersAdresse['code'] == $valueAdresseTemp->getCodeX3()) {
                            $exists = true;
                            // Adresse trouvé dans la liste. On modifie les valeurs 
                            // $tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'][$keyTiersAdresse]

                            foreach ($valueAdresseTemp->getAllVariables(['id', 'synchro', 'code_tiers_x3']) as $keySource => $valueSource) {
                                
                                if(isset($tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'][$keyTiersAdresse][$keySource])) {
                                    // Modifie les valeurs du tiers importé depuis la base
                                    $tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'][$keyTiersAdresse][$keySource] = $valueSource;
                                }
                            }
                        } 
                    }  

                    // Adresse non trouvé, on l'ajoute
                    if(!$exists) {
                        $tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'][] = $valueAdresseTemp->getAllVariables(['id', 'synchro', 'code_tiers_x3']);
                    }  

                } else {
                    $tiers[$valueAdresseTemp->getCodeTiersX3()]['adresses'] = [$valueAdresseTemp->getAllVariables(['id', 'synchro', 'code_tiers_x3'])];
                }

            }
        }




        //$adressesTempSynchro = $this->getEntityManager()->getRepository(AdresseSynchro::class)->findBySynchro(0);

        */
        
        return array_values($tiers);

    }

    

    public function findByCodeX3($value)
    {
        $result = $this->createQueryBuilder('t')
            ->where('t.codex3 = :value')->setParameter('value', $value)
            ->getQuery()
            ->getResult();
        if(isset($result[0])){
            return $result[0];
        } else{
            return null;
        }
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.something = :value')->setParameter('value', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    // src/AppBundle/Repository/MyClassRepository.php

    
    //public function GetNbJours() {
        
        //$conn = $this->getContainer()->get('doctrine.dbal.default_connection');
        
        //$rawSql = "SELECT DATEDIFF(2019-01-01, 2018-01-01) as date FROM memo";
       
        //$result = executeQuery($rawSql);

      //  $result = 100;
        
      //  return $result;       
        
        
    //}
}
