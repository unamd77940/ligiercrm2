<?php

namespace App\Repository;

use App\Entity\Memo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MemoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Memo::class);
    }

    public function generateMissingCodes() {
        $em = $this->getEntityManager();
        $missing = $this->createQueryBuilder('m')
                    ->where('m.code IS NULL')
                    ->getQuery()
                    ->getResult()
                    ;

        foreach ($missing as $key => $memo) {
            $memo->setCode("MEM".$memo->getId());
            $em->persist($memo);
        }
        $em->flush();
    }




    public function getMemoForClients($clients = [])
    {
        $codesClients = [];
        //$codesClients[] = "C00302";
        foreach ($clients as $key => $value) {
            $codesClients[] = $value['code'];
        }

        $memoResult = $this->findBy(array('clientprospect' => $codesClients));
        
        $memoArray = [];
        foreach ($memoResult as $key => $value) {
                        $memo['id'] = $value->getId();

            $memo['code'] = $value->getCode();
            $memo['etape'] = $value->getEtape();
            $memo['etat'] = $value->getEtat();

            $memo['interlocuteur'] = $value->getInterlocuteur();
            $memo['objectif'] = $value->getObjectif();
            $memo['contenu'] = $value->getContenu();
            $memo['datecreation'] = $value->getDateCreation()->format('d-m-Y');
            if($value->getProchainRdv()) {
                $memo['prochain_rdv'] = $value->getProchainRdv()->format('d-m-Y');
            } else {
                $memo['prochain_rdv'] = "";
            }
            if($value->getProchainRdvHeure()) {
                $memo['prochain_rdv_heure'] = $value->getProchainRdvHeure()->format('H:i');
            } else {
                $memo['prochain_rdv_heure'] = "";
            }
            $memo['clientprospect'] = $value->getClientProspect();
            
            $memo['obj_principal_produits'] = $value->getObjPrincipalProduits();
            $memo['obj_principal_operations'] = $value->getObjPrincipalProduits();
            $memo['obj_principal_actions'] = $value->getObjPrincipalProduits();
            
            $memo['obj_repli_produits'] = $value->getObjRepliProduits();
            $memo['obj_repli_operations'] = $value->getObjRepliProduits();
            $memo['obj_repli_actions'] = $value->getObjRepliProduits();

            $memo['livraison'] = $value->getLivraison();
            $memo['type'] = $value->getType();

            $memoArray[] = $memo;
        }

        return $memoArray;
    }


}
