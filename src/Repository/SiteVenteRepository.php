<?php

namespace App\Repository;

use App\Entity\SiteVente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SiteVenteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SiteVente::class);
    }

    
    public function getSiteVente()
    {
        $result = $this->createQueryBuilder('s')
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $tab = [];
        foreach ($result as $key => $value) {
            $tab[] = [$value->getCode(), $value->getTitre()];
        }

        return $tab;
    }
    
}
