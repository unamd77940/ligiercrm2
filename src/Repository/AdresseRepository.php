<?php

namespace App\Repository;

use App\Entity\Adresse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AdresseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Adresse::class);
    }

    public function findByCodeX3AndCodeTiersX3($id, $idTiers)
    {
        $result = $this->createQueryBuilder('a')
            ->where('a.code_x3 = :id')->andWhere('a.code_tiers_x3 = :idTiers')
            ->setParameter('id', $id)
            ->setParameter('idTiers', $idTiers)
            ->getQuery()
            ->getResult();
        if(isset($result[0])){
            return $result[0];
        } else{
            return null;
        }
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
