<?php

namespace App\Repository;

use App\Entity\Distributeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DistributeurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Distributeur::class);
    }

    public function getAllDistributeur()
    {
        $distributeurResult = $this->findAll();
        $savdistributeur = [];

        foreach ($distributeurResult as $key => $value) {
            $distributeur['id'] = $value->getId();
            $distributeur['nom'] = $value->getNom();
            $distributeur['adresse1'] = $value->getAdresse1();            
            $distributeur['adresse2'] = $value->getAdresse2();
            $distributeur['codepostal'] = $value->getCodepostal();
            $distributeur['ville'] = $value->getVille();
            $distributeur['longitude'] = $value->getLongitude();
            $distributeur['latitude'] = $value->getLatitude();
            $distributeur['telephone'] = $value->getTelephone();
            $distributeur['email'] = $value->getEmail();
            $distributeur['commentaires'] = $value->getCommentaires();

            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savdistributeur[] = $distributeur;
        }
        return $savdistributeur;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('d')
            ->where('d.something = :value')->setParameter('value', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
