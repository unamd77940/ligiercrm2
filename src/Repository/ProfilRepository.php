<?php

namespace App\Repository;

use App\Entity\Profil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProfilRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Profil::class);
    }
 public function getAllProfil()
    {
        $profilResult = $this->findAll();
        $savprofil = [];

        foreach ($profilResult as $key => $value) {
            $profil['id'] = $value->getId();
            $profil['nom'] = $value->getNom();
            $profil['iddistributeur'] = $value->getIddistributeur();
           
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savprofil[] = $profil;
        }
        return $savprofil;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
