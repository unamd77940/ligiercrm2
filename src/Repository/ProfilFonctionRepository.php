<?php

namespace App\Repository;

use App\Entity\ProfilFonction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProfilFonctionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProfilFonction::class);
    }

    public function getAllProfilfonction()
    {
        $profilfonctionResult = $this->findAll();
        $savprofilfonction = [];

        foreach ($profilfonctionResult as $key => $value) {
            $profilfonction['id'] = $value->getId();
            $profilfonction['nom'] = $value->getNom();
            $profilfonction['id_profil'] = $value->getId_profil();
           
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savprofilfonction[] = $profilfonction;
        }
        return $savprofilfonction;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
