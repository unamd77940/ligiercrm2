<?php

namespace App\Repository;

use App\Entity\SiteExpedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SiteExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SiteExpedition::class);
    }

    
    public function getSiteExpedition()
    {
        $result = $this->createQueryBuilder('s')
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $tab = [];
        foreach ($result as $key => $value) {
            $tab[] = [$value->getCode(), $value->getTitre()];
        }

        return $tab;
    }
    
}
