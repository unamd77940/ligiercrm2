<?php

namespace App\Repository;

use App\Entity\Agence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AgenceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Agence::class);
    }

    public function getAllAgence()
    {
        $agenceResult = $this->findAll();
        $savagence = [];

        foreach ($agenceResult as $key => $value) {
            $agence['id'] = $value->getId();
            $agence['iddistributeur'] = $value->getIddistributeur();
            $agence['nom'] = $value->getNom();
            $agence['adresse1'] = $value->getAdresse1();            
            $agence['adresse2'] = $value->getAdresse2();
            $agence['codepostal'] = $value->getCodepostal();
            $agence['ville'] = $value->getVille();
            $agence['longitude'] = $value->getLongitude();
            $agence['latitude'] = $value->getLatitude();
            $agence['telephone'] = $value->getTelephone();
            $agence['email'] = $value->getEmail();
            $agence['commentaires'] = $value->getCommentaires();

            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savagence[] = $agence;
        }
        return $savagence;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
