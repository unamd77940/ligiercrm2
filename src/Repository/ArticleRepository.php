<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }


    public function getArticles()
    {
        $articlesResult = $this->findAll();
        $savArticles = [];

        foreach ($articlesResult as $key => $value) {
            $article['code'] = $value->getCodeX3();
            $article['designation'] = $value->getDesignation();
            $article['fournisseur'] = $value->getFournisseur();
            $article['uv'] = $value->getUv();
            $article['uf'] = $value->getUf();
            $article['cdt'] = $value->getCdt();
            $article['prix_tarif'] = $value->getPrixTarif();
            $article['prix_median'] = $value->getPrixMedian();
            $article['prix_revient_rennes'] = $value->getPrixRevientRennes();
            $article['prix_revient_lille'] = $value->getPrixRevientLille();
            $article['remise'] = 0;
            $article['tenue'] = [];

            $sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
                $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            }

            $savArticles[] = $article;
        }
        return $savArticles;
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
