<?php

namespace App\Repository;

use App\Entity\Devis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DevisRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Devis::class);
    }




    public function getDevisForClients($clients = [])
    {
        $codesClients = [];
        //$codesClients[] = "C00302";
        foreach ($clients as $key => $value) {
            $codesClients[] = $value['code'];
        }

        $devisResult = $this->findBy(array('clientcode' => $codesClients));
        
        $devisArray = [];
        foreach ($devisResult as $key => $value) {
            $devis['code'] = $value->getCode();
            $devis['clientcode'] = $value->getClientCode();
            $devis['date_creation'] = $value->getDate()->format('d-m-Y');
            $devis['articles'] = $value->getArticles();
            $devis['statut'] = preg_replace('/\r/', '', $value->getStatut());
            $devis['perte'] = $value->getPerte();
            $devis['etat'] = $value->getEtat();
            $devis['observations'] = $value->getObservations();
            $devis['adresse_code'] = $value->getAdresseCode();
            $devis['site_vente'] = $value->getSiteVente();
            $devis['site_expedition'] = $value->getSiteExpedition();
            $devis['commentaires'] = $value->getCommentaires();
            $devis['ref'] = $value->getRef();
            $devis['frais_transport'] = $value->getFraisTransport();
            $devis['totaldevis'] = $value->getTotalHt();
            $devis['totalHT'] = $value->getTotalHt();
            $devis['source'] = $value->getSource();
            $devis['idvehicule'] = $value->getIdvehicule();
            $devis['commentairesd'] = $value->getCommentairesd();
            $devis['daterelance'] = $value->getDaterelance();


            // Ajoute le calcul des montants pour les lignes de devis
            $articles = json_decode($devis['articles']);
            foreach ($articles as $key => $value) {
                $articles[$key]->total_ligne = $value->quantite*$value->prix_net;
            }
            $devis['articles'] = json_encode($articles);

            $devisArray[] = $devis;
        }

        return $devisArray;
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('d')
            ->where('d.something = :value')->setParameter('value', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
