<?php

namespace App\Repository;

use App\Entity\Parametres;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ParametresRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Parametres::class);
    }

    public function getAllParametres()
    {
        $parametresResult = $this->findAll();
        $savparametres = [];

        foreach ($parametresResult as $key => $value) {
            $parametres['id'] = $value->getId();
            $parametres['nom'] = $value->getNom();
           
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savparametres[] = $parametres;
        }
        return $savparametres;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
