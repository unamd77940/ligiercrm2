<?php

namespace App\Repository;

use App\Entity\Pays;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PaysRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pays::class);
    }

    
    public function getAllPays()
    {
        $results = $this->createQueryBuilder('p')
            ->orderBy('p.nom', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $pays = [];

        foreach ($results as $key => $value) {
            $pays[$value->getCode()] = $value->getNom();
        }

        return $pays;
    }
    
}
