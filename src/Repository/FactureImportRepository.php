<?php

namespace App\Repository;

use App\Entity\FactureImport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FactureImportRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FactureImport::class);
    }


    public function countEntries() {
        return $this->createQueryBuilder('f')
            ->select('COUNT(f)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }


    public function findEntries($volume = 100)
    {
        return $this->createQueryBuilder('f')
            ->setMaxResults($volume)
            ->getQuery()
            ->getResult()
            ;
    }


}
