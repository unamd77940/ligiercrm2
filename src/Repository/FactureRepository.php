<?php

namespace App\Repository;

use App\Entity\Facture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FactureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Facture::class);
    }




    public function getFactureForClients($clients = [])
    {
        $codesClients = [];
        //$codesClients[] = "C00302";
        foreach ($clients as $key => $value) {
            $codesClients[] = $value['code'];
        }

        $factureResult = $this->findBy(array('client_code' => $codesClients));
        
        $factureArray = [];
        foreach ($factureResult as $key => $value) {
            $facture['code'] = $value->getCode();
            $facture['client_code'] = $value->getClientCode();
            $facture['date'] = $value->getDate()->format('d-m-Y');
            $facture['articles'] = $value->getArticles();
            $facture['site_vente'] = $value->getSiteVente();
            $facture['type'] = $value->getType();
            $facture['totalHT'] = $value->getTotalHt();

            // Ajoute le calcul des montants pour les lignes de facture
            $articles = json_decode($facture['articles']);
            foreach ($articles as $key => $value) {
                $articles[$key]->total_ligne = $value->quantite*$value->prix_net;
            }
            $facture['articles'] = json_encode($articles);

            $factureArray[] = $facture;
        }

        return $factureArray;
    }


}
