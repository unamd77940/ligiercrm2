<?php

namespace App\Repository;

use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ContactRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function findByCodeX3AndCodeTiersX3($id, $idTiers)
    {
        $result = $this->createQueryBuilder('c')
            ->where('c.code_x3 = :id')->andWhere('c.code_tiers_x3 = :idTiers')
            ->setParameter('id', $id)
            ->setParameter('idTiers', $idTiers)
            ->getQuery()
            ->getResult();
        if(isset($result[0])){
            return $result[0];
        } else{
            return null;
        }
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
