<?php

namespace App\Repository;

use App\Entity\CommandeImport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CommandeImportRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CommandeImport::class);
    }


    public function findEntries($volume = 100)
    {
        return $this->createQueryBuilder('c')
            ->setMaxResults($volume)
            ->getQuery()
            ->getResult()
            ;
    }

    public function countEntries() {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }




}
