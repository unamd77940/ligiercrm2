<?php

namespace App\Repository;

use App\Entity\Codepostaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CodepostauxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Codepostaux::class);
    }
public function getAllCodepostaux()
    {
        $codepostauxResult = $this->findAll();
        $savcodepostaux = [];

        foreach ($codepostauxResult as $key => $value) {
            $codepostaux['id'] = $value->getId();
            $codepostaux['codepostal'] = $value->getCodepostal();
            $codepostaux['ville'] = $value->getVille();
            $codepostaux['longitude'] = $value->getLongitude();
            $codepostaux['latitude'] = $value->getLatitude();
            

            $savcodepostaux[] = $codepostaux;
        }
        return $savcodepostaux;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
