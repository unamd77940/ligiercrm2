<?php

namespace App\Repository;

use App\Entity\Smsmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SmsmailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Smsmail::class);
    }


    public function getSmsmail()
    {
        $smsmailResult = $this->findAll();
        $savsmsmail = [];

        foreach ($smsmailResult as $key => $value) {
            $smsmail['nom'] = $value->getNom();
            $smsmail['type'] = $value->getType();
            $smsmail['corps'] = $value->getCorps();            
            $smsmail['signature'] = $value->getSignature();
            //$sites = ['ch2', 'li1', 'na1', 've1', 'ch3', 'ch4', 'dsi'];
            //foreach ($sites as $keySite => $valueSite) {
                // $methodTenue = 'getTenue'.ucfirst($keySite);
                // $methodStock = 'getStockInterneA'.ucfirst($keySite);
            //    $article['tenue'][$valueSite] = [$value->{'getTenue'.ucfirst($valueSite)}(), $value->{'getStockInterneA'.ucfirst($valueSite)}()];
            //}

            $savsmsmail[] = $smsmail;
        }
        return $savsmsmail;
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}


