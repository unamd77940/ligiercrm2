var preselectedClient;

$(document).ready(function(){
	console.log('main.js');
	clientShortcuts();
	//preselectedClient = decodeURIComponent(window.location.search.substring(1));

	//getFileContent();


	// Affiche le message mis en file d'attente
	if(localStorage.getItem('messageQueue')) {
		var message = JSON.parse(localStorage.getItem('messageQueue'));
		try {
			displayMessage(message[0], message[1]);
		} catch(e) {
			console.log(e);
		}
		localStorage.removeItem('messageQueue');
	}


	if(!localStorage.getItem("clients")) {


		var modeleClient = {"id":1,
							"code": "CLI1",
							"raisonsociale": "Entreprise 1",
							"encours": "",
							"max": "",
							"retardpaiement":"",
							"etat":"",
							"adresses" : [], 
							"contacts" : [],
							"activite_id": "",
							"franco": "",
							"siret": "",
							"naf": "",
							"tva": "",
							"iban": "",
							"bic": "",
							"condition_id": "",
							};

		var modeleClient2 = {"id":2,
							"code": "CLI2",
							"raisonsociale": "Entreprise 2",
							"encours": "",
							"max": "",
							"retardpaiement":"",
							"etat":"",
							"adresses" : [], 
							"contacts" : [],
							"activite_id": "",
							"franco": "",
							"siret": "",
							"naf": "",
							"tva": "",
							"iban": "",
							"bic": "",
							"condition_id": "",
							};


		var tabClient = [];	

		tabClient = JSON.stringify(tabClient);
		localStorage.setItem("clients", tabClient);
	}

	$('#infoStorage').click(function() {console.log(localStorage)});
	$('#clearStorage').click(function() {localStorage.clear();console.log(localStorage)});
	$('#synchro').click(function() {synchro()});
	$('#synchroFichiers').click(function() {synchroFichiers()});

});


/**
 *  Ouvrir une modal box et la remplir avec le contenu en localStore
 *  
 * 	@param reqId : Id du client
 *  @param modalName : id html de la modal (pour jQuery)
 *  @param modalPrefix : prefixe utilisé pour les input (ex : si <input name="modal_popup_nom"> et que la clé à modifier est "nom", le prefixe est "modal_popup_")
 *
 */
function fillAndOpenModal(reqClient = null, modalName, modalPrefix = null) {
	if(typeof reqClient == 'object') fillForm(reqClient, 'formPopupClient', modalPrefix );
	//client = getClientByCode(reqClient);

	openModal(modalName, modalPrefix);
}


/**
 *  Afficher une modal box
 *  
 *  @param name : id html de la modal (pour jQuery)
 *
 */
function openModal(name) {
	$('#modalBg').fadeIn(200);
	$('#'+name).fadeIn(300);
	// Reformate les champs numéro de téléphone
	$('.numberDot').each(function() {
		phoneFormatAddDot($(this));
	});
	window.scrollTo(0, 0);
}



function closeModal(name) {
	$('#modalBg').fadeOut(200);
	$('#'+name).fadeOut(150);
	try {
		$('#'+name+" form")[0].reset();	
	} catch(e) {
		console.log(e);
	}
}



/**
 *
 * Ferme automatiquement une modal par le bouton qui a la classe "boutoncancel"
 *
 */
$('.boutoncancel').click(function() {
	closeModal($(this).closest('div.modal').prop("id"));
	
});



function saveModalPopup(name) {
	closeModal(name);	
}




/**
 *
 * Récupère le site de vente du commercial
 *
 */
function getSiteCommercial() {
	var infoCommercial = JSON.parse(localStorage.getItem('commercial'));
	if(infoCommercial.site) {
		return infoCommercial.site;
	} else {
		return 'CH2';
	}
}



function listClients() {
	var tabClient = getJsonData("clients");
	console.log(tabClient);
	//var tabClient = JSON.parse(localStorage.getItem("clients"));

	var liste = "";

	$.each(tabClient, function(index, value) {
		liste+= "<tr>";
		liste+= "<td>"+value.raisonsociale+"</td>";
		liste+= "<td>"+(value.contacts && value.contacts.length ? value.contacts[0].nom : '')+"</td>";
		liste+= "<td>"+(value.contacts && value.contacts.length ? value.contacts[0].prenom : '')+"</td>";
		liste+= "<td style=\"text-align:right;\"><span class=\"iconBt\" onClick='changeStorageAndGo([\"currentClient\", \""+value.code+"\"], \"client-fiche\");'><i class=\"fas fa-edit\"></i></span></td>";
		liste+= "</tr>";
	});
                    return false;

	$('#listingClients').append(liste);

	$('#listingClients').DataTable( {
		"pageLength": 10,
		"lengthChange": false,
	    language: {
	    	searchPlaceholder: "Rechercher dans la liste",
	        url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json'
	    }
	} );
}

function getClientById(reqId) {
	var tabClient = JSON.parse(localStorage.getItem("clients"));
	var client;

	// var client = tabClient.filter(function (chain) {
	// 	return chain.id === reqId;
	// })[0];
	for (var i = 0; i < tabClient.length; i++) {
		if(tabClient[i].id==reqId) {
		 	client = tabClient[i];
		}
	}

	return client;
}


function getClientByCode(reqCode, data = null) {
	var tabClient;
	if(data) {
		tabClient = data;
	} else {
		tabClient = JSON.parse(localStorage.getItem("clients"));
	}
	//console.log(tabClient);

	var client;

	// var client = tabClient.filter(function (chain) {
	// 	return chain.id === reqId;
	// })[0];
	for (var i = 0; i < tabClient.length; i++) {
		if(tabClient[i].code==reqCode) {
		 	client = tabClient[i];
			//console.log(i);
		}
	}
	return client;
}



/**
 *  Remplir un formulaire avec les données en localStorage
 *  
 * 	@param data : Obj contenant les clés=>valeurs
 *  @param id : id html du formulaire
 *  @param prefixInput : prefixe utilisé pour les input (ex : si <input name="modal_popup_nom"> et que la clé à modifier est "nom", le prefixe est "modal_popup_")
 *
 */
function fillForm(data, idForm, prefixInput = '' ) {
	for (var key in data){
		if(key) {
			if($('#'+idForm+' input[name='+prefixInput+key+'][type=text]').length )
		    	$('#'+idForm+' input[name='+prefixInput+key+']').val(data[key]);

			if($('#'+idForm+' input[name='+prefixInput+key+'][type=time]').length )
		    	$('#'+idForm+' input[name='+prefixInput+key+']').val(data[key]);

		    
			if(typeof value == "string" || typeof value == "number") {
				value = value.replace("'", "");
			    if(data[key] && $('#'+idForm+' input[name='+prefixInput+key+'][type=radio][value='+value+']').length) {
			    	$('#'+idForm+' input[name='+prefixInput+key+'][type=radio][value=\''+value+'\']').prop("checked", true);
			    }	
			}

		    if($('#'+idForm+' select[name='+prefixInput+key+']').length) {
		    	$('#'+idForm+' select[name='+prefixInput+key+']').val(data[key]);
		    }

                   

		}

	}
}


/**
 *  Récupère les données d'un formulaire
 *  
 *  @param id : id html du formulaire
 *  @param prefixInput : prefixe utilisé pour les input (ex : si <input name="modal_popup_nom"> et que la clé à modifier est "nom", le prefixe est "modal_popup_")
 *
 */
function getDataFromForm(idForm, prefixInput = '' ) {
	var data = {};

	$('#'+idForm+' input[type=text]').each(function() {
		data[$(this).prop("name").replace(prefixInput, '')] = $(this).val();
	});

	$('#'+idForm+' input[type=time]').each(function() {
		data[$(this).prop("name").replace(prefixInput, '')] = $(this).val();
	});

	$('#'+idForm+' select').each(function() {
		data[$(this).prop("name").replace(prefixInput, '')] = $(this).val();
	});

	$('#'+idForm+' textarea').each(function() {
		data[$(this).prop("name").replace(prefixInput, '')] = $(this).val();
	});

	$('#'+idForm+' input[type=radio]:checked').each(function() {
		data[$(this).prop("name").replace(prefixInput, '')] = $(this).val();
	});		

 	delete data[''];

	return data;
}




/**
 *  Modifie la liste déroulante des articles
 *  
 *  @param idArticle : objet liste déroulante
 *  @param displayStockOnly : boolean pour savoir si on doit n'afficher que les articles en stock (true) ou tous les articles (false)
 *  @param articles : tableau contenant tous les articles
 *
 */
function updateSelectArticle(idArticle, displayStockOnly, articles) {
	var customselect = document.getElementById(idArticle);

	try {
		//customselect.sumo.unload();		
	} catch(e) {console.log(e)}

	var articleOption = "<option></option>";
	for (var i = 0; i < articles.length; i++) {
		var instock= false;
		// Vérifie si le stock est dispo
		for(var keystock in articles[i]['tenue']) {
			if(articles[i]['tenue'][keystock][1]>0) {
				instock=true;
				break;
			}
		}
		// N'affiche que les articles en stock (et que l'option de n'afficher que les articles dispo est active)
		if((instock && displayStockOnly) || !displayStockOnly) articleOption+= '<option value="'+articles[i]["code"]+'" >'+articles[i]["code"] + " - " + articles[i]["designation"] + " - " + articles[i]["fournisseur"]+'</option>';

	}

	$('#'+idArticle).html(articleOption);
	customselect.sumo.reload();
}


/**
 *  Arrondi à 4 chiffres après la virgule
 *  
 *  @param value : montant à arrondir
 *
 */
function roundFourthDecimal(value) {
	return Math.round(value*10000)/10000;
}


/**
 *  Récupère les données d'un client
 *  
 *  @param id : id du client
 *
 */
function getClients(reqId) {
	var client = getClientById(reqId);
	if(!client) client = getClientByCode(reqId);

	if(!client) {
		alert('Client inconnnu');
	} else {
		for (var key in client){
			if($('#ficheClient input[name='+key+']').length)
		    	$('#ficheClient input[name='+key+']').val(client[key]);
		}
	}

	return client;
}

/**
 *  Récupère les données d'un vehicule
 *  
 *  @param id : id du vehicule
 *
 */

function getVehicules(reqId) {
	var vehicule = getVehiculeById(reqId);
	if(!vehicule) vehicule = getVehiculeByCode(reqId);

	if(!vehicule) {
		alert('Vehicule inconnnu');
	} else {
		for (var key in vehicule){
			if($('#ficheVehicule input[name='+key+']').length)
		    	$('#ficheVehicule input[name='+key+']').val(vehicule[key]);
		}
	}

	return vehicule;
}

/**
 *  Récupère les données d'un vehicule
 *  
 *  @param id : id du vehicule
 *
 */


/**
 *  Modifie le client
 *  
 *  @param idclient : id du client
 *  @param objClient : obj contenant des clés=>valeurs à mettre à jour ou ajouter
 *
 */
function updateClients(codeclient, objClient) {
	var tabClient = JSON.parse(localStorage.getItem("clients"));


	// Recherche du client dans la table stockée
	var clientIndex = tabClient.findIndex(function (chain) {
		return chain.code === codeclient;
	});

	// console.log("CODE CLIENT : "+codeclient);
	// console.log("INDEX CLIENT : "+clientIndex);
	// Le client existe déjà dans le localStorage
	if(clientIndex>=0) {
		console.log(objClient);
		//tabClient[clientIndex] = objClient;
		// for (var key in tabClient[clientIndex]){
		//     if (tabClient[clientIndex].hasOwnProperty(key)) {
		//     	// Si une valeur existe pour cette clé dans l'objet reçu, on met à jour
		//     	if(objClient[key])
		//     		tabClient[clientIndex][key] = objClient[key];
		//     }
		// }
		for (var key in objClient){
			tabClient[clientIndex][key] = objClient[key];
		}

	} else {
		// Le client est un nouveau client
		tabClient.push(objClient);
	}
	localStorage.setItem("clients", JSON.stringify(tabClient));

}

function addObjectToArrayClient(codeclient, keyToUpdate, objectToAdd) {
	var tabClient = JSON.parse(localStorage.getItem("clients"));

	// Recherche du client dans la table stockée
	for (var i = 0; i < tabClient.length; i++) {
		if(tabClient[i].code == codeclient && tabClient[i][keyToUpdate]) {
			// Client trouvé et clé également
			tabClient[i][keyToUpdate].push(objectToAdd);			

			// Mise à jour du localStorage
			localStorage.setItem("clients", JSON.stringify(tabClient));
			break;
		}
	}

}


function changeStorageAndGo(keyValueToChange, destination) {
	if(keyValueToChange.length) {
		localStorage.setItem(keyValueToChange[0], keyValueToChange[1]);		
	}
	window.location.href = destination;
}



/**
 *  Vérifie la validitée d'un formulaire
 *  
 *  @param toCheck : obj contenant les données à vérifier
 *  ex : {
 			"checkForNumber" : ["idjQuery 1", "id jquery 2"...] ,
 			}
	- checkForNumber : vérifie qu'on a bien à faire à des float ou integer
	- checkForEmail : vérifie la validité d'un mail
	- checkForAPE : vérifie la validité d'un code APE
 *
 */
function isFormValid(toCheck) {
	errors = [];
	//console.log(toCheck);

	// Verif que les données sont des nombres
	if(toCheck && toCheck.checkForNumber) {
		for (var i = 0; i < toCheck.checkForNumber.length; i++) {
			try {
				if(!checkIfNumber($(toCheck.checkForNumber[i]).val()))
					errors.push([toCheck.checkForNumber[i], "Cette valeur doit être un nombre"]);
			} catch(ex) {
				console.log(ex);
			}
			
		}
	}

	// Verif que les données sont des nombres > 0
	if(toCheck && toCheck.checkForPositiveNumber) {
		for (var i = 0; i < toCheck.checkForPositiveNumber.length; i++) {
			try {
				if(!checkIfPositiveNumber($(toCheck.checkForPositiveNumber[i]).val()))
					errors.push([toCheck.checkForPositiveNumber[i], "Cette valeur doit être supérieure à 0"]);
			} catch(ex) {
				console.log(ex);
			}
			
		}
	}


	// Verif les formats d'email
	if(toCheck && toCheck.checkForEmail) {
		for (var i = 0; i < toCheck.checkForEmail.length; i++) {
			try {
				if(!checkIfEmail($(toCheck.checkForEmail[i]).val()))
					errors.push([toCheck.checkForEmail[i], "Format d'email invalide"]);
			} catch(ex) {
				console.log(ex);
			}
			
		}
	}

	// Verif que le code APE est valide
	if(toCheck && toCheck.checkForAPE) {
		for (var i = 0; i < toCheck.checkForAPE.length; i++) {
			try {
				if(!checkIfAPE($(toCheck.checkForAPE[i]).val()))
					errors.push([toCheck.checkForAPE[i], "Format APE invalide (4 chiffres + 1 lettre)"]);
			} catch(ex) {
				console.log(ex);
			}
			
		}
	}

	// Verif que le code APE est valide
	if(toCheck && toCheck.checkForSIREN) {
		for (var i = 0; i < toCheck.checkForSIREN.length; i++) {
			try {
				if(!checkIfSIREN($(toCheck.checkForSIREN[i]).val()))
					errors.push([toCheck.checkForSIREN[i], "SIREN invalide (9 chiffres)"]);
			} catch(ex) {
				console.log(ex);
			}
			
		}
	}

	// Verif que le telephone/fax est valide
	if(toCheck && toCheck.checkForPhone) {
		for (var i = 0; i < toCheck.checkForPhone.length; i++) {
			try {
				if(!checkIfPhone($(toCheck.checkForPhone[i]).val()))
					errors.push([toCheck.checkForPhone[i], "Numéro invalide (10 chiffres)"]);
			} catch(ex) {
				console.log(ex);
			}
			
		}
	}

	return (errors.length==0 ? true : errors);
}


function removeErrorMessageForm(id) {
	$(id+" select").removeClass('toCheck');
	$(id+" input").removeClass('toCheck');
	$(id+" textarea").removeClass('toCheck');
	$(id+" .messageErrorMini").remove();
}


function checkIfNumber(toCheck) {
	if(toCheck.match(/^[0-9]+[\.\,]*[0-9]*$/gm)) 
		return true;
	else return false;
}


function checkIfPositiveNumber(toCheck) {
	if(toCheck.match(/^[1-9]+[\.\,]*[0-9]*$/gm)) 
		return true;
	else return false;
}

function checkIfPhone(toCheck) {
	if(toCheck.match(/^[0-9]{10}$/gm) || toCheck.match(/^[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}/gm) || toCheck == "") 
		return true;
	else return false;
}

function checkIfEmail(toCheck) {
	if(toCheck.match(/^[0-9a-zA-Z-.+]+@[0-9a-zA-Z-]+.[0-9a-zA-Z.]{2,10}$/gm) || toCheck == "") 
		return true;
	else return false;
}

function checkIfAPE(toCheck) {
	if(toCheck.match(/^[0-9]{4}[a-zA-Z]{1}$/gm)) 
		return true;
	else return false;
}

function checkIfSIREN(toCheck) {
	if(toCheck.match(/^[0-9]{9}$/gm)) 
		return true;
	else return false;
}

/* Formate les champs 0123456789 -> 01.23.45.67.89 */
function phoneFormatAddDot(elem) {
	var value = elem.val();
	if(value) {
		value = value.replace(/\D/g, '');

		var j=0;
		for (var i = value.length - 1; i >= 0; i--) {
			j++;
			if(j%2==0 && i) {
				value = value.slice(0, i)+'.'+value.slice(i, value.length);
			}
		}
		elem.val(value);
	}
}


function phoneFormatRemoveDot(elem) {
	var value = elem.val();
	value = value.replace(/\D/g, '');
	elem.val(value);
}


$('.numberDot').focus(function() {
	phoneFormatRemoveDot($(this));
});

$('.numberDot').focusout(function() {
	phoneFormatAddDot($(this));
});


/**
 *
 * GENERATION DES CODES
 *
 */
function generateNewAdressCode(adresses) {
	if(!adresses.length) {
		// Aucune adresses -> on génère le code de l'adresse principale
		return 'F01';
	} else {
		// Adresse existante, on recherche le dernier numéro
		var find = false;
		var i = 1;
		while(!find) {
			var ref = "L"+(i<10?'0':'')+i;
			console.log("TEST : "+ref);
			var index = adresses.findIndex(function (chain) {
				return chain.code === ref;
			});
			console.log(index);
   			if(index<0) {
   				find = true;
   				console.log("FINAL : "+ref);
   				return ref;
   			} else {
   				i++;
   			}
		}		
	}

}


/**
 *
 * SYNCHRO
 *
 */
function synchroFichiers() {
	$.ajax({
		url : '/ajax/generate-json',
		type : 'POST',
		data : {
				commercial : localStorage.getItem('id_commercial'),
				},
		dataType : 'json',

		success : function(json, statut){ 
			filesNames = json;
			var properties = {};
			var updatedFiles = '';
			console.log(filesNames);
			try {
				if(localStorage.getItem('indexProperties')) {
					properties = JSON.parse(localStorage.getItem('indexProperties'));
				}
                                if(filesNames.vehicule) {
					properties.vehicule = {'file': filesNames.vehicule, 'local': 'vehicule'};
					updatedFiles+="Vehicule ";
				}
                                if(filesNames.smsmail) {
					properties.smsmail = {'file': filesNames.smsmail, 'local': 'smsmail'};
					updatedFiles+="Smsmail ";
				}
				if(filesNames.clients) {
					properties.clients = {'file': filesNames.clients, 'local': 'clients'};
					updatedFiles+="Clients ";
				}

				if(filesNames.ors) {
					properties.ors = {'file': filesNames.ors, 'local': 'ors'};
					updatedFiles+="SAV ";
				}

				if(filesNames.devis) {
					properties.devis = {'file': filesNames.devis, 'local': 'devis'};
					updatedFiles+="Devis ";
				}

				if(filesNames.factures) {
					properties.factures = {'file': filesNames.factures, 'local': 'factures'};
					updatedFiles+="Factures ";
				}

				if(filesNames.memos) {
					properties.memos = {'file': filesNames.memos, 'local': 'memos'};
					updatedFiles+="Memo ";
				}

				if(filesNames.commandes) {
					properties.commandes = {'file': filesNames.commandes, 'local': 'commandes'};
					updatedFiles+="Commandes ";
				}

				if(filesNames.articles) {
					properties.articles = {'file': filesNames.articles, 'local': 'articles'};
					updatedFiles+="Articles ";
				}

				if(filesNames.pays) {
					properties.pays = {'file': filesNames.pays};
				}

				if(filesNames.appcache) {
					properties.appcache = {'file': filesNames.appcache};
				}

                                if(filesNames.parametres) {
					properties.parametres = {'file': filesNames.parametres, 'local': 'parametres'};
					updatedFiles+="parametres ";
				}
                                
                                if(filesNames.parametresvaleur) {
					properties.parametresvaleur = {'file': filesNames.parametresvaleur, 'local': 'parametresvaleur'};
					updatedFiles+="parametresvaleur ";
				}
                                if(filesNames.profil) {
					properties.profil = {'file': filesNames.profil, 'local': 'profil'};
					updatedFiles+="profil ";
				}
                                
                                if(filesNames.profilfonction) {
					properties.profilfonction = {'file': filesNames.profilfonction, 'local': 'profilfonction'};
					updatedFiles+="profilfonction ";
				}
                                
                                if(filesNames.codepostaux) {
					properties.profilfonction = {'file': filesNames.codepostaux, 'local': 'codepostaux'};
					updatedFiles+="codepostaux ";
				}
                                if(filesNames.agence) {
					properties.agence = {'file': filesNames.agence, 'local': 'agence'};
					updatedFiles+="agence ";
				}
                                
                                if(filesNames.distributeur) {
					properties.distributeur = {'file': filesNames.distributeur, 'local': 'distributeur'};
					updatedFiles+="distributeur ";
				}
                                
				console.log(filesNames);
				console.log(properties);
				localStorage.setItem('indexProperties', JSON.stringify(properties));

				removeStandBy(true);
				displayMessage('Les fichier(s) locaux "<i>'+updatedFiles+'</i>" ont été mis à jour');
			} catch(e) {
				console.log(e);
				displayMessage('Problème lors de la mise à jour', 'error');
			}

			/**/
		},

		error : function(result, statut, error){
			removeStandBy(false);
		}
		
    });
} 



function synchro() {
	if(!localStorage.getItem('id_commercial')) localStorage.setItem('id_commercial', 'DLE');


	$.ajax({
		url : '/ajax/synchro',
		type : 'POST',
		data : {
				storage : JSON.stringify(localStorage),
				commercial : localStorage.getItem('id_commercial'),
				memos : localStorage.getItem('memos'),
				prospects : localStorage.getItem('prospects'),
				enlevements : localStorage.getItem('enlevements'),
				devis : localStorage.getItem('devis'),
				clients : localStorage.getItem('clients'),
				vehicule : localStorage.getItem('vehicule'),
				smsmail : localStorage.getItem('smsmail'),
                                commandes : localStorage.getItem('commandes'),
				},
		dataType : 'json',

		success : function(json, statut){ 
			console.log(json);
			localStorage.setItem('memos', '[]'),
			localStorage.setItem('prospects', '[]'),
			localStorage.setItem('enlevements', '[]'),
			localStorage.setItem('devis', '[]'),
			localStorage.setItem('clients', '[]'),
			localStorage.setItem('vehicule', '[]'),
			localStorage.setItem('smsmail', '[]'),
                        localStorage.setItem('commandes', '[]'),
			localStorage.setItem('activites', JSON.stringify(json.activites));
			localStorage.setItem('sitevente', JSON.stringify(json.sitevente));
			localStorage.setItem('fonctions', JSON.stringify(json.fonctions));
			localStorage.setItem('pays', JSON.stringify(json.pays));
			localStorage.setItem('conditionsPaiement', JSON.stringify(json.conditionsPaiement));
			/**/
			console.log(localStorage);
		},

		error : function(result, statut, error){
			
		}
		
    });
}


var temp;
function displayMessage(message, icon = 'success') {
	var fa = 'check';
	if(icon == 'info') fa = 'info-circle';
	if(icon == 'error') fa = 'exclamation-triangle';


	$('#messageGlobal').html('<i class="fas fa-'+fa+'"></i> '+message);
	$('#messageGlobal').addClass('down');

	temp = setInterval(hideMessage, 3000);
}

function queueMessage(message, icon = 'success') {
	localStorage.setItem('messageQueue', JSON.stringify([message, icon]));
}



function hideMessage() {
	$('#messageGlobal').removeClass('down');
	clearInterval(temp);
}


/**
 *
 * Récupère le contenu du fichier
 * @param file -> nom du fichier
 * @param path -> liens vers le fichier et l'index du localstorage
 *
 */
function getFileContent(file = 'client.json', path) {
	console.log(file);

	$.ajax({
	  	url: "/json/"+file,
	  	type: "GET",
	  	//async: false,

	  	success : function(json, statut){ 
	  		getFromFileAndLocal(path, json);
	  		//console.log(json);
	  		return json;
		},

		error : function(result, statut, error){
			console.log(error);
			return false;
		}
	});
}


/**
 *
 * Récupère le nom des fichiers et le nom dans le localStorage
 * @param property -> liens vers le fichier et l'index du localstorage
 *
 */
function getJsonData(property = 'clients') {
	// Génère la liste qui stocke les noms de fichiers et les noms dans le localStorage pour une propriété donnée
	// if(!localStorage.getItem('indexProperties')) {
	// 	var index = {};
	// 	index.clients = {"file": "client.json", "local": "clients"};
		
	// 	localStorage.setItem('indexProperties', JSON.stringify(index));
	// }
	if(property == "prospects" && !JSON.parse(localStorage.getItem('indexProperties'))[property]) {
		var properties = JSON.parse(localStorage.getItem('indexProperties'));
		properties.prospects = {"file": "prospect.json", "local": "prospects"};
		localStorage.setItem('indexProperties', JSON.stringify(properties));
	}

	var path = JSON.parse(localStorage.getItem('indexProperties'))[property];
	var data = [];

	if(path && path.file) {
		getFileContent(path.file, path);
	} else {
		console.log("Erreur "+property+" : merci de définir le nom du fichier à récupérer ainsi que l'espace dans le localStorage");
	}
        return false;


	//console.log(property);
}



/**
 *
 * Fusionne et complète les données présentes dans le fichier avec les données locales
 * @param path -> liens vers le fichier et l'index du localstorage
 * @param data -> données récupérée du fichier
 *
 */
function getFromFileAndLocal(path, data) {
	if(path.local && localStorage.getItem(path.local)!='') {
		// Ajoute ou modifie les données avec celles stockées dans le localStorage
	//console.log(path.local);	

		if(localStorage.getItem(path.local)=="") localStorage.setItem(path.local, '[]');
		var dataLocal = JSON.parse(localStorage.getItem(path.local));
		if(dataLocal) {
			for (var i = 0; i < dataLocal.length; i++) {

				var index = data.findIndex(function (chain) {
	   					return chain.code === dataLocal[i].code;
	   			});
	   			if(index!= "undefined" && index>=0) {
	   				// Mise à jour des données existantes   				
	   				data[index] = dataLocal[i];
	   			} else {
					// ajout aux données existantes
					data.push(dataLocal[i]);
				}
			}
		}
	}
	//return data;
	//console.log(data);
	displayData(data);
}


/**
 *
 * Affiche ou masque la barre d'accès rapide client
 * @param action -> Si 'show' ou rien : affiche la barre / si 'hide' cache la barre
 *
 */
function clientShortcuts(action = 'show') {
	if(action == 'show' && localStorage.getItem('currentClient') && localStorage.getItem('currentClientRaisonSociale')) {
		$('#topBar>div>a>span>span').text(localStorage.getItem('currentClientRaisonSociale'));
		$('#topBar').show();
		$('body').addClass('morePadding');
	} else {
		$('#topBar').hide();
		$('body').removeClass('morePadding');
	}
}


// Cache la barre de préselection de client
$('#topBarUp').click(function() {
	$('#topBar').removeClass('isDown');
	$('#topBar').addClass('isUp');
	$('body').removeClass('morePadding');	
});

// Ré-affiche la barre de préselection de client
$('#topBarDown').click(function() {
	$('#topBar').removeClass('isUp');
	$('#topBar').addClass('isDown');
	$('body').addClass('morePadding');
});

// Annule la préselection de client
$('#clientID > i').click(function() {
	if(confirm("Annuler la présélection de ce client ?")) {
		localStorage.removeItem('currentClientRaisonSociale');
		localStorage.removeItem('currentClient');
		location.reload();
	}
});


// Gestion du menu inférieur en responsive
$('body>footer>span').click(function() {
	if($(this).parent().hasClass('open')) {
		$(this).parent().removeClass('open');
		console.log($(this).children());
		$(this).children().addClass('fa-angle-up');
		$(this).children().removeClass('fa-angle-down');
	} else {
		$(this).parent().addClass('open');
		$(this).children().removeClass('fa-angle-up');
		$(this).children().addClass('fa-angle-down');
	}
});


/**
 *
 * Vérifie que l'user nouvellement loggué est bien celui loggué précédemment
 *
 */
function checkNewUser(logged) {
	if(!localStorage.getItem('id_commercial') || localStorage.getItem('id_commercial')!= logged.code ) {
		// Pas de "id_commercial" existant, ou différent de celui qui vient de se logguer.
		localStorage.clear();
		localStorage.setItem('id_commercial', logged.code);
		var dataCommercial = {"nom":logged.nom, "prenom":logged.prenom, "code":logged.code, "site":logged.site};
		localStorage.setItem('commercial', JSON.stringify(dataCommercial));
		window.location.replace('synchro');
		//alert("nouvel utilisateur");	
	} else {
		localStorage.setItem('commercial', JSON.stringify({"nom":logged.nom, "prenom":logged.prenom, "code":logged.code, "site":logged.site}));
		
	}
}

/**
 *
 * Désactive les champs des formulaires passés en argument
 *
 */
function readonlyForm(forms) {
	for (var i = 0; i < forms.length; i++) {
		console.log($('#'+forms[i]));

		$('#'+forms[i]+' input').prop('disabled', 'true');
		$('#'+forms[i]+' input[type=submit]').hide();
		$('#'+forms[i]+' select').prop('disabled', 'true');
	}
	$('#nvArticle').hide();
	$('.boutonDelete').hide();
	$('#adresses .boutonSelect').hide();
	$('#editArticle #saveModalArt').hide();
	$('#closeModalArt').removeProp('disabled');
	$('#closeModalArt').removeAttr('disabled');
}

/**
 * Fonctions EDETR
 */





function getVehiculeById(reqId, data = null) {
	var tabVehicule;
	if(data) {
		tabVehicule = data;
	} else {
		tabVehicule = JSON.parse(localStorage.getItem("vehicule"));
	}
	//console.log(tabClient);

	var vehicule;

	// var client = tabClient.filter(function (chain) {
	// 	return chain.id === reqId;
	// })[0];
	for (var i = 0; i < tabVehicule.length; i++) {
		if(tabVehicule[i].id==reqId) {
		 	vehicule = tabVehicule[i];
			//console.log(i);
		}
	}
	return vehicule;
}

