--
-- Structure de la table `tiers_imports`
--

CREATE TABLE IF NOT EXISTS `tiers_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_x3` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `type_tiers` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_categorie` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raison_sociale` varchar(71) COLLATE utf8_unicode_ci DEFAULT NULL,
  `encours` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_encours` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retard_paiement` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocage` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sigle` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taille` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profession` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `franco` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_siret` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_naf` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iban` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bic` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domiciliation` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rep_gestion` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_activation` date DEFAULT NULL,
  `date_desactivation` date DEFAULT NULL,
  `statut_tiers` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_cond_paie` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_origine_tiers` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_prospect` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `langue_tiers` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `tva_intracom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pays_tiers` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `last_x3_update` datetime NOT NULL,
  `last_mysql_update` datetime NOT NULL,
  `addedit_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


--
-- Structure de la table `contact_imports`
--

CREATE TABLE IF NOT EXISTS `contact_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiers_id` int(11) DEFAULT NULL,
  `code_x3` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `code_tiers_x3` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `civilite` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL,
  `fonction` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailc` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_address_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobilec` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faxc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL,
  `addedit_flag` tinyint(1) NOT NULL,
  `last_mysql_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;



--
-- Structure de la table `adresse_imports`
--

CREATE TABLE IF NOT EXISTS `adresse_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_x3` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `code_tiers_x3` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address3` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chantier` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `portable` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `sav_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_x3` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_tiers_x3` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etat` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titre_atelier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaire` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;